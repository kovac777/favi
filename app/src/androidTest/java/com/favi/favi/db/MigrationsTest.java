package com.favi.favi.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.arch.persistence.room.testing.MigrationTestHelper;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

/**
 * Created by maxvinnik on 13.01.2018.
 */
@RunWith(AndroidJUnit4.class)
public class MigrationsTest {

    private static final String TEST_DB = "migration-test";

    @Rule
    public MigrationTestHelper migrationTestHelper =
            new MigrationTestHelper(InstrumentationRegistry.getInstrumentation(),
                    AppDatabase.class.getCanonicalName(), new FrameworkSQLiteOpenHelperFactory());

    @Test
    public void testMigration1To2() throws IOException {
        SupportSQLiteDatabase supportSQLiteDatabase = migrationTestHelper.createDatabase(TEST_DB, 1);

        supportSQLiteDatabase.close();

        migrationTestHelper.runMigrationsAndValidate(TEST_DB, 2, true,
                Migrations.MIGRATION_1_2);

        migrationTestHelper.closeWhenFinished(supportSQLiteDatabase);
    }
}