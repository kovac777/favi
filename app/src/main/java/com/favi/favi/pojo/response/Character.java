package com.favi.favi.pojo.response;

import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;

import com.favi.favi.adapter.recyclerview.BaseRVAdapter;

/**
 * Created by kovac on 08.11.2017.
 * My SKYPE: maxvinnik
 */

public class Character implements BaseRVAdapter.ItemViewType, Parcelable {

    public static final Creator<Character> CREATOR = new Creator<Character>() {
        @Override
        public Character createFromParcel(Parcel in) {
            return new Character(in);
        }

        @Override
        public Character[] newArray(int size) {
            return new Character[size];
        }
    };
    /**
     * tagId : 1
     * tagName : Donald Trump
     * icon : http://x.api.faviapp.com/Images\tags\trump.jpg
     * enabled : true
     */

    private int tagId;
    private String tagName;
    private String icon;
    private boolean enabled;

    @Ignore
    protected Character(Parcel in) {
        tagId = in.readInt();
        tagName = in.readString();
        icon = in.readString();
        enabled = in.readByte() != 0;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public BaseRVAdapter.ViewType getType() {
        return BaseRVAdapter.ViewType.CHARACTER;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(tagId);
        parcel.writeString(tagName);
        parcel.writeString(icon);
        parcel.writeByte((byte) (enabled ? 1 : 0));
    }
}
