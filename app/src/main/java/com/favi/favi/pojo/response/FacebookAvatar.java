package com.favi.favi.pojo.response;

/**
 * Created by maxvinnik on 16.01.2018.
 */

public class FacebookAvatar {
    /**
     * picture : {"data":{"height":720,"is_silhouette":true,"url":"https://scontent.xx.fbcdn.net/v/t31.0-1/c212.0.720.720/p720x720/10506738_10150004552801856_220367501106153455_o.jpg?oh=45f50b2b7a5d1249883029d44d1eb99e&oe=5AEC9C2E","width":720}}
     * id : 135550110576869
     */

    private PictureBean picture;
    private String id;

    public PictureBean getPicture() {
        return picture;
    }

    public void setPicture(PictureBean picture) {
        this.picture = picture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class PictureBean {
        /**
         * data : {"height":720,"is_silhouette":true,"url":"https://scontent.xx.fbcdn.net/v/t31.0-1/c212.0.720.720/p720x720/10506738_10150004552801856_220367501106153455_o.jpg?oh=45f50b2b7a5d1249883029d44d1eb99e&oe=5AEC9C2E","width":720}
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * height : 720
             * is_silhouette : true
             * url : https://scontent.xx.fbcdn.net/v/t31.0-1/c212.0.720.720/p720x720/10506738_10150004552801856_220367501106153455_o.jpg?oh=45f50b2b7a5d1249883029d44d1eb99e&oe=5AEC9C2E
             * width : 720
             */

            private int height;
            private boolean is_silhouette;
            private String url;
            private int width;

            public int getHeight() {
                return height;
            }

            public void setHeight(int height) {
                this.height = height;
            }

            public boolean isIs_silhouette() {
                return is_silhouette;
            }

            public void setIs_silhouette(boolean is_silhouette) {
                this.is_silhouette = is_silhouette;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public int getWidth() {
                return width;
            }

            public void setWidth(int width) {
                this.width = width;
            }
        }
    }
}
