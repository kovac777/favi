package com.favi.favi.pojo.request;

/**
 * Created by User on 24.01.2018.
 * My SKYPE: maxvinnik
 */

public class FirebaseToken {

    /**
     * key : web
     * token : adadasd
     */

    private String key;
    private String token;

    public FirebaseToken(String key, String token) {
        this.key = key;
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
