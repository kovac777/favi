package com.favi.favi.pojo.request;

/**
 * Created by kovac on 14.11.2017.
 * My SKYPE: maxvinnik
 */

public class GenerateFav {

    private String text;
    private int tagId;
    private String key;

    public GenerateFav(String text, int tag, String key) {
        this.text = text;
        this.tagId = tag;
        this.key = key;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
