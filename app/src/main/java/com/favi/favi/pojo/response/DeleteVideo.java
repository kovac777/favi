package com.favi.favi.pojo.response;

/**
 * Created by kovac on 03.11.2017.
 * My SKYPE: maxvinnik
 */

public class DeleteVideo {

    /**
     * deleted : true
     */

    private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
