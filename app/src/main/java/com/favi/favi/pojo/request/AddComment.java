package com.favi.favi.pojo.request;

/**
 * Created by User on 20.01.2018.
 * My SKYPE: maxvinnik
 */

public class AddComment {

    private int videoId;
    private String text;

    public AddComment(int videoId, String text) {
        this.videoId = videoId;
        this.text = text;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
