package com.favi.favi.pojo;

import com.favi.favi.adapter.recyclerview.BaseRVAdapter;

/**
 * Created by kovac on 09.11.2017.
 * My SKYPE: maxvinnik
 */

public class AllCountWords implements BaseRVAdapter.ItemViewType {

    private int count;

    public AllCountWords(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public BaseRVAdapter.ViewType getType() {
        return BaseRVAdapter.ViewType.ALL_COUNT_WORDS;
    }
}
