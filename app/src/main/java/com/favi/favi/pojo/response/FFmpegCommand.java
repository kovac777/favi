package com.favi.favi.pojo.response;

/**
 * Created by kovac on 20.11.2017.
 * My SKYPE: maxvinnik
 */

public class FFmpegCommand {

    /**
     * ffmpeg : -i "http://api.faviapp.com/Videos/output\64\d9\2e\3a\1b\45\63.mp4" -i "http://api.faviapp.com/Videos/output\e3\e1\2c\cc\23\49\c2.mp4"  -filter_complex "[0:v]setpts=PTS-STARTPTS[v1];[1:v]format=yuva420p,setpts=PTS-STARTPTS+(0.223039993190765/TB)[v2];[v1][v2]overlay,format=yuv420p[v];[0:a][1:a]acrossfade=d=0.272000004053116[a]" -map [v] -map [a] -y "{0}"
     */

    private String ffmpeg;

    public String getFfmpeg() {
        return ffmpeg;
    }

    public void setFfmpeg(String ffmpeg) {
        this.ffmpeg = ffmpeg;
    }
}
