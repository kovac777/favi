package com.favi.favi.pojo.request;

/**
 * Created by User on 20.01.2018.
 * My SKYPE: maxvinnik
 */

public class EditComment {

    private int commentId;
    private String text;

    public EditComment(int commentId, String text) {
        this.commentId = commentId;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }
}
