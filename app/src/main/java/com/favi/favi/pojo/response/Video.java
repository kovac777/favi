package com.favi.favi.pojo.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by kovac on 03.11.2017.
 * My SKYPE: maxvinnik
 */

public class Video implements Parcelable {

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };
    /**
     * url : /Videos/output/bd/1e/39/3b/cb/f5.mp4
     * vidid : 1
     * thumbnail : /Videos/output/bd/1e/39/3b/cb/f5_t.png
     * editPieces : []
     */

    private String url;
    private int vidid;
    private String thumbnail;
    //ignore this field yet
    private List<?> editPieces;

    protected Video(Parcel in) {
        url = in.readString();
        vidid = in.readInt();
        thumbnail = in.readString();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getVidid() {
        return vidid;
    }

    public void setVidid(int vidid) {
        this.vidid = vidid;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<?> getEditPieces() {
        return editPieces;
    }

    public void setEditPieces(List<?> editPieces) {
        this.editPieces = editPieces;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(url);
        parcel.writeInt(vidid);
        parcel.writeString(thumbnail);
    }
}
