package com.favi.favi.pojo;

import com.favi.favi.adapter.recyclerview.BaseRVAdapter;

/**
 * Created by kovac on 09.11.2017.
 * My SKYPE: maxvinnik
 */

public class Chip implements BaseRVAdapter.ItemViewType {

    private String word;

    public Chip(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public BaseRVAdapter.ViewType getType() {
        return BaseRVAdapter.ViewType.CHIP;
    }
}
