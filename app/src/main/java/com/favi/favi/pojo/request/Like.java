package com.favi.favi.pojo.request;

/**
 * Created by User on 24.01.2018.
 * My SKYPE: maxvinnik
 */

public class Like {

    /**
     * vidid : 11
     * key : ‘web’
     * userId : 4
     */

    private int vidid;
    private String key;
    private int userId;

    public Like(int vidid, String key, int userId) {
        this.vidid = vidid;
        this.key = key;
        this.userId = userId;
    }

    public int getVidid() {
        return vidid;
    }

    public void setVidid(int vidid) {
        this.vidid = vidid;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
