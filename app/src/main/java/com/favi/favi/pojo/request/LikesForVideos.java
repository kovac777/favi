package com.favi.favi.pojo.request;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kovac on 29.11.2017.
 * My SKYPE: maxvinnik
 */

public class LikesForVideos {

    private List<Integer> videoIds;

    public LikesForVideos() {
        videoIds = new ArrayList<>();
    }

    public List<Integer> getVideoIds() {
        return videoIds;
    }

    public void setVideoIds(List<Integer> videoIds) {
        this.videoIds = videoIds;
    }
}
