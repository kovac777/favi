package com.favi.favi.pojo.response;

/**
 * Created by kovac on 03.11.2017.
 * My SKYPE: maxvinnik
 */

public class RestoreVideo {

    /**
     * restored : true
     */

    private boolean restored;

    public boolean isRestored() {
        return restored;
    }

    public void setRestored(boolean restored) {
        this.restored = restored;
    }
}
