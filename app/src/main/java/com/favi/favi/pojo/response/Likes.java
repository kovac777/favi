package com.favi.favi.pojo.response;

/**
 * Created by kovac on 03.11.2017.
 * My SKYPE: maxvinnik
 */

public class Likes {

    /**
     * likes : 2
     */

    private int likes;

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }
}
