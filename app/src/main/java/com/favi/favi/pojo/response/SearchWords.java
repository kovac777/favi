package com.favi.favi.pojo.response;

import java.util.List;

/**
 * Created by kovac on 03.11.2017.
 * My SKYPE: maxvinnik
 */

public class SearchWords {


    /**
     * words : [{"_id":"290f17e3-e2d7-492d-8412-adbfaa947dcd","text":"can","count":1},{"_id":"cf537ca8-cc30-4592-8f41-ffdc31b644b7","text":"can't","count":1},{"_id":"16c33a82-eadb-4098-af97-743cbb8d0aac","text":"american","count":1},{"_id":"9571845b-3d55-4555-afc2-889d5a693eff","text":"americans","count":1},{"_id":"1f53732a-acba-4e20-a35b-c298d757835b","text":"republicans","count":1}]
     * allCount : 10
     */

    private int allCount;
    private List<WordsBean> words;

    public int getAllCount() {
        return allCount;
    }

    public void setAllCount(int allCount) {
        this.allCount = allCount;
    }

    public List<WordsBean> getWords() {
        return words;
    }

    public void setWords(List<WordsBean> words) {
        this.words = words;
    }

    public static class WordsBean {
        /**
         * _id : 290f17e3-e2d7-492d-8412-adbfaa947dcd
         * text : can
         * count : 1
         */

        private String _id;
        private String text;
        private int count;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }
}
