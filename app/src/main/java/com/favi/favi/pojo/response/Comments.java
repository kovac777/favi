package com.favi.favi.pojo.response;

import com.favi.favi.adapter.recyclerview.BaseRVAdapter;

import java.util.List;

/**
 * Created by User on 20.01.2018.
 * My SKYPE: maxvinnik
 */

public class Comments {

    /**
     * count : 2
     * comments : [{"commentId":6,"text":"hello favi2","date":"2018-01-10T02:47:33.283","myComment":false,"user":{"userId":7,"image":"http://favi.web.local/","userName":"Taiseer","email":null,"date":"2018-01-09T23:52:45.577"}},{"commentId":5,"text":"hello favi","date":"2018-01-10T02:23:01.43","myComment":false,"user":{"userId":7,"image":"http://favi.web.local/","userName":"Taiseer","email":null,"date":"2018-01-09T23:52:45.577"}}]
     */

    private int count;
    private List<CommentsBean> comments;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<CommentsBean> getComments() {
        return comments;
    }

    public void setComments(List<CommentsBean> comments) {
        this.comments = comments;
    }

    public static class CommentsBean implements BaseRVAdapter.ItemViewType {
        /**
         * commentId : 6
         * text : hello favi2
         * date : 2018-01-10T02:47:33.283
         * myComment : false
         * user : {"userId":7,"image":"http://favi.web.local/","userName":"Taiseer","email":null,"date":"2018-01-09T23:52:45.577"}
         */

        private int commentId;
        private String text;
        private String date;
        private boolean myComment;
        private UserBean user;

        public int getCommentId() {
            return commentId;
        }

        public void setCommentId(int commentId) {
            this.commentId = commentId;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public boolean isMyComment() {
            return myComment;
        }

        public void setMyComment(boolean myComment) {
            this.myComment = myComment;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        @Override
        public BaseRVAdapter.ViewType getType() {
            return BaseRVAdapter.ViewType.COMMENT;
        }

        public static class UserBean {
            /**
             * userId : 7
             * image : http://favi.web.local/
             * userName : Taiseer
             * email : null
             * date : 2018-01-09T23:52:45.577
             */

            private int userId;
            private String image;
            private String userName;
            private String email;
            private String date;

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }
        }
    }
}
