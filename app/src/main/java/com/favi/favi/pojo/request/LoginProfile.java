package com.favi.favi.pojo.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by maxvinnik on 13.01.2018.
 */

public class LoginProfile implements Parcelable {

    public static final String FACEBOOK_PROVIDER = "Facebook";
    public static final String GOOGLE_PROVIDER = "Google";
    public static final String TWITTER_PROVIDER = "Twitter";
    public static final Creator<LoginProfile> CREATOR = new Creator<LoginProfile>() {
        @Override
        public LoginProfile createFromParcel(Parcel in) {
            return new LoginProfile(in);
        }

        @Override
        public LoginProfile[] newArray(int size) {
            return new LoginProfile[size];
        }
    };
    private String userName;
    private String provider;
    private String externalAccessToken;
    private String email;
    private String image;

    public LoginProfile(String userName, String provider, String externalAccessToken,
                        String email, String image) {
        this.userName = userName;
        this.provider = provider;
        this.externalAccessToken = externalAccessToken;
        this.email = email;
        this.image = image;
    }

    protected LoginProfile(Parcel in) {
        userName = in.readString();
        provider = in.readString();
        externalAccessToken = in.readString();
        email = in.readString();
        image = in.readString();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getExternalAccessToken() {
        return externalAccessToken;
    }

    public void setExternalAccessToken(String externalAccessToken) {
        this.externalAccessToken = externalAccessToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(userName);
        parcel.writeString(provider);
        parcel.writeString(externalAccessToken);
        parcel.writeString(email);
        parcel.writeString(image);
    }
}
