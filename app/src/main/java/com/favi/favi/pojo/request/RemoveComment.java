package com.favi.favi.pojo.request;

/**
 * Created by User on 20.01.2018.
 * My SKYPE: maxvinnik
 */

public class RemoveComment {

    private int commentId;

    public RemoveComment(int commentId) {
        this.commentId = commentId;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }
}
