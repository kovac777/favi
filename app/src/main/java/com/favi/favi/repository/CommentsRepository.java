package com.favi.favi.repository;

import android.content.SharedPreferences;

import com.favi.favi.api.ServiceGenerator;
import com.favi.favi.db.AppDatabase;
import com.favi.favi.pojo.request.AddComment;
import com.favi.favi.pojo.request.EditComment;
import com.favi.favi.pojo.request.RemoveComment;
import com.favi.favi.pojo.response.Comments;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by maxvinnik on 19.01.2018.
 */

@Singleton
public class CommentsRepository {

    private ServiceGenerator serviceGenerator;
    private AppDatabase appDatabase;
    private SharedPreferences sharedPreferences;

    @Inject
    public CommentsRepository(ServiceGenerator serviceGenerator, AppDatabase appDatabase,
                              SharedPreferences sharedPreferences) {
        this.serviceGenerator = serviceGenerator;
        this.appDatabase = appDatabase;
        this.sharedPreferences = sharedPreferences;
    }

    public Single<Comments> getComments(int videoId, int skip) {
        return serviceGenerator.commentsApi()
                .getVideoComments(videoId, skip, ServiceGenerator.TAKE_PAGINATION_COUNT);
    }

    public Single<Comments.CommentsBean> postComment(String authorization, AddComment addComment) {
        return serviceGenerator.commentsApi().addComment(authorization, addComment);
    }

    public Single<Comments.CommentsBean> editComment(String authorization, EditComment editComment) {
        return serviceGenerator.commentsApi().editComment(authorization, editComment);
    }

    public Completable removeComment(String authorization, RemoveComment removeComment) {
        return serviceGenerator.commentsApi().removeComment(authorization, removeComment);
    }
}
