package com.favi.favi.repository;

import android.content.SharedPreferences;

import com.favi.favi.adapter.recyclerview.FeedRVAdapter;
import com.favi.favi.api.ServiceGenerator;
import com.favi.favi.db.AppDatabase;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.pojo.request.GenerateFav;
import com.favi.favi.pojo.request.Like;
import com.favi.favi.pojo.request.LikesForVideos;
import com.favi.favi.pojo.response.Character;
import com.favi.favi.pojo.response.FFmpegCommand;
import com.favi.favi.pojo.response.Likes;
import com.favi.favi.pojo.response.SearchWords;
import com.favi.favi.util.Constants;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by kovac on 08.11.2017.
 * My SKYPE: maxvinnik
 */

@Singleton
public class FavRepository {

    public static final int SEARCH_WORD_COUNT = 100;

    private ServiceGenerator serviceGenerator;
    private AppDatabase appDatabase;
    private SharedPreferences sharedPreferences;

    private List<Feed> list;

    @Inject
    public FavRepository(ServiceGenerator serviceGenerator,
                         AppDatabase appDatabase,
                         SharedPreferences sharedPreferences) {
        this.serviceGenerator = serviceGenerator;
        this.appDatabase = appDatabase;
        this.sharedPreferences = sharedPreferences;
    }

    public Single<List<Feed>> getFeed(int feedType, int userId, int skip) {
        if (feedType == FeedRVAdapter.FeedType.MY_FAVS.getValue()) {
            return appDatabase.feedDAO().getMyFavs(skip);
        } else {
            return serviceGenerator.feedApi()
                    .getFeed(sharedPreferences.getString(Constants.PREFS_UUID_KEY, null),
                            feedType, skip, ServiceGenerator.TAKE_PAGINATION_COUNT, userId);
        }
    }

    private Single<List<Likes>> getLikesForVideos(List<Feed> list) {
        this.list = list;
        LikesForVideos request = new LikesForVideos();
        for (int i = 0; i < list.size(); i++) {
            request.getVideoIds().add(list.get(i).getId());
        }

        return Single.create(e -> serviceGenerator.feedApi().getLikesForVideos(request)
                .subscribeOn(Schedulers.io())
                .subscribe(e::onSuccess, throwable -> {
                    e.onError(throwable);
                    throwable.printStackTrace();
                }));
    }

    public Single<Likes> postLike(int videoId, int userId) {
        return serviceGenerator.feedApi()
                .postLike(new Like(videoId, sharedPreferences.getString(Constants.PREFS_UUID_KEY, null), userId));
    }

    public Single<SearchWords> getWords(String text, int tagId) {
        return serviceGenerator.newFavApi().getWords(text, tagId, SEARCH_WORD_COUNT);
    }

    public Single<FFmpegCommand> getFFmpegCommand(String text, int tag) {
        GenerateFav request = new GenerateFav(
                text, tag, sharedPreferences.getString(Constants.PREFS_UUID_KEY, null));
        return serviceGenerator.newFavApi().getFFmpegCommand(request);
    }

    public Single<Feed> postInLab(Feed feed) {
        MultipartBody.Part part = MultipartBody.Part.createFormData("", feed.getName(),
                RequestBody.create(MediaType.parse("video/mp4"), new File(feed.getLocalPath())));
        return serviceGenerator.feedApi()
                .postInLab(sharedPreferences.getString(Constants.PREFS_UUID_KEY, null),
                        feed.getTagId(), feed.getName(), feed.getUserId(), part);
    }

    public Single<Long> saveMyFavToDB(Feed myFav) {
        return Single.fromCallable(() -> appDatabase.feedDAO().addFav(myFav))
                .subscribeOn(Schedulers.io());
    }

    public Single<Integer> updateMyFavInDB(Feed feed) {
        return Single.fromCallable(() -> appDatabase.feedDAO().updateFav(feed))
                .subscribeOn(Schedulers.io());
    }

    public Single<List<Feed>> getMyFavFromDB(long uid) {
        return appDatabase.feedDAO().getFavByUID(uid);
    }

    public Single<Response<ResponseBody>> downloadVideo(String path) {
        return serviceGenerator.feedApi().downloadVideo(ServiceGenerator.FEED_URL + "/" + path);
    }

    public Completable postReport(int vidId) {
        return serviceGenerator.feedApi()
                .postReport(sharedPreferences.getString(Constants.PREFS_UUID_KEY, null), vidId);
    }

    public Single<List<Character>> getCharacters() {
        return serviceGenerator.newFavApi().getCharacters();
    }
}
