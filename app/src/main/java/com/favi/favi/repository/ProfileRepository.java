package com.favi.favi.repository;

import android.content.SharedPreferences;

import com.favi.favi.api.ServiceGenerator;
import com.favi.favi.db.AppDatabase;
import com.favi.favi.db.entity.ExternalAccessToken;
import com.favi.favi.db.entity.LocalProfile;
import com.favi.favi.pojo.request.FirebaseToken;
import com.favi.favi.pojo.request.LoginProfile;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by maxvinnik on 13.01.2018.
 */

@Singleton
public class ProfileRepository {

    private ServiceGenerator serviceGenerator;
    private AppDatabase appDatabase;
    private SharedPreferences sharedPreferences;

    @Inject
    public ProfileRepository(ServiceGenerator serviceGenerator,
                             AppDatabase appDatabase,
                             SharedPreferences sharedPreferences) {
        this.serviceGenerator = serviceGenerator;
        this.appDatabase = appDatabase;
        this.sharedPreferences = sharedPreferences;
    }

    public Flowable<List<LocalProfile>> getProfile() {
        return appDatabase.profileDAO().getFlowableProfile();
    }

    public Single<List<LocalProfile>> getSingleProfile() {
        return appDatabase.profileDAO().getSingleProfile();
    }

    public Completable deleteProfileFromDB() {
        return appDatabase.profileDAO().getSingleProfile()
                .flatMapCompletable(localProfiles ->
                        Completable.fromAction(() -> appDatabase.profileDAO().deleteProfile(localProfiles.get(0))));
    }

    public Single<LocalProfile> loginProfile(LoginProfile loginProfile) {
        return serviceGenerator.profileApi().login(loginProfile)
                .flatMap(localProfile -> saveProfileToDB(localProfile).toSingleDefault(localProfile));
    }

    public Completable saveProfileToDB(LocalProfile localProfile) {
        return Completable.fromAction(() -> appDatabase.profileDAO().saveProfile(localProfile));
    }

    public Single<ExternalAccessToken> getExternalAccessToken() {
        return appDatabase.externalAccessTokenDAO().getExternalAccessToken();
    }

    public Completable saveExternalAccessTokenToDB(ExternalAccessToken externalAccessToken) {
        return Completable.fromAction(() -> appDatabase.externalAccessTokenDAO().saveToken(externalAccessToken));
    }

    public Completable editProfile(LocalProfile profile) {
        return serviceGenerator.profileApi()
                .editProfile(profile.getToken_type() + " " + profile.getAccess_token(), profile)
                .flatMapCompletable(this::saveProfileToDB);
    }

    public Single<LocalProfile> obtainToken(String provider, String externalAccessToken) {
        return serviceGenerator.profileApi().obtainAccessToken(provider, externalAccessToken)
                .flatMap(localProfile -> saveProfileToDB(localProfile).toSingleDefault(localProfile));
    }

    public Completable pushFirebaseToken(FirebaseToken firebaseToken) {
        return serviceGenerator.profileApi().pushFirebaseToken(firebaseToken);
    }
}
