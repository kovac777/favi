package com.favi.favi.util;

import android.content.Context;

import com.favi.favi.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Hours;
import org.joda.time.LocalDateTime;
import org.joda.time.Minutes;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by kovac on 15.11.2017.
 * My SKYPE: maxvinnik
 */

public class TimeUtil {

    public static final int UNDEFINED = -1;

    public static String getAddedTime(Context context, String date) {
        DateTime now = DateTime.now();

        int splitIndex = date.indexOf(".");
        if (splitIndex != UNDEFINED) {
            date = date.substring(0, splitIndex);
        }

        DateTime addedTime;

        addedTime = LocalDateTime.parse(date,
                DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss"))
                .toDateTime().withZoneRetainFields(DateTimeZone.UTC)
                .withZone(DateTimeZone.getDefault());

        int justNow = Minutes.minutesBetween(addedTime, now).getMinutes();
        if (justNow < 1) {
            return context.getString(R.string.just_now);
        }

        int minutesAgo = Minutes.minutesBetween(addedTime, now).getMinutes();
        if (minutesAgo < 59) {
            return minutesAgo + " " + context.getString(R.string.minutes_ago);
        }

        int hoursAgo = Hours.hoursBetween(addedTime, now).getHours();
        if (hoursAgo < 24) {
            return hoursAgo + " " + context.getString(R.string.hours_ago);
        }

        int yearsAgo = Years.yearsBetween(addedTime, now).getYears();
        if (yearsAgo < 1) {
            return addedTime.dayOfMonth().get() + " " + addedTime.monthOfYear().getAsText(Locale.getDefault());
        } else
            return addedTime.dayOfMonth().get() + "." + addedTime.monthOfYear().get() + "." + addedTime.year().get();
    }

    public static String getAddedTime() {
        long currentMillis = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date resultDate = new Date(currentMillis);
        return sdf.format(resultDate);
    }

    public static boolean isAccessTokenExpired(String expired) {
        expired = expired.substring(0, expired.lastIndexOf(" "));
        DateTime expiredDateTime = LocalDateTime.parse(expired,
                DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss"))
                .toDateTime().withZoneRetainFields(DateTimeZone.UTC)
                .withZone(DateTimeZone.getDefault());

        return !expiredDateTime.isAfterNow();
    }

    public static boolean isRateAppNeed(String previousRateMillis) {
        if (previousRateMillis == null) {
            return true;
        }

        return DateTime.now().minus(Long.parseLong(previousRateMillis)).dayOfMonth().get() > 1;
    }
}
