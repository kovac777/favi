package com.favi.favi.util;

/**
 * Created by Max on 26.03.2017.
 * My SKYPE: maxvinnik
 */

public class Constants {

    public static final String PREFS_UUID_KEY = "prefsUUIDKey";
    public static final String PREFS_PREVIOUS_INPUT_TEXT = "prefsPreviousInputText";
    public static final String PREFS_IS_LOGGED_IN = "prefsIsLoggedIn";
    public static final String PREFS_LAST_RATE_MILLIS = "prefsIsRateApp";

    public static final String ROBOT_FAVI_NAME = "Robot FaVi";
    public static final String ROBOT_FAVI_IMAGE = "http://api.faviapp.com/images/users/robot.jpg";

    public static final String YANDEX_METRICS_API_KEY = "4a552d63-027f-4dd4-90b5-e0b64dfb113e";
    public static final String APPS_FLYER_KEY = "2uJwKVpaWvkqb59VxVrxtE";

    public static final String FILE_PROVIDER_AUTHORITES = "com.favi.favi.fileprovider";

    public static final String LINK_FACEBOOK = "https://www.facebook.com/faviapp/";
    public static final String LINK_INSTAGRAM = "https://www.instagram.com/faviapp/";
    public static final String LINK_TWITTER = "https://twitter.com/RobotFavi";
    public static final String LINK_YOUTUBE = "https://www.youtube.com/channel/UCeQklC3PbBNHxHXyD3rMRxg?view_as=subscriber";

    public static final String PACKAGE_INSTAGRAM = "com.instagram.android";
    public static final String PACKAGE_FACEBOOK = "com.facebook.katana";
    public static final String PACKAGE_TWITTER = "com.twitter.android";
}
