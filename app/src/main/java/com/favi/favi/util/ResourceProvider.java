package com.favi.favi.util;

import android.content.Context;

/**
 * Created by User on 06.12.2017.
 * My SKYPE: maxvinnik
 */

public class ResourceProvider {

    private Context context;

    public ResourceProvider(Context context) {
        this.context = context;
    }

    public String getString(int resId) {
        return context.getResources().getString(resId);
    }

    public Context getContext() {
        return context;
    }
}
