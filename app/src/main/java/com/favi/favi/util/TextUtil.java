package com.favi.favi.util;

import android.content.Context;

import com.favi.favi.api.ServiceGenerator;
import com.favi.favi.db.entity.Feed;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kovac on 20.11.2017.
 * My SKYPE: maxvinnik
 */

public class TextUtil {

    public static String[] getFFmpegCommandWithOutput(Context context, String ffmpegCommand, String favName) {
        favName = favName.trim();
        favName = favName.replaceAll(" ", "_");
        favName = favName + System.currentTimeMillis() + ".mp4";

        String outputPath = FileUtil.getFavFile(context, favName).getPath();

        String commandWithOutput = ffmpegCommand.replaceAll("\"", "");
        commandWithOutput = commandWithOutput.replace("{0}", outputPath);

        return commandWithOutput.split("\\s+");
    }

    public static CharSequence getVideoName(Feed feed) {
        String fullName = feed.getName();
        fullName = fullName.replaceAll("\"", "");
        int indexBeginExtension = fullName.lastIndexOf(".");
        return fullName.substring(0, 1).toUpperCase() +
                fullName.substring(1, indexBeginExtension == -1 ? fullName.length() : indexBeginExtension);
    }

    public static String getStreamVideoLink(String remoteVideoPath) {
        int indexStartExtension = remoteVideoPath.lastIndexOf(".");

        String streamVideoLink = remoteVideoPath.substring(0, indexStartExtension);
        streamVideoLink = streamVideoLink.replace(ServiceGenerator.FEED_URL +
                "/Videos\\output\\", "");
        streamVideoLink = streamVideoLink.replaceAll("\\\\", "");

        String s = ServiceGenerator.BASE_URL + "video/get?filename=" + streamVideoLink;
        return s;
    }

    public static boolean validateEmail(String email) {
        String EMAIL_REGEX = "^[\\w-+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
