package com.favi.favi.util;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;

/**
 * Created by kovac on 20.11.2017.
 * My SKYPE: maxvinnik
 */

public class FileUtil {

    public static boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    @NonNull
    public static File getFavFile(Context context, String favName) {
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_MOVIES), "FAVI");
        if (!file.mkdirs()) {
            Log.e("logi", "Directory not created");
        }
        return new File(file, favName);
    }

    @NonNull
    public static File getCacheFavFile(Context context, String favName) {
        File file = new File(context.getExternalCacheDir(), "FAVI");
        if (!file.mkdirs()) {
            Log.e("logi", "Directory not created");
        }
        return new File(file, favName);
    }
}
