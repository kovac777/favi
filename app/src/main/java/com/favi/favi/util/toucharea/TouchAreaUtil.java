package com.favi.favi.util.toucharea;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Created by kovac on 04.11.2017.
 * My SKYPE: maxvinnik
 */

public class TouchAreaUtil {

    /**
     * Helper method for increase touchable area of view(s).
     * <p>WARNING! If you pass several count of views, then pass views that have ONLY the same parentView.</p>
     *
     * @param paddingDp padding of top, left, bottom, right.
     * @param view      which need to increase touch area.
     */
    public static void extendTouchArea(int paddingDp, @NonNull final View... view) {
        TouchDelegateComposite touchDelegateComposite = new TouchDelegateComposite(view[0]);
        final Resources resources = view[0].getResources();
        final int padding = (int) (paddingDp * resources.getDisplayMetrics().density + 0.5f);

        for (int i = 0; i < view.length; i++) {
            int index = i;

            if (view[index] == null)
                continue;

            view[i].getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    final Rect touchArea = new Rect();
                    view[index].getHitRect(touchArea);
                    touchArea.top -= padding;
                    touchArea.bottom += padding;
                    touchArea.left -= padding;
                    touchArea.right += padding;

                    touchDelegateComposite.addDelegate(new TouchDelegate(touchArea, view[index]));

                    view[index].getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }

        final View parent = ((View) view[0].getParent());
        parent.setTouchDelegate(touchDelegateComposite);
    }

    public static void extendTouchAreaLeft(int paddingDp, View view) {
        TouchDelegateComposite touchDelegateComposite = new TouchDelegateComposite(view);
        final Resources resources = view.getResources();
        final int padding = (int) (paddingDp * resources.getDisplayMetrics().density + 0.5f);

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final Rect touchArea = new Rect();
                view.getHitRect(touchArea);
                touchArea.left -= padding;

                touchDelegateComposite.addDelegate(new TouchDelegate(touchArea, view));

                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        final View parent = ((View) view.getParent());
        parent.setTouchDelegate(touchDelegateComposite);
    }

    public static void extendTouchAreaRight(int paddingDp, View view) {
        TouchDelegateComposite touchDelegateComposite = new TouchDelegateComposite(view);
        final Resources resources = view.getResources();
        final int padding = (int) (paddingDp * resources.getDisplayMetrics().density + 0.5f);

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final Rect touchArea = new Rect();
                view.getHitRect(touchArea);
                touchArea.right += padding;

                touchDelegateComposite.addDelegate(new TouchDelegate(touchArea, view));

                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        final View parent = ((View) view.getParent());
        parent.setTouchDelegate(touchDelegateComposite);
    }

    public static void extendTouchAreaBottom(int paddingDp, View view) {
        TouchDelegateComposite touchDelegateComposite = new TouchDelegateComposite(view);
        final Resources resources = view.getResources();
        final int padding = (int) (paddingDp * resources.getDisplayMetrics().density + 0.5f);

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final Rect touchArea = new Rect();
                view.getHitRect(touchArea);
                touchArea.bottom += padding;

                touchDelegateComposite.addDelegate(new TouchDelegate(touchArea, view));

                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        final View parent = ((View) view.getParent());
        parent.setTouchDelegate(touchDelegateComposite);
    }

    public static void extendTouchAreaTop(int paddingDp, View view) {
        TouchDelegateComposite touchDelegateComposite = new TouchDelegateComposite(view);
        final Resources resources = view.getResources();
        final int padding = (int) (paddingDp * resources.getDisplayMetrics().density + 0.5f);

        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final Rect touchArea = new Rect();
                view.getHitRect(touchArea);
                touchArea.top -= padding;

                touchDelegateComposite.addDelegate(new TouchDelegate(touchArea, view));

                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        final View parent = ((View) view.getParent());
        parent.setTouchDelegate(touchDelegateComposite);
    }
}
