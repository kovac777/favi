package com.favi.favi.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;

/**
 * Created by kovac on 20.11.2017.
 * My SKYPE: maxvinnik
 */

public class PermissionUtil {

    public static final int LOCATION_PERMISSION_REQUEST_CODE = 100;
    public static final int STORAGE_PERMISSION_REQUEST = 200;
    private static final String ACCESS_FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String ACCESS_COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private static final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;

    public static boolean hasLocationPermission(Context context) {
        return (ActivityCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED);
    }

    public static void requestLocationPermission(Fragment fragment) {
        String locationPermissions[] = new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};
        fragment.requestPermissions(locationPermissions, LOCATION_PERMISSION_REQUEST_CODE);
    }

    public static void requestLocationPermission(Activity activity) {
        String locationPermissions[] = new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};
        ActivityCompat.requestPermissions(activity, locationPermissions, LOCATION_PERMISSION_REQUEST_CODE);
    }

    public static boolean hasStoragePermission(Context context) {
        return (ActivityCompat.checkSelfPermission(context, READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED);
    }

    public static void requestStoragePermission(Fragment fragment) {
        String[] storagePermissions = new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE};
        fragment.requestPermissions(storagePermissions, STORAGE_PERMISSION_REQUEST);
    }

    public static void requestStoragePermission(Activity activity) {
        String[] storagePermissions = new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE};
        ActivityCompat.requestPermissions(activity, storagePermissions, STORAGE_PERMISSION_REQUEST);
    }
}
