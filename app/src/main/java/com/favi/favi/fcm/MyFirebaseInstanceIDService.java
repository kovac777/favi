package com.favi.favi.fcm;

import android.content.SharedPreferences;

import com.favi.favi.pojo.request.FirebaseToken;
import com.favi.favi.repository.ProfileRepository;
import com.favi.favi.util.Constants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

/**
 * Created by User on 04.12.2017.
 * My SKYPE: maxvinnik
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Inject
    ProfileRepository profileRepository;
    @Inject
    SharedPreferences sharedPreferences;

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
    }

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        profileRepository.pushFirebaseToken(new FirebaseToken(
                sharedPreferences.getString(Constants.PREFS_UUID_KEY, null), refreshedToken));
    }
}
