package com.favi.favi.livedata;

import android.arch.lifecycle.LiveData;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by kovac on 09.11.2017.
 * My SKYPE: maxvinnik
 */

public class NetworkLiveData extends LiveData<Boolean> {

    private Context context;
    private BroadcastReceiver receiver;

    private boolean isConnected;

    public NetworkLiveData(Context context) {
        this.context = context;
    }

    @SuppressWarnings("ConstantConditions")
    private void prepareReceiver(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();

                setValue(isConnected);
            }
        };

        context.registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onActive() {
        prepareReceiver(context);
    }

    @Override
    protected void onInactive() {
        context.unregisterReceiver(receiver);
        receiver = null;
    }
}
