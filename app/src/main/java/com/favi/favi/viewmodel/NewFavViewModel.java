package com.favi.favi.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.favi.favi.app.FaViApp;
import com.favi.favi.pojo.response.SearchWords;
import com.favi.favi.repository.FavRepository;
import com.favi.favi.viewmodel.interact.FavInteractor;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kovac on 08.11.2017.
 * My SKYPE: maxvinnik
 */

public class NewFavViewModel extends BaseViewModel {

    private FavRepository favRepository;
    private FavInteractor favInteractor;
    private Disposable disposable;

    private MutableLiveData<Integer> tag;
    private MutableLiveData<SearchWords> searchWords;

    @Inject
    public NewFavViewModel(FaViApp context, FavRepository favRepository, FavInteractor favInteractor) {
        super(context);
        this.favRepository = favRepository;
        this.favInteractor = favInteractor;

        tag = new MutableLiveData<>();
        searchWords = new MutableLiveData<>();
    }

    public LiveData<SearchWords> searchWords(String text, int tag) {
        this.tag.setValue(tag);

        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();

        disposable = favRepository.getWords(text, tag)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(searchWords::setValue, this::handleError);

        return searchWords;
    }

    public LiveData<Long> generateFavAndSaveToDB(String text, int tag) {
        MutableLiveData<Long> newFavUID = new MutableLiveData<>();

        favInteractor.generateFavAndSaveToDB(text, tag)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(newFavUID::setValue, throwable -> {
                    throwable.printStackTrace();
                    handleError(throwable);
                    resetUI.setValue(true);
                });

        return newFavUID;
    }

    public LiveData<Integer> getTag() {
        return tag;
    }
}
