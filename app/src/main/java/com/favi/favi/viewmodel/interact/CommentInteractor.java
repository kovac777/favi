package com.favi.favi.viewmodel.interact;

import com.favi.favi.db.entity.LocalProfile;
import com.favi.favi.pojo.request.AddComment;
import com.favi.favi.pojo.request.EditComment;
import com.favi.favi.pojo.request.RemoveComment;
import com.favi.favi.pojo.response.Comments;
import com.favi.favi.repository.CommentsRepository;
import com.favi.favi.repository.ProfileRepository;
import com.favi.favi.util.TimeUtil;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by User on 20.01.2018.
 * My SKYPE: maxvinnik
 */

public class CommentInteractor {

    private ProfileRepository profileRepository;
    private CommentsRepository commentsRepository;

    @Inject
    public CommentInteractor(ProfileRepository profileRepository, CommentsRepository commentsRepository) {
        this.profileRepository = profileRepository;
        this.commentsRepository = commentsRepository;
    }

    public Single<Comments.CommentsBean> postComment(AddComment addComment) {
        return profileRepository.getSingleProfile()
                .flatMap(localProfiles -> {
                    if (TimeUtil.isAccessTokenExpired(localProfiles.get(0).getExpires())) {
                        return obtainToken()
                                .flatMap(localProfile ->
                                        commentsRepository.postComment(
                                                localProfile.getToken_type() + " " +
                                                        localProfile.getAccess_token(), addComment));
                    } else {
                        return commentsRepository.postComment(
                                localProfiles.get(0).getToken_type() + " " +
                                        localProfiles.get(0).getAccess_token(), addComment);
                    }
                });
    }

    public Single<Comments.CommentsBean> editComment(EditComment editComment) {
        return profileRepository.getSingleProfile()
                .flatMap(localProfiles -> {
                    if (TimeUtil.isAccessTokenExpired(localProfiles.get(0).getExpires())) {
                        return obtainToken()
                                .flatMap(localProfile ->
                                        commentsRepository.editComment(
                                                localProfile.getToken_type() + " " +
                                                        localProfile.getAccess_token(), editComment));
                    } else {
                        return commentsRepository.editComment(
                                localProfiles.get(0).getToken_type() + " " +
                                        localProfiles.get(0).getAccess_token(), editComment);
                    }
                });
    }

    public Completable removeComment(RemoveComment removeComment) {
        return profileRepository.getSingleProfile()
                .flatMapCompletable(localProfiles -> {
                    if (TimeUtil.isAccessTokenExpired(localProfiles.get(0).getExpires())) {
                        return obtainToken()
                                .flatMapCompletable(localProfile ->
                                        commentsRepository.removeComment(
                                                localProfile.getToken_type() + " " +
                                                        localProfile.getAccess_token(), removeComment));
                    } else {
                        return commentsRepository.removeComment(
                                localProfiles.get(0).getToken_type() + " " +
                                        localProfiles.get(0).getAccess_token(), removeComment);
                    }
                });
    }

    private Single<LocalProfile> obtainToken() {
        return profileRepository.getExternalAccessToken()
                .flatMap(externalAccessToken -> profileRepository.obtainToken(
                        externalAccessToken.getProvider(),
                        externalAccessToken.getToken()));
    }
}
