package com.favi.favi.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.favi.favi.app.FaViApp;
import com.favi.favi.pojo.response.Character;
import com.favi.favi.repository.FavRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by maxvinnik on 04.01.2018.
 */

public class SelectCharacterViewModel extends BaseViewModel {

    private FavRepository favRepository;

    @Inject
    public SelectCharacterViewModel(FaViApp context, FavRepository favRepository) {
        super(context);
        this.favRepository = favRepository;
    }

    public LiveData<List<Character>> getCharacters() {
        MutableLiveData<List<Character>> data = new MutableLiveData<>();

        favRepository.getCharacters()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data::setValue, this::handleError);

        return data;
    }
}
