package com.favi.favi.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.favi.favi.app.FaViApp;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.repository.FavRepository;
import com.favi.favi.viewmodel.interact.FavInteractor;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kovac on 15.11.2017.
 * My SKYPE: maxvinnik
 */

public class ShareViewModel extends BaseViewModel {

    Disposable disposable;
    private FavInteractor favInteractor;
    private FavRepository favRepository;

    @Inject
    public ShareViewModel(FaViApp context, FavInteractor favInteractor, FavRepository favRepository) {
        super(context);
        this.favInteractor = favInteractor;
        this.favRepository = favRepository;
    }

    public LiveData<Feed> regenerateFav(String text, int tag, Feed feed) {
        MutableLiveData<Feed> data = new MutableLiveData<>();

        disposable = favInteractor.regenerateFavAndUpdateInDB(text, tag, feed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data::setValue,
                        throwable -> {
                            handleError(throwable);
                            resetUI.setValue(true);
                            throwable.printStackTrace();
                        });

        return data;
    }

    public void cancelRegenerate() {
        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
    }

    public LiveData<Feed> getMyFavFromDB(long uid) {
        MutableLiveData<Feed> data = new MutableLiveData<>();

        favRepository.getMyFavFromDB(uid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(feeds -> data.setValue(feeds.get(0)),
                        throwable -> {
                            handleError(throwable);
                            resetUI.setValue(true);
                            throwable.printStackTrace();
                        });

        return data;
    }

    public LiveData<Feed> postFavInLab(Feed feed) {
        MutableLiveData<Feed> data = new MutableLiveData<>();

        favInteractor.postInLab(feed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data::setValue,
                        throwable -> {
                            handleError(throwable);
                            resetUI.setValue(true);
                            throwable.printStackTrace();
                        });

        return data;
    }

    public LiveData<Integer> updateFavInDB(Feed feed) {
        MutableLiveData<Integer> data = new MutableLiveData<>();

        favRepository.updateMyFavInDB(feed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data::setValue,
                        throwable -> {
                            handleError(throwable);
                            resetUI.setValue(true);
                            throwable.printStackTrace();
                        });

        return data;
    }
}
