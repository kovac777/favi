package com.favi.favi.viewmodel.interact;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.favi.favi.app.FaViApp;
import com.favi.favi.db.AppDatabase;
import com.favi.favi.db.entity.ExternalAccessToken;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.db.entity.LocalProfile;
import com.favi.favi.pojo.request.LoginProfile;
import com.favi.favi.pojo.response.FacebookAvatar;
import com.favi.favi.repository.ProfileRepository;
import com.favi.favi.ui.activity.LoginActivity;
import com.favi.favi.util.Constants;
import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by User on 16.01.2018.
 * My SKYPE: maxvinnik
 */

public class ProfileInteractor {

    private FaViApp context;
    private ProfileRepository profileRepository;
    private AppDatabase appDatabase;
    private SharedPreferences sharedPreferences;

    @Inject
    public ProfileInteractor(FaViApp context, ProfileRepository profileRepository,
                             AppDatabase appDatabase, SharedPreferences sharedPreferences) {
        this.context = context;
        this.profileRepository = profileRepository;
        this.appDatabase = appDatabase;
        this.sharedPreferences = sharedPreferences;
    }

    public void loginFB(CallbackManager callbackManager) {
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Profile.fetchProfileForCurrentAccessToken();

                ExternalAccessToken token = new ExternalAccessToken(LoginProfile.FACEBOOK_PROVIDER,
                        loginResult.getAccessToken().getToken());

                final LoginProfile[] login = new LoginProfile[1];

                profileRepository.saveExternalAccessTokenToDB(token).toSingleDefault(token)
                        .flatMap(this::getAvatar)
                        .flatMap(loginProfile -> {
                            login[0] = loginProfile;
                            return profileRepository.loginProfile(loginProfile);
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(localProfile -> {
                            sharedPreferences.edit()
                                    .putBoolean(Constants.PREFS_IS_LOGGED_IN, true).apply();

                            //update MyFavs with localProfile`s data
                            // for proper display author`s video
                            updateMyFavsWithNewData(localProfile);
                        }, throwable -> LoginActivity.start(context, login[0]));
            }

            private void updateMyFavsWithNewData(LocalProfile localProfile) {
                appDatabase.feedDAO().getAllMyFavs()
                        .map(feeds -> {
                            for (int i = 0; i < feeds.size(); i++) {
                                Feed feed = feeds.get(i);
                                feed.setLocalProfileData(localProfile);
                            }
                            return feeds;
                        })
                        .map(feeds -> appDatabase.feedDAO().updateFavs(feeds))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(feeds -> {
                            // TODO: 26.01.2018 send callback to SignInDialog for update recyclerView (make flowable)
                        });
            }

            @SuppressWarnings("unchecked")
            private Single<LoginProfile> getAvatar(ExternalAccessToken token) {
                return Single.create(e -> {
                    GraphRequest graphRequest =
                            GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                                    (object, response) -> {
                                        if (response.getError() != null)
                                            e.onError(response.getError().getException());
                                        else {
                                            FacebookAvatar facebookAvatar = new Gson().fromJson(
                                                    object.toString(), FacebookAvatar.class);

                                            Profile profileFacebook = Profile.getCurrentProfile();
                                            e.onSuccess(new LoginProfile(
                                                    profileFacebook.getName(),
                                                    token.getProvider(),
                                                    token.getToken(),
                                                    null, facebookAvatar.getPicture().getData().getUrl()));
                                        }
                                    });

                    Bundle params = new Bundle();
                    params.putString("fields", "picture.width(720).height(720)");
                    graphRequest.setParameters(params);
                    graphRequest.executeAndWait();
                });
            }

            @Override
            public void onCancel() {
                Log.d("logs", "onCancel: ");
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
            }
        });
    }
}
