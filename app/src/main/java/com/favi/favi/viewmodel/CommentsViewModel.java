package com.favi.favi.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.favi.favi.api.ServiceGenerator;
import com.favi.favi.app.FaViApp;
import com.favi.favi.pojo.request.AddComment;
import com.favi.favi.pojo.request.EditComment;
import com.favi.favi.pojo.request.RemoveComment;
import com.favi.favi.pojo.response.Comments;
import com.favi.favi.repository.CommentsRepository;
import com.favi.favi.viewmodel.interact.CommentInteractor;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by maxvinnik on 19.01.2018.
 */


public class CommentsViewModel extends BaseViewModel {

    private CommentInteractor commentInteractor;
    private CommentsRepository commentsRepository;

    private int skipVideoCount = 0;

    private MutableLiveData<Comments> comments;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Boolean> isLastPage;

    private MutableLiveData<Comments.CommentsBean> postComment;
    private MutableLiveData<Comments.CommentsBean> editComment;
    private MutableLiveData<Boolean> deleteComment;

    private boolean isFeedRefreshed;


    @Inject
    public CommentsViewModel(FaViApp context, CommentInteractor commentInteractor,
                             CommentsRepository commentsRepository) {
        super(context);
        this.commentInteractor = commentInteractor;
        this.commentsRepository = commentsRepository;

        comments = new MutableLiveData<>();
        isLoading = new MutableLiveData<>();
        isLastPage = new MutableLiveData<>();

        postComment = new MutableLiveData<>();
        editComment = new MutableLiveData<>();
        deleteComment = new MutableLiveData<>();
    }

    public LiveData<Comments> getComments(int videoId, boolean refreshComments) {
        isFeedRefreshed = refreshComments;

        isLoading.setValue(true);

        if (refreshComments) {
            isLastPage.setValue(false);
            skipVideoCount = 0;
        }

        commentsRepository.getComments(videoId, skipVideoCount)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(commentsResult -> {
                    isLoading.setValue(false);

                    if (commentsResult.getComments().size() < ServiceGenerator.TAKE_PAGINATION_COUNT)
                        isLastPage.setValue(true);

                    comments.setValue(commentsResult);

                    skipVideoCount = skipVideoCount + ServiceGenerator.TAKE_PAGINATION_COUNT;
                }, throwable -> {
                    isLoading.setValue(false);
                    handleError(throwable);
                });

        return comments;
    }

    public void post(int videoId, String text) {
        commentInteractor.postComment(new AddComment(videoId, text))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> showLoadIndicator.setValue(true))
                .doOnError(this::handleError)
                .doFinally(() -> showLoadIndicator.setValue(false))
                .subscribe(postComment::setValue);
    }

    public void edit(int commentId, String text) {
        commentInteractor.editComment(new EditComment(commentId, text))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> showLoadIndicator.setValue(true))
                .doOnError(this::handleError)
                .doFinally(() -> showLoadIndicator.setValue(false))
                .subscribe(editComment::setValue);
    }

    public void delete(int commentId) {
        commentInteractor.removeComment(new RemoveComment(commentId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> showLoadIndicator.setValue(true))
                .doOnError(this::handleError)
                .doFinally(() -> showLoadIndicator.setValue(false))
                .subscribe(() -> deleteComment.setValue(true));
    }

    public MutableLiveData<Comments.CommentsBean> postComment() {
        return postComment;
    }

    public MutableLiveData<Comments.CommentsBean> editComment() {
        return editComment;
    }

    public MutableLiveData<Boolean> deleteComment() {
        return deleteComment;
    }

    public boolean isFeedRefreshed() {
        return isFeedRefreshed;
    }

    public LiveData<Boolean> isLoading() {
        return isLoading;
    }

    public LiveData<Boolean> isLastPage() {
        return isLastPage;
    }
}
