package com.favi.favi.viewmodel.interact;

import android.support.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.FeedRVAdapter;
import com.favi.favi.app.FaViApp;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.ffmpeg.ExecuteResponseHandler;
import com.favi.favi.pojo.response.FFmpegCommand;
import com.favi.favi.pojo.response.Likes;
import com.favi.favi.repository.FavRepository;
import com.favi.favi.repository.ProfileRepository;
import com.favi.favi.util.FileUtil;
import com.favi.favi.util.TextUtil;
import com.favi.favi.util.TimeUtil;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleSource;
import io.reactivex.schedulers.Schedulers;
import okio.BufferedSink;
import okio.Okio;

/**
 * Created by User on 24.11.2017.
 * My SKYPE: maxvinnik
 */

@Singleton
public class FavInteractor {

    private FaViApp context;
    private FavRepository favRepository;
    private ProfileRepository profileRepository;

    private FFmpeg ffmpeg;

    @Inject
    public FavInteractor(FaViApp context, FavRepository favRepository,
                         ProfileRepository profileRepository, FFmpeg ffmpeg) {
        this.context = context;
        this.favRepository = favRepository;
        this.profileRepository = profileRepository;
        this.ffmpeg = ffmpeg;
    }

    public Single<Long> generateFavAndSaveToDB(String text, int tag) {
        return favRepository.getFFmpegCommand(text, tag)
                .flatMap(ffmpegCommand -> Single.create(e ->
                        generateVideoFromFFmpeg(text, tag, ffmpegCommand, e))
                        .flatMap(feed -> favRepository.saveMyFavToDB((Feed) feed)));
    }

    public Single<Feed> regenerateFavAndUpdateInDB(String text, int tag, Feed feed) {
        return favRepository.getFFmpegCommand(text, tag)
                .flatMap(ffmpegCommand -> Single.create(e ->
                        generateVideoFromFFmpeg(text, tag, ffmpegCommand, e))
                        .flatMap(newFeed -> updateFavInDBAndReturnFav(feed, (Feed) newFeed)));
    }

    public Single<Feed> postInLab(Feed feed) {
        return favRepository.postInLab(feed)
                .flatMap(feedFromServer -> updateFavInDBAndReturnFav(feed, feedFromServer));
    }

    public Single<Likes> postLike(int videoId) {
        return getUserId().flatMap(userId -> favRepository.postLike(videoId, userId));
    }

    private SingleSource<Feed> updateFavInDBAndReturnFav(Feed feed, Feed newFeed) {
        newFeed.setUid(feed.uid);
        newFeed.setLocalPath(feed.getLocalPath());
        newFeed.setUserId(feed.getUserId());
        newFeed.setUserName(feed.getUserName());
        newFeed.setUserDate(feed.getUserDate());
        newFeed.setImage(feed.getImage());
        newFeed.setPosting(false);
        newFeed.setAdapterPosition(feed.getAdapterPosition());


        return favRepository.updateMyFavInDB(newFeed)
                .flatMap(integer -> Single.create(e -> e.onSuccess(newFeed)));
    }

    private void generateVideoFromFFmpeg(String text, int tagId, FFmpegCommand ffmpegCommand, SingleEmitter<Object> e) throws FFmpegCommandAlreadyRunningException {
        String[] commandWithOutputPath = TextUtil
                .getFFmpegCommandWithOutput(context, ffmpegCommand.getFfmpeg(), text);

        if (ffmpeg.isFFmpegCommandRunning()) {
            ffmpeg.killRunningProcesses();
        }

        ffmpeg.execute(commandWithOutputPath, new ExecuteResponseHandler() {
            @Override
            public void onSuccess(String message) {
                profileRepository.getSingleProfile()
                        .subscribeOn(Schedulers.io())
                        .subscribe(localProfiles -> {
                            String videoPath = commandWithOutputPath[commandWithOutputPath.length - 1];

                            Feed feed = new Feed();
                            feed.setDate(TimeUtil.getAddedTime());
                            feed.setLocalPath(videoPath);
                            feed.setName(text.trim());
                            feed.setTagId(tagId);

                            if (localProfiles.size() > 0) {
                                feed.setLocalProfileData(localProfiles.get(0));
                            }

                            e.onSuccess(feed);
                        }, e::onError);
            }

            @Override
            public void onFailure(String message) {
                e.onError(new Throwable(message));
            }
        });
    }

    public Single<File> downloadFav(Feed feed) {
        return favRepository.downloadVideo(feed.getPath())
                .flatMap(responseBodyResponse -> Single.create(e -> {
                    if (responseBodyResponse.isSuccessful() && responseBodyResponse.body() != null) {
                        try {
                            File fav = FileUtil.getCacheFavFile(context, feed.getName() + ".mp4");
                            BufferedSink bufferedSink = Okio.buffer(Okio.sink(fav));
                            //noinspection ConstantConditions
                            bufferedSink.writeAll(responseBodyResponse.body().source());
                            bufferedSink.flush();
                            bufferedSink.close();
                            e.onSuccess(fav);
                        } catch (IOException exception) {
                            exception.printStackTrace();
                            e.onError(exception);
                        }
                    } else {
                        RuntimeException exception =
                                new RuntimeException(context.getString(R.string.error_caching_video));
                        e.onError(exception);
                        Crashlytics.logException(exception);
                        Crashlytics.log("Error while downloading fav:\n" +
                                "ID = " + feed.getId() + "\n" +
                                "PATH = " + feed.getPath());
                    }
                }));
    }

    public Single<List<Feed>> getFeed(FeedRVAdapter.FeedType feedType, int skip,
                                      @Nullable Integer otherUserId) {
        if (otherUserId == null)
            return getUserId().flatMap(myUserId ->
                    favRepository.getFeed(getFeedType(feedType), myUserId, skip));
        else
            return favRepository.getFeed(getFeedType(feedType), otherUserId, skip);
    }

    public Single<Integer> getUserId() {
        return profileRepository.getSingleProfile()
                .map(localProfiles -> {
                    if (localProfiles.size() > 0)
                        return localProfiles.get(0).getUserId();
                    else
                        return 0;
                });
    }

    private int getFeedType(FeedRVAdapter.FeedType feedType) {
        switch (feedType) {
            case ALL:
                return -1;
            case FEATURED:
                return 0;
            case LAB:
                return 1;
            case MY_FAVS:
                return 2;
            case LIKED_FAVS:
                return 3;
            case OTHER_USER_FAVS:
                return 4;
            default:
                throw new RuntimeException("Can`t define FeedType!");
        }
    }
}
