package com.favi.favi.viewmodel;

import com.favi.favi.app.FaViApp;
import com.favi.favi.repository.ProfileRepository;

import javax.inject.Inject;

/**
 * Created by maxvinnik on 14.01.2018.
 */

// TODO: 31.01.2018 Delete unusable viewmodel
public class LoginViewModel extends BaseViewModel {

    private ProfileRepository profileRepository;

    @Inject
    public LoginViewModel(FaViApp context, ProfileRepository profileRepository) {
        super(context);
        this.profileRepository = profileRepository;
    }
}
