package com.favi.favi.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.favi.favi.adapter.recyclerview.FeedRVAdapter;
import com.favi.favi.api.ServiceGenerator;
import com.favi.favi.app.FaViApp;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.pojo.response.Likes;
import com.favi.favi.repository.FavRepository;
import com.favi.favi.util.Constants;
import com.favi.favi.util.ContentUriProvider;
import com.favi.favi.viewmodel.interact.FavInteractor;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kovac on 05.11.2017.
 * My SKYPE: maxvinnik
 */

public class FeedViewModel extends BaseViewModel {

    private FavRepository favRepository;
    private FavInteractor favInteractor;

    private int skipVideoCount = 0;

    private MutableLiveData<List<Feed>> feed;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Boolean> isLastPage;
    private MutableLiveData<FeedRVAdapter.FeedType> feedType;
    private MutableLiveData<Likes> likes;

    private boolean isFeedRefreshed;

    private Disposable downloadVideoDisposable;
    private MutableLiveData<Boolean> isDownloadingVideo;
    private MutableLiveData<Intent> shareIntent;

    @Inject
    public FeedViewModel(FaViApp context, FavRepository favRepository, FavInteractor favInteractor) {
        super(context);
        this.favRepository = favRepository;
        this.favInteractor = favInteractor;

        feed = new MutableLiveData<>();
        isLoading = new MutableLiveData<>();
        isLastPage = new MutableLiveData<>();
        feedType = new MutableLiveData<>();
        likes = new MutableLiveData<>();
        shareIntent = new MutableLiveData<>();
    }

    @SuppressWarnings("ConstantConditions")
    public LiveData<List<Feed>> getFeed(FeedRVAdapter.FeedType feedType, boolean refreshFeed,
                                        @Nullable Integer otherUserId) {
        isFeedRefreshed = refreshFeed;

        isLoading.setValue(true);

        if (refreshFeed) {
            isLastPage.setValue(false);
            skipVideoCount = 0;
        }

        favInteractor.getFeed(feedType, skipVideoCount, otherUserId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(feeds -> {
                    isLoading.setValue(false);

                    if (feeds.size() < ServiceGenerator.TAKE_PAGINATION_COUNT)
                        isLastPage.setValue(true);

                    feed.setValue(feeds);

                    skipVideoCount = skipVideoCount + ServiceGenerator.TAKE_PAGINATION_COUNT;
                }, throwable -> {
                    isLoading.setValue(false);
                    handleError(throwable);
                });

        return feed;
    }

    public LiveData<Likes> postLike(int videoId) {
        favInteractor.postLike(videoId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(likes -> this.likes.setValue(likes), this::handleError);

        return likes;
    }

    public LiveData<Feed> postFavInLab(Feed feed) {
        MutableLiveData<Feed> data = new MutableLiveData<>();

        favInteractor.postInLab(feed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data::setValue, throwable -> {
                    handleError(throwable);
                    throwable.printStackTrace();
                });

        return data;
    }

    @SuppressWarnings("ConstantConditions")
    public LiveData<Boolean> shareFav(Feed feed) {
        isDownloadingVideo = new MutableLiveData<>();

        if (feed.getLocalPath() != null) {
            share(new File(feed.getLocalPath()));
        } else {
            downloadVideo(feed, isDownloadingVideo);
        }

        return isDownloadingVideo;
    }

    private void downloadVideo(Feed feed, MutableLiveData<Boolean> isDownloadingVideo) {
        isDownloadingVideo.setValue(true);

        downloadVideoDisposable = favInteractor.downloadFav(feed)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(file -> {
                    isDownloadingVideo.setValue(false);
                    share(file);
                }, throwable -> {
                    isDownloadingVideo.setValue(false);
                    handleError(throwable);
                    throwable.printStackTrace();
                });
    }

    public void cancelDownloadVideo() {
        isDownloadingVideo.setValue(false);
        downloadVideoDisposable.dispose();
    }

    private void share(File file) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        Uri uri = ContentUriProvider.getUriForFile(super.getApplication(),
                Constants.FILE_PROVIDER_AUTHORITES, file);
        intent.setType("video/*");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.postValue(intent);
    }

    public LiveData<Intent> shareIntent() {
        return shareIntent;
    }

    public LiveData<Boolean> isLastPage() {
        return isLastPage;
    }

    public LiveData<Boolean> isLoading() {
        return isLoading;
    }

    public LiveData<FeedRVAdapter.FeedType> feedType() {
        return feedType;
    }

    public boolean isFeedRefreshed() {
        return isFeedRefreshed;
    }

    public LiveData<Boolean> postReport(int vidId) {
        MutableLiveData<Boolean> data = new MutableLiveData<>();

        favRepository.postReport(vidId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> data.setValue(true), throwable -> {
                    data.setValue(false);
                    handleError(throwable);
                    throwable.printStackTrace();
                });

        return data;
    }

    public LiveData<Boolean> startOtherProfile(int userId) {
        MutableLiveData<Boolean> data = new MutableLiveData<>();

        favInteractor.getUserId()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(myUserId -> {
                    if (userId == myUserId)
                        data.setValue(false);
                    else
                        data.setValue(true);
                }, throwable -> {
                    throwable.printStackTrace();
                    handleError(throwable);
                });

        return data;
    }
}
