package com.favi.favi.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.facebook.CallbackManager;
import com.favi.favi.app.FaViApp;
import com.favi.favi.db.entity.LocalProfile;
import com.favi.favi.pojo.request.LoginProfile;
import com.favi.favi.repository.ProfileRepository;
import com.favi.favi.viewmodel.interact.ProfileInteractor;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by maxvinnik on 13.01.2018.
 */

public class ProfileViewModel extends BaseViewModel {

    public MutableLiveData<Boolean> isEditProfileSuccessful;
    private ProfileRepository profileRepository;
    private ProfileInteractor profileInteractor;

    @Inject
    public ProfileViewModel(FaViApp context, ProfileRepository profileRepository, ProfileInteractor profileInteractor) {
        super(context);
        this.profileRepository = profileRepository;
        this.profileInteractor = profileInteractor;

        isEditProfileSuccessful = new MutableLiveData<>();
    }

    public LiveData<LocalProfile> getProfile() {
        MutableLiveData<LocalProfile> data = new MutableLiveData<>();

        profileRepository.getProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(localProfiles -> {
                    if (localProfiles.size() > 0)
                        data.setValue(localProfiles.get(0));
                    else
                        data.setValue(null);
                }, throwable -> {
                    data.setValue(null);
                    throwable.printStackTrace();
                    handleError(throwable);
                });

        return data;
    }

    public LiveData<Boolean> loginProfile(LoginProfile loginProfile) {
        MutableLiveData<Boolean> data = new MutableLiveData<>();

        profileRepository.loginProfile(loginProfile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(localProfile -> data.setValue(true), throwable -> {
                    data.setValue(false);
                    throwable.printStackTrace();
                    handleError(throwable);
                });

        return data;
    }

    public LiveData<Boolean> editProfile(LocalProfile localProfile) {
        profileRepository.editProfile(localProfile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> showLoadIndicator.setValue(true))
                .doOnTerminate(() -> showLoadIndicator.setValue(false))
                .subscribe(() -> isEditProfileSuccessful.setValue(true),
                        throwable -> {
                            throwable.printStackTrace();
                            handleError(throwable);
                        });

        return isEditProfileSuccessful;
    }

    public void loginFB(CallbackManager callbackManager) {
        profileInteractor.loginFB(callbackManager);
    }

    public LiveData<Boolean> startOtherProfile(int userId) {
        MutableLiveData<Boolean> data = new MutableLiveData<>();

        profileRepository.getSingleProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(localProfiles -> {
                    if (localProfiles.size() > 0 && localProfiles.get(0).getUserId() == userId)
                        data.setValue(false);
                    else
                        data.setValue(true);
                }, throwable -> {
                    data.setValue(false);
                    throwable.printStackTrace();
                    handleError(throwable);
                });

        return data;
    }
}
