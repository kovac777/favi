package com.favi.favi.viewmodel;

import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.favi.favi.app.FaViApp;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.HttpException;

/**
 * Created by kovac on 05.11.2017.
 * My SKYPE: maxvinnik
 */

public abstract class BaseViewModel extends AndroidViewModel {

    MutableLiveData<Boolean> showLoadIndicator = new MutableLiveData<>();
    MutableLiveData<Boolean> resetUI = new MutableLiveData<>();
    private MutableLiveData<Throwable> httpException = new MutableLiveData<>();
    private MutableLiveData<Throwable> socketTimeOutException = new MutableLiveData<>();
    private MutableLiveData<Throwable> ioException = new MutableLiveData<>();
    private MutableLiveData<Throwable> unknownException = new MutableLiveData<>();

    public BaseViewModel(@NonNull FaViApp application) {
        super(application);
    }

    void handleError(Throwable throwable) {
        if (throwable instanceof HttpException) httpException.postValue(throwable);
        else if (throwable instanceof SocketTimeoutException)
            socketTimeOutException.postValue(throwable);
        else if (throwable instanceof IOException) ioException.postValue(throwable);
        else unknownException.postValue(throwable);
    }

    public LiveData<Throwable> httpException() {
        return httpException;
    }

    public LiveData<Throwable> socketTimeOutException() {
        return socketTimeOutException;
    }

    public LiveData<Throwable> ioException() {
        return ioException;
    }

    public LiveData<Throwable> unknownException() {
        return unknownException;
    }

    public LiveData<Boolean> showLoadIndicator() {
        return showLoadIndicator;
    }

    public LiveData<Boolean> resetUI() {
        return resetUI;
    }

}
