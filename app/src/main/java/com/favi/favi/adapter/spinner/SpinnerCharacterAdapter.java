package com.favi.favi.adapter.spinner;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.favi.favi.R;
import com.favi.favi.pojo.response.Character;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by kovac on 08.11.2017.
 * My SKYPE: maxvinnik
 */

public class SpinnerCharacterAdapter extends ArrayAdapter<Character> {

    private List<Character> characters;
    private LayoutInflater layoutInflater;

    public SpinnerCharacterAdapter(@NonNull Context context, int resource,
                                   @NonNull List<Character> objects) {
        super(context, resource, objects);

        this.characters = objects;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createViewFromResource(layoutInflater, position, convertView, parent, R.layout.spinner_item);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createViewFromResource(layoutInflater, position, convertView, parent, R.layout.spinner_item);
    }

    protected View createViewFromResource(LayoutInflater inflater, int position,
                                          View convertView, ViewGroup parent,
                                          int resource) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(resource, parent, false);
        } else {
            view = convertView;
        }

        bindView(position, view);

        return view;
    }

    protected void bindView(int position, View view) {
        CircleImageView ivAvatar = view.findViewById(R.id.civAvatar);
        TextView tvName = view.findViewById(R.id.tvName);

        Glide.with(ivAvatar)
                .load(characters.get(position).getIcon())
                .into(ivAvatar);
        tvName.setText(characters.get(position).getTagName());
    }

    @Nullable
    @Override
    public Character getItem(int position) {
        return characters.get(position);
    }

    public int getCharacterPosition(int tagId) {
        for (int i = 0; i < characters.size(); i++) {
            if (characters.get(i).getTagId() == tagId)
                return i;
        }

        throw new RuntimeException("Can`t define Character position!");
    }
}
