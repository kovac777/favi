package com.favi.favi.adapter.recyclerview;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.constraint.Group;
import android.support.v4.content.ContextCompat;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.dummyitem.Loading;
import com.favi.favi.api.ServiceGenerator;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.exoplayer.FavPlayer;
import com.favi.favi.exoplayer.PlayerListener;
import com.favi.favi.glide.GlideApp;
import com.favi.favi.util.Constants;
import com.favi.favi.util.TextUtil;
import com.favi.favi.util.TimeUtil;
import com.favi.favi.util.toucharea.TouchAreaUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FeedRVAdapter extends BaseRVAdapter<BaseRVAdapter.ItemViewType> implements PlayerListener {

    public static final int FEED_HEADER_VIEW_TYPE = 1;
    public static final int FEED_VIEW_TYPE = 2;
    public static final int LOADING_VIEW_TYPE = 3;

    private Context context;
    private FavPlayer favPlayer;
    private ItemClickListener itemClickListener;
    private FeedViewHolder previousPlayerViewHolder;
    private FeedType feedType;

    public FeedRVAdapter(Context context, FavPlayer favPlayer, ItemClickListener itemClickListener) {
        this.context = context;
        this.favPlayer = favPlayer;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        switch (data.get(position).getType()) {
            case FEED_LAB_HEADER:
                return FEED_HEADER_VIEW_TYPE;
            case FEED:
                return FEED_VIEW_TYPE;
            case LOADING:
                return LOADING_VIEW_TYPE;
            default:
                throw new RuntimeException("Cant define item view type!");
        }
    }

    @Override
    protected int getItemLayoutId(int viewType) {
        switch (viewType) {
            case FEED_HEADER_VIEW_TYPE:
                return R.layout.rv_item_feed_lab_header;
            case FEED_VIEW_TYPE:
                return R.layout.rv_item_feed;
            case LOADING_VIEW_TYPE:
                return R.layout.rv_item_loading;
            default:
                throw new RuntimeException("Can`t define item view type");
        }
    }

    @Override
    protected BaseRVViewHolder<ItemViewType> createViewHolder(View itemView, int viewType) {
        switch (viewType) {
            case FEED_HEADER_VIEW_TYPE:
                return new FeedLabHeaderViewHolder(itemView);
            case FEED_VIEW_TYPE:
                return new FeedViewHolder(itemView);
            case LOADING_VIEW_TYPE:
                return new LoadingViewHolder(itemView);
            default:
                throw new RuntimeException("Can`t define item view type!");
        }
    }

    public void setFeedType(FeedType feedType) {
        this.feedType = feedType;
    }

    public void addFooterLoading() {
        add(new Loading());
    }

    public void removeFooterLoading() {
        remove(data.size() - 1);
    }

    public void updateItem(Feed feed, int position) {
        data.set(position, feed);
        notifyItemChanged(position);
    }

    public void updateCommentsCount(int position, int commentsCount) {
        ((Feed) data.get(position)).setComments(commentsCount);
        notifyItemChanged(position);
    }

    private void setLike(int position) {
        ((Feed) data.get(position)).setLiked(true);
        ((Feed) data.get(position)).setLikes(((Feed) data.get(position)).getLikes() + 1);
        updateItem((Feed) data.get(position), position);
    }

    private void setDislike(int position) {
        ((Feed) data.get(position)).setLiked(false);
        ((Feed) data.get(position)).setLikes(((Feed) data.get(position)).getLikes() - 1);
        updateItem((Feed) data.get(position), position);
    }

    public FeedViewHolder getPreviousPlayerViewHolder() {
        return previousPlayerViewHolder;
    }

    @Override
    public void videoFinished() {
        if (previousPlayerViewHolder != null) {
            previousPlayerViewHolder.ivPlay.performClick();
        }
    }

    public enum FeedType {
        ALL(-1), FEATURED(0), LAB(1), MY_FAVS(2), LIKED_FAVS(3), OTHER_USER_FAVS(4);

        private int value;

        FeedType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public interface ItemClickListener {

        void onLikeDislikeClicked(int videoId, int position);

        void onShareClicked(Feed feed);

        void onPostToLabClicked(Feed feed);

        void onReportClicked(int vidId);

        void onCommentsClicked(int vidId, int comments, int position);

        void onProfileClicked(int userId, String userName, String userAvatar);
    }

    protected class FeedLabHeaderViewHolder extends BaseRVViewHolder<ItemViewType> {

        public FeedLabHeaderViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void onBind(ItemViewType object, int position) {

        }
    }

    protected class FeedViewHolder extends BaseRVViewHolder<ItemViewType> implements View.OnClickListener {

        @BindView(R.id.textureView)
        TextureView textureView;

        @BindView(R.id.ivSound)
        ImageView ivSound;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.tvShare)
        TextView tvShare;
        @BindView(R.id.tvComments)
        TextView tvComments;
        @BindView(R.id.ivComments)
        ImageView ivComments;
        @BindView(R.id.tvCountLikes)
        TextView tvCountLikes;
        @BindView(R.id.ivLike)
        ImageView ivLike;
        @BindView(R.id.ivPreview)
        ImageView ivPreview;
        @BindView(R.id.ivAvatar)
        CircleImageView ivAvatar;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.ivPlay)
        ImageView ivPlay;
        @BindView(R.id.ivReport)
        ImageView ivReport;
        @BindView(R.id.tvShareInLab)
        TextView tvShareInLab;
        @BindView(R.id.ivShare)
        ImageView ivShare;
        @BindView(R.id.ivShareInLab)
        ImageView ivShareInLab;
        @BindView(R.id.groupAddedToLab)
        Group groupAddedToLab;

        @BindView(R.id.groupAddToLab)
        Group groupAddToLab;

        @BindView(R.id.groupUploading)
        Group groupUploading;

        private Feed feed;

        public FeedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            textureView.setOnClickListener(this);
            ivSound.setOnClickListener(this);
            tvShare.setOnClickListener(this);
            tvCountLikes.setOnClickListener(this);
            ivLike.setOnClickListener(this);
            ivPlay.setOnClickListener(this);
            ivReport.setOnClickListener(this);
            tvShareInLab.setOnClickListener(this);
            ivShareInLab.setOnClickListener(this);
            ivShare.setOnClickListener(this);
            tvComments.setOnClickListener(this);
            ivComments.setOnClickListener(this);
            ivAvatar.setOnClickListener(this);

            TouchAreaUtil.extendTouchArea(32, tvCountLikes, ivLike, tvShare, ivLike,
                    tvShareInLab, ivComments, tvComments);
        }

        @Override
        protected void onBind(ItemViewType object, int position) {
            feed = (Feed) object;

            //setup uploading UI
            if (feed.isPosting()) {
                groupAddToLab.setVisibility(View.GONE);
                groupAddedToLab.setVisibility(View.GONE);
                groupUploading.setVisibility(View.VISIBLE);
            } else {
                groupUploading.setVisibility(View.GONE);

                //setup inLab UI
                if (feedType == FeedType.MY_FAVS && !feed.isInLab()) {
                    groupAddedToLab.setVisibility(View.GONE);
                    groupAddToLab.setVisibility(View.VISIBLE);
                } else {
                    groupAddToLab.setVisibility(View.GONE);
                    groupAddedToLab.setVisibility(View.VISIBLE);
                }

                //setup isLiked UI
                if (feed.isLiked()) {
                    tvCountLikes.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                    ivLike.setImageResource(R.drawable.liked);
                } else {
                    tvCountLikes.setTextColor(ContextCompat.getColor(context, R.color.colorNonLiked));
                    ivLike.setImageResource(R.drawable.like);
                }
            }

            //setup profile
            GlideApp.with(context)
                    .load(getAvatar(feed))
                    .placeholder(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(ivAvatar);
            tvName.setText(getName(feed));

            //setup preview
            String previewPath;
            if (feedType == FeedType.MY_FAVS) {
                previewPath = feed.getLocalPath();
            } else previewPath = ServiceGenerator.FEED_URL + feed.getThumbnail();
            GlideApp.with(context)
                    .asBitmap()
                    .load(previewPath)
                    .into(ivPreview);

            //setup likes count
            tvCountLikes.setText(String.valueOf(feed.getLikes()));

            //setup time
            tvTime.setText(TimeUtil.getAddedTime(context, feed.getDate()));

            //setup comments count
            tvComments.setText(String.valueOf(feed.getComments()));
        }

        private CharSequence getName(Feed feed) {
            if (feed.getUserName() != null)
                return feed.getUserName();
            else return Constants.ROBOT_FAVI_NAME;
        }

        private String getAvatar(Feed feed) {
            if (feed.getImage() != null)
                return feed.getImage();
            else return Constants.ROBOT_FAVI_IMAGE;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivSound:
                    if (favPlayer.getVolume() == 0) {
                        ivSound.setImageResource(R.drawable.sound);
                        favPlayer.setVolume(1);
                    } else {
                        ivSound.setImageResource(R.drawable.mute);
                        favPlayer.setVolume(0);
                    }
                    break;
                case R.id.textureView:
                case R.id.ivPlay:
                    setupPlayer();
                    break;
                case R.id.tvShare:
                case R.id.ivShare:
                    itemClickListener.onShareClicked(feed);
                    break;
                case R.id.tvCountLikes:
                case R.id.ivLike:
                    if (!feed.isLiked()) {
                        setLike(getAdapterPosition());
                        itemClickListener.onLikeDislikeClicked(feed.getId(), getAdapterPosition());
                    } else {
                        setDislike(getAdapterPosition());
                        itemClickListener.onLikeDislikeClicked(feed.getId(), getAdapterPosition());
                    }
                    break;
                case R.id.tvComments:
                case R.id.ivComments:
                    itemClickListener.onCommentsClicked(feed.getId(), feed.getComments(), getAdapterPosition());
                    break;
                case R.id.ivAvatar:
                    itemClickListener.onProfileClicked(feed.getUserId(), feed.getUserName(), feed.getImage());
                    break;
                case R.id.tvShareInLab:
                case R.id.ivShareInLab:
                    postToLab();
                    break;
                case R.id.ivReport:
                    itemClickListener.onReportClicked(feed.getId());
                    break;
            }
        }

        private void postToLab() {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            if (preferences.getBoolean(Constants.PREFS_IS_LOGGED_IN, false)) {
                feed.setPosting(true);
                groupAddToLab.setVisibility(View.GONE);
                groupAddedToLab.setVisibility(View.GONE);
                groupUploading.setVisibility(View.VISIBLE);
            }
            itemClickListener.onPostToLabClicked(feed);
        }

        private void setupPlayer() {
            String videoPath;
            if (feedType == FeedType.MY_FAVS) {
                videoPath = feed.getLocalPath();
            } else {
                videoPath = TextUtil.getStreamVideoLink(
                        ServiceGenerator.FEED_URL + "/" + feed.getPath());
            }

            //stop previous running video if need
            if (previousPlayerViewHolder != null && favPlayer.isPlaying() &&
                    previousPlayerViewHolder.getAdapterPosition() != getAdapterPosition()) {
                favPlayer.stop();
                previousPlayerViewHolder.ivSound.setVisibility(View.GONE);
                previousPlayerViewHolder.ivPreview.setVisibility(View.VISIBLE);
                previousPlayerViewHolder.ivPlay.setVisibility(View.VISIBLE);
                previousPlayerViewHolder.ivPreview.setVisibility(View.VISIBLE);

                favPlayer.setDataSource(Uri.parse(videoPath));
                favPlayer.attachToView(textureView);
                favPlayer.play();
                ivSound.setVisibility(View.VISIBLE);
                ivPreview.setVisibility(View.GONE);
                ivPlay.setVisibility(View.GONE);
                ivPreview.setVisibility(View.GONE);
            } else {
                if (favPlayer.isPlaying()) {
                    favPlayer.stop();
                    ivSound.setVisibility(View.GONE);
                    ivPreview.setVisibility(View.VISIBLE);
                    ivPlay.setVisibility(View.VISIBLE);
                    ivPreview.setVisibility(View.VISIBLE);
                } else {
                    favPlayer.setDataSource(Uri.parse(videoPath));
                    favPlayer.attachToView(textureView);
                    favPlayer.play();
                    ivSound.setVisibility(View.VISIBLE);
                    ivPreview.setVisibility(View.GONE);
                    ivPlay.setVisibility(View.GONE);
                    ivPreview.setVisibility(View.GONE);
                }
            }

            previousPlayerViewHolder = this;
        }
    }

    private class LoadingViewHolder extends BaseRVViewHolder<ItemViewType> {

        public LoadingViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void onBind(ItemViewType object, int position) {

        }

    }
}
