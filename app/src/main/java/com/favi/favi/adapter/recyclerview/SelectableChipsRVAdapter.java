package com.favi.favi.adapter.recyclerview;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.favi.favi.R;
import com.favi.favi.pojo.Chip;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kovac on 08.11.2017.
 * My SKYPE: maxvinnik
 */

public class SelectableChipsRVAdapter extends BaseRVAdapter<BaseRVAdapter.ItemViewType> {

    public static final int CHIP_VIEW_TYPE = 1;
    private Context context;
    private OnChipClickListener onChipClickListener;
    public SelectableChipsRVAdapter(Context context, OnChipClickListener onChipClickListener) {
        this.context = context;
        this.onChipClickListener = onChipClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        switch (data.get(position).getType()) {
            case CHIP:
                return CHIP_VIEW_TYPE;
            default:
                throw new RuntimeException("Can`t define item view type!");
        }
    }

    @Override
    protected int getItemLayoutId(int viewType) {
        switch (viewType) {
            case CHIP_VIEW_TYPE:
                return R.layout.rv_item_chip;
            default:
                throw new RuntimeException("Can`t define item view type!");
        }
    }

    @Override
    protected BaseRVViewHolder<ItemViewType> createViewHolder(View itemView, int viewType) {
        switch (viewType) {
            case CHIP_VIEW_TYPE:
                return new ChipViewHolder(itemView);
            default:
                throw new RuntimeException("Can`t define item view type!");
        }
    }

    public interface OnChipClickListener {
        void onChipClicked(String word, int position);
    }

    protected class ChipViewHolder extends BaseRVViewHolder<ItemViewType> implements View.OnClickListener {

        @BindView(R.id.tvChip)
        TextView tvChip;

        private Chip chip;

        ChipViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        protected void onBind(ItemViewType object, int position) {
            chip = (Chip) object;

            tvChip.setText(chip.getWord());
        }

        @Override
        public void onClick(View view) {
            onChipClickListener.onChipClicked(chip.getWord(), getAdapterPosition());
        }
    }
}
