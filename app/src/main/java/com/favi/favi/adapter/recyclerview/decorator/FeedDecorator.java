package com.favi.favi.adapter.recyclerview.decorator;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.favi.favi.util.DimensionUtil;

/**
 * Created by kovac on 08.11.2017.
 * My SKYPE: maxvinnik
 */

public class FeedDecorator extends RecyclerView.ItemDecoration {

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int position = parent.getChildViewHolder(view).getAdapterPosition();

        if (position == 0) {
            outRect.top = DimensionUtil.dpToPx(16);
        }
        outRect.bottom = DimensionUtil.dpToPx(16);
    }
}
