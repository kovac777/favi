package com.favi.favi.adapter.recyclerview.dummyitem;

import com.favi.favi.adapter.recyclerview.BaseRVAdapter;

/**
 * Created by kovac on 10.12.2017.
 * My SKYPE: maxvinnik
 */

public class LabHeader implements BaseRVAdapter.ItemViewType {

    @Override
    public BaseRVAdapter.ViewType getType() {
        return BaseRVAdapter.ViewType.FEED_LAB_HEADER;
    }
}
