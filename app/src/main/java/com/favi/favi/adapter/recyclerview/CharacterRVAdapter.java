package com.favi.favi.adapter.recyclerview;

import android.content.Context;
import android.view.View;

import com.favi.favi.R;
import com.favi.favi.glide.GlideApp;
import com.favi.favi.pojo.response.Character;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by maxvinnik on 04.01.2018.
 */

public class CharacterRVAdapter extends BaseRVAdapter<BaseRVAdapter.ItemViewType> {

    private Context context;
    private ItemClickListener itemClickListener;

    public CharacterRVAdapter(Context context, ItemClickListener itemClickListener) {
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    protected int getItemLayoutId(int viewType) {
        return R.layout.rv_item_character;
    }

    @Override
    protected BaseRVViewHolder<ItemViewType> createViewHolder(View itemView, int viewType) {
        return new CharacterViewHolder(itemView);
    }

    public interface ItemClickListener {
        void onItemClicked(ArrayList<Character> characters, Character character, int position);
    }

    protected class CharacterViewHolder extends BaseRVViewHolder<ItemViewType> implements View.OnClickListener {

        @BindView(R.id.ivCharacter)
        CircleImageView ivCharacter;

        private Character character;

        public CharacterViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            ivCharacter.setOnClickListener(this);
        }

        @Override
        protected void onBind(ItemViewType object, int position) {
            character = (Character) object;

            if (!character.isEnabled())
                ivCharacter.setClickable(false);

            GlideApp.with(context)
                    .load(character.getIcon())
                    .into(ivCharacter);
        }

        @Override
        public void onClick(View view) {
            if (character.isEnabled())
                itemClickListener.onItemClicked(getEnabledCharacters(), character, getAdapterPosition());
        }

        private ArrayList<Character> getEnabledCharacters() {
            ArrayList<Character> characters = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) instanceof Character &&
                        ((Character) data.get(i)).isEnabled()) {
                    Character character = (Character) data.get(i);
                    characters.add(character);
                }
            }
            return characters;
        }
    }
}
