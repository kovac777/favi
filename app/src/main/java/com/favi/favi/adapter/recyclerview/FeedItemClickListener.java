package com.favi.favi.adapter.recyclerview;

import com.favi.favi.db.entity.Feed;

/**
 * Created by User on 31.01.2018.
 * My SKYPE: maxvinnik
 */

public abstract class FeedItemClickListener implements FeedRVAdapter.ItemClickListener {

    @Override
    public void onLikeDislikeClicked(int videoId, int position) {

    }

    @Override
    public void onShareClicked(Feed feed) {

    }

    @Override
    public void onPostToLabClicked(Feed feed) {

    }

    @Override
    public void onReportClicked(int vidId) {

    }

    @Override
    public void onCommentsClicked(int vidId, int comments, int position) {

    }

    @Override
    public void onProfileClicked(int userId, String userName, String userAvatar) {

    }
}
