package com.favi.favi.adapter.recyclerview;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.favi.favi.exoplayer.FavPlayer;

/**
 * Created by kovac on 06.11.2017.
 * My SKYPE: maxvinnik
 */

public abstract class Pagination extends RecyclerView.OnScrollListener {

    private LinearLayoutManager layoutManager;
    private BaseRVAdapter<BaseRVAdapter.ItemViewType> rvAdapter;
    private FavPlayer favPlayer;

    protected Pagination(LinearLayoutManager layoutManager,
                         BaseRVAdapter<BaseRVAdapter.ItemViewType> rvAdapter,
                         @Nullable FavPlayer favPlayer) {
        this.layoutManager = layoutManager;
        this.rvAdapter = rvAdapter;
        this.favPlayer = favPlayer;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();

        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstVisiblePosition) >= totalItemCount &&
                    firstVisiblePosition >= 0) {
                loadMore();
            }
        }

        if (rvAdapter instanceof FeedRVAdapter) {
            FeedRVAdapter feedRVAdapter = (FeedRVAdapter) rvAdapter;

            //stop video when we not see it
            if (!favPlayer.isPlaying() || feedRVAdapter.getPreviousPlayerViewHolder() == null)
                return;

            if (feedRVAdapter.getPreviousPlayerViewHolder().getAdapterPosition() < firstVisiblePosition ||
                    feedRVAdapter.getPreviousPlayerViewHolder().getAdapterPosition() > layoutManager.findLastVisibleItemPosition()) {
                feedRVAdapter.getPreviousPlayerViewHolder().ivPlay.performClick();
            }
        }
    }

    public abstract boolean isLoading();

    public abstract boolean isLastPage();

    public abstract void loadMore();
}
