package com.favi.favi.adapter.recyclerview;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.dummyitem.Loading;
import com.favi.favi.glide.GlideApp;
import com.favi.favi.pojo.response.Comments;
import com.favi.favi.util.TimeUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 23.01.2018.
 * My SKYPE: maxvinnik
 */

public class CommentsRVAdapter extends BaseRVAdapter<BaseRVAdapter.ItemViewType> {

    public static final int COMMENT_VIEW_TYPE = 1;
    public static final int LOADING_VIEW_TYPE = 2;
    private Context context;
    private ItemClickListener itemClickListener;
    public CommentsRVAdapter(Context context, ItemClickListener itemClickListener) {
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        switch (data.get(position).getType()) {
            case COMMENT:
                return COMMENT_VIEW_TYPE;
            case LOADING:
                return LOADING_VIEW_TYPE;
            default:
                throw new RuntimeException("Can`t define item view type!");
        }
    }

    @Override
    protected int getItemLayoutId(int viewType) {
        switch (viewType) {
            case COMMENT_VIEW_TYPE:
                return R.layout.rv_item_comment;
            case LOADING_VIEW_TYPE:
                return R.layout.rv_item_loading;
            default:
                throw new RuntimeException("Can`t define item view type!");
        }
    }

    @Override
    protected BaseRVViewHolder<ItemViewType> createViewHolder(View itemView, int viewType) {
        switch (viewType) {
            case COMMENT_VIEW_TYPE:
                return new CommentViewHolder(itemView);
            case LOADING_VIEW_TYPE:
                return new LoadingViewHolder(itemView);
            default:
                throw new RuntimeException("Can`t define view type!");
        }
    }

    public void addFooterLoading() {
        add(new Loading());
    }

    public void removeFooterLoading() {
        remove(data.size() - 1);
    }

    public void addComment(Comments.CommentsBean comment) {
        data.add(0, comment);
        notifyItemInserted(0);
    }

    public interface ItemClickListener {
        void onProfileClicked(int userId, String userName, String userAvatar);

        void onEditClicked(int commentId);

        void onDeleteClicked(int commentId);
    }

    protected class CommentViewHolder extends BaseRVViewHolder<ItemViewType> {

        @BindView(R.id.ivAvatar)
        CircleImageView ivAvatar;
        @BindView(R.id.tvAuthor)
        TextView tvAuthor;
        @BindView(R.id.tvComment)
        TextView tvComment;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.ivCommentDots)
        ImageView ivCommentDots;

        private Comments.CommentsBean comment;

        public CommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.ivAvatar})
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivAvatar:
                    itemClickListener.onProfileClicked(
                            comment.getUser().getUserId(),
                            comment.getUser().getUserName(),
                            comment.getUser().getImage());
                    break;
            }
        }

        @Override
        protected void onBind(ItemViewType object, int position) {
            comment = (Comments.CommentsBean) object;

//            if (comment.isMyComment()) {
//                ivCommentDots.setVisibility(View.VISIBLE);
//            } else
//                ivCommentDots.setVisibility(View.GONE);

            GlideApp.with(context)
                    .load(comment.getUser().getImage())
                    .into(ivAvatar);

            tvComment.setText(comment.getText());

            tvAuthor.setText(comment.getUser().getUserName());

            tvDate.setText(TimeUtil.getAddedTime(context, comment.getDate()));
        }
    }

    private class LoadingViewHolder extends BaseRVViewHolder<ItemViewType> {

        public LoadingViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void onBind(ItemViewType object, int position) {

        }

    }
}
