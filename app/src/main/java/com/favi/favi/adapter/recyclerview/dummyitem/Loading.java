package com.favi.favi.adapter.recyclerview.dummyitem;

import com.favi.favi.adapter.recyclerview.BaseRVAdapter;

/**
 * Created by kovac on 06.11.2017.
 * My SKYPE: maxvinnik
 */

public class Loading implements BaseRVAdapter.ItemViewType {

    @Override
    public BaseRVAdapter.ViewType getType() {
        return BaseRVAdapter.ViewType.LOADING;
    }
}
