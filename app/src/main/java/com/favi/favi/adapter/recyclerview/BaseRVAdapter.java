package com.favi.favi.adapter.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kovac on 04.11.2017.
 * My SKYPE: maxvinnik
 */

public abstract class BaseRVAdapter<ItemViewType> extends RecyclerView.Adapter<BaseRVAdapter.BaseRVViewHolder> {

    protected List<ItemViewType> data;

    protected abstract int getItemLayoutId(int viewType);

    protected abstract BaseRVViewHolder<ItemViewType> createViewHolder(View itemView, int viewType);

    @Override
    public BaseRVViewHolder<ItemViewType> onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(getItemLayoutId(viewType), parent, false);
        return createViewHolder(view, viewType);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(BaseRVViewHolder holder, int position) {
        holder.onBind(data.get(position), position);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public void add(ItemViewType t) {
        if (this.data == null) data = new ArrayList<>();

        data.add(t);
        notifyItemInserted(data.size() - 1);
    }

    public void add(ItemViewType t, int position) {
        if (this.data == null) data = new ArrayList<>();

        data.add(position, t);
        notifyItemInserted(position);
    }

    public void addAll(List<ItemViewType> data) {
        if (this.data == null)
            this.data = new ArrayList<>();

        if (this.data.size() == 0) {
            this.data.addAll(data);
            notifyItemRangeInserted(0, data.size());
        } else {
            int positionStart = this.data.size();
            this.data.addAll(data);
            notifyItemRangeInserted(positionStart, data.size());
        }
    }

    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        if (data != null) {
            int previousCount = data.size();
            data.clear();
            notifyItemRangeRemoved(0, previousCount);
        }
    }

    public enum ViewType {
        //Feed
        FEED_LAB_HEADER, FEED, LOADING,
        //New Fav
        CHIP, ALL_COUNT_WORDS,
        //Select Character
        CHARACTER,
        //Comments
        COMMENT
    }

    public interface ItemViewType {

        ViewType getType();
    }

    protected abstract class BaseRVViewHolder<T1> extends RecyclerView.ViewHolder {

        public BaseRVViewHolder(View itemView) {
            super(itemView);
        }

        protected abstract void onBind(T1 object, int position);
    }

}
