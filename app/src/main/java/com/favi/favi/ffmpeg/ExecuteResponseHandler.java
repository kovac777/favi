package com.favi.favi.ffmpeg;

import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;

/**
 * Created by kovac on 21.11.2017.
 * My SKYPE: maxvinnik
 */

public class ExecuteResponseHandler implements FFmpegExecuteResponseHandler {

    @Override
    public void onSuccess(String message) {
        Log.d("logi", "onSuccess " + message);
    }

    @Override
    public void onProgress(String message) {
        Log.d("logi", "onProgress " + message);
    }

    @Override
    public void onFailure(String message) {
        Log.d("logi", "onFailure " + message);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onFinish() {

    }
}
