package com.favi.favi.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.favi.favi.BuildConfig;
import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.FeedItemClickListener;
import com.favi.favi.adapter.recyclerview.FeedRVAdapter;
import com.favi.favi.adapter.recyclerview.Pagination;
import com.favi.favi.adapter.recyclerview.decorator.FeedDecorator;
import com.favi.favi.adapter.recyclerview.dummyitem.LabHeader;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.exoplayer.FavPlayer;
import com.favi.favi.helper.AnalyticsHelper;
import com.favi.favi.ui.fragment.SplashFragment;
import com.favi.favi.util.Constants;
import com.favi.favi.util.toucharea.TouchAreaUtil;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.FeedViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeedActivity extends DialogActivity {

    public static final int RC_FEED_TYPE = 100;
    public static final String INTENT_FEED_TYPE_KEY = "intentFeedTypeKey";

    @BindView(R.id.srl)
    SwipeRefreshLayout srl;
    @BindView(R.id.rvFeed)
    RecyclerView rvFeed;
    @BindView(R.id.tvFeedCategory)
    TextView tvFeedCategory;

    private FeedViewModel feedViewModel;

    private FeedRVAdapter.FeedType feedType = FeedRVAdapter.FeedType.FEATURED;
    private boolean isLoading;
    private boolean isLastPage;

    private FavPlayer favPlayer;
    private FeedRVAdapter rvAdapter;

    private SplashFragment splashFragment;

    public static void start(Context context, FeedRVAdapter.FeedType feedType) {
        Intent starter = new Intent(context, FeedActivity.class);
        starter.putExtra(INTENT_FEED_TYPE_KEY, feedType);
        starter.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        initSplashFragment();

        ButterKnife.bind(this);

        generateUUIDIfNeed();

        favPlayer = new FavPlayer(this);
        initViews();
        favPlayer.setPlayerListener(rvAdapter);
        setupObservables();
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        feedViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(FeedViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(feedViewModel);
        return viewModels;
    }

    private void generateUUIDIfNeed() {
        if (sharedPreferences.getString(Constants.PREFS_UUID_KEY, null) == null) {
            String UUID = java.util.UUID.randomUUID().toString();
            sharedPreferences.edit().putString(Constants.PREFS_UUID_KEY, UUID).apply();
        }
    }

    private void initSplashFragment() {
        splashFragment = SplashFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.rootView, splashFragment, SplashFragment.TAG)
                .addToBackStack(SplashFragment.TAG)
                .commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() != null) {
            feedType = (FeedRVAdapter.FeedType) intent.getExtras().getSerializable(INTENT_FEED_TYPE_KEY);
            tvFeedCategory.setText(getFeedType());
            feedViewModel.getFeed(feedType, true, null);
        }
    }

    @SuppressWarnings("ConstantConditions")
    private void setupObservables() {
        feedViewModel.getFeed(FeedRVAdapter.FeedType.FEATURED, false, null)
                .observe(this, this::updateFeed);

        feedViewModel.isLoading().observe(this, this::setVisibilityFooterLoading);

        feedViewModel.feedType().observe(this, feedType -> this.feedType = feedType);

        feedViewModel.isLastPage().observe(this, isLastPage -> this.isLastPage = isLastPage);

        feedViewModel.shareIntent().observe(this, intent ->
                startActivity(Intent.createChooser(intent, getString(R.string.share_fav_to))));
    }

    private void setVisibilityFooterLoading(Boolean aBoolean) {
        isLoading = aBoolean;

        if (isLoading)
            rvAdapter.addFooterLoading();
        else
            rvAdapter.removeFooterLoading();
    }

    private void updateFeed(List<Feed> feeds) {
        rvAdapter.setFeedType(feedType);

        if (srl.isRefreshing()) srl.setRefreshing(false);

        if (feedViewModel.isFeedRefreshed()) {
            rvAdapter.clear();

            // Add header only for Lab feed type
            if (feedType == FeedRVAdapter.FeedType.LAB && rvAdapter.getItemCount() == 0) {
                rvAdapter.add(new LabHeader());
            }

            List<FeedRVAdapter.ItemViewType> list = new ArrayList<>();
            list.addAll(feeds);
            rvAdapter.addAll(list);
            return;
        }

        List<FeedRVAdapter.ItemViewType> list = new ArrayList<>();
        list.addAll(feeds);
        rvAdapter.addAll(list);

        if (splashFragment.isAdded())
            getSupportFragmentManager().beginTransaction().remove(splashFragment).commit();
    }

    private void initViews() {
        TouchAreaUtil.extendTouchArea(16, tvFeedCategory);
        initRV();
    }

    private void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);

        rvAdapter = new FeedRVAdapter(this, favPlayer, new FeedItemClickListener() {
            @Override
            public void onLikeDislikeClicked(int videoId, int position) {
                feedViewModel.postLike(videoId);
            }

            @Override
            public void onShareClicked(Feed feed) {
                if (!BuildConfig.DEBUG) {
                    analyticsHelper.sendShareAnalytics(AnalyticsHelper.FEED_SCREEN,
                            AnalyticsHelper.SHARE_WITH_NATIVE, feed.getTagId());
                }

                showCachingVideoDialog(feed);
            }

            @Override
            public void onReportClicked(int vidId) {
                showReportDialog(vidId);
            }

            @Override
            public void onCommentsClicked(int vidId, int comments, int position) {
                CommentsActivity.start(FeedActivity.this, vidId, comments, position);
            }

            @SuppressWarnings("ConstantConditions")
            @Override
            public void onProfileClicked(int userId, String userName, String userAvatar) {
                if (!isLoggedIn())
                    return;
                feedViewModel.startOtherProfile(userId).observe(FeedActivity.this, isStart -> {
                    if (isStart)
                        OtherUserProfileActivity.start(FeedActivity.this, userId, userName, userAvatar);
                });
            }
        });

        Pagination pagination = new Pagination(layoutManager, rvAdapter, favPlayer) {
            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public void loadMore() {
                feedViewModel.getFeed(feedType, false, null);
            }
        };

        rvFeed.setLayoutManager(layoutManager);
        rvFeed.addOnScrollListener(pagination);
        rvFeed.addItemDecoration(new FeedDecorator());
        rvFeed.setAdapter(rvAdapter);

        srl.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent));
        srl.setOnRefreshListener(() -> feedViewModel.getFeed(feedType, true, null));
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    protected void onResume() {
        super.onResume();
        getCacheDir().delete();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        favPlayer.release();
    }

    @OnClick({R.id.ivNewFav, R.id.tvFeedCategory})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivNewFav:
                startActivity(new Intent(this, SelectCharacterActivity.class));
                break;
            case R.id.tvFeedCategory:
                Intent intent = new Intent(this, SelectFeedActivity.class);
                intent.putExtra(SelectFeedActivity.INTENT_KEY_FEED_TYPE, feedType);
                startActivityForResult(intent, RC_FEED_TYPE);
                break;
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RC_FEED_TYPE:
                if (resultCode == RESULT_OK) {
                    if (feedType.name().equals(data.getExtras()
                            .getSerializable(SelectFeedActivity.INTENT_KEY_FEED_TYPE)))
                        return;

                    rvAdapter.clear();
                    feedType = (FeedRVAdapter.FeedType) data.getExtras()
                            .getSerializable(SelectFeedActivity.INTENT_KEY_FEED_TYPE);
                    tvFeedCategory.setText(getFeedType());
                    feedViewModel.getFeed(feedType, true, null);
                }
                break;
            case CommentsActivity.RC_RESULT:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getBundleExtra(CommentsActivity.INTENT_KEY_RESULT);
                    rvAdapter.updateCommentsCount(
                            bundle.getInt(CommentsActivity.INTENT_KEY_VIDEO_POSITION),
                            bundle.getInt(CommentsActivity.INTENT_KEY_COMMENTS_COUNT));
                }
                break;
        }
    }

    private CharSequence getFeedType() {
        switch (feedType) {
            case FEATURED:
                return getString(R.string.featured);
            case LAB:
                return getString(R.string.lab);
            default:
                throw new RuntimeException("Can`t define feed type!");
        }
    }
}
