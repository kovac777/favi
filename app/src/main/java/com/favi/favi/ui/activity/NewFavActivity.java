package com.favi.favi.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.favi.favi.BuildConfig;
import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.BaseRVAdapter;
import com.favi.favi.adapter.recyclerview.SelectableChipsRVAdapter;
import com.favi.favi.adapter.spinner.SpinnerCharacterAdapter;
import com.favi.favi.pojo.Chip;
import com.favi.favi.pojo.response.Character;
import com.favi.favi.pojo.response.SearchWords;
import com.favi.favi.util.Constants;
import com.favi.favi.util.FileUtil;
import com.favi.favi.util.KeyboardUtil;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.NewFavViewModel;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;


public class NewFavActivity extends DialogActivity implements View.OnClickListener, TextWatcher {

    public static final String INTENT_KEY_LIST_CHARACTERS = "intentListCharacters";
    public static final String INTENT_KEY_SELECTED_CHARACTER = "intentCharacter";
    public static final String INTENT_KEY_SELECTED_POSITION = "intentPosition";

    public static final int KNUCKLES_TAG_ID = 15;

    @BindView(R.id.toolbarNewFav)
    Toolbar toolbarNewFav;
    @BindView(R.id.tvToolbarNewFavTitle)
    TextView tvToolbarNewFavTitle;
    @BindView(R.id.ivNewFavMake)
    ImageView btnNewFavMake;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.materialProgressBar)
    MaterialProgressBar materialProgressBar;

    @BindView(R.id.spinner)
    AppCompatSpinner spinner;

    @BindView(R.id.etSearchWords)
    EditText etSearchWords;
    @BindView(R.id.ivBackSpace)
    ImageView ivBackSpace;
    @BindView(R.id.rvSelectableChips)
    RecyclerView rvSelectableChips;

    private NewFavViewModel newFavViewModel;

    private SelectableChipsRVAdapter selectableChipsRVAdapter;
    private int tag;
    private CountDownTimer countDownTimer;

    public static void start(Context context, ArrayList<Character> characters, Character character, int position) {
        Intent starter = new Intent(context, NewFavActivity.class);
        starter.putParcelableArrayListExtra(INTENT_KEY_LIST_CHARACTERS, characters);
        starter.putExtra(INTENT_KEY_SELECTED_CHARACTER, character);
        starter.putExtra(INTENT_KEY_SELECTED_POSITION, position);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_fav);

        ButterKnife.bind(this);

        initViews();
        setupObservables();
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        newFavViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(NewFavViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(newFavViewModel);
        return viewModels;
    }

    @SuppressWarnings("ConstantConditions")
    private void setupObservables() {
        newFavViewModel.searchWords("", tag)
                .observe(this, this::updateWords);

        newFavViewModel.getTag().observe(this, integer -> tag = integer);

        newFavViewModel.resetUI().observe(this, resetUI -> initViewVisibility());
    }

    private void updateWords(SearchWords searchWords) {
        if (searchWords == null) return;

        List<BaseRVAdapter.ItemViewType> chips = new ArrayList<>();
        for (int i = 0; i < searchWords.getWords().size(); i++) {
            chips.add(new Chip(searchWords.getWords().get(i).getText()));
        }

        selectableChipsRVAdapter.clear();
        selectableChipsRVAdapter.addAll(chips);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initViewVisibility();
    }

    private void initViewVisibility() {
        tvCancel.setVisibility(View.VISIBLE);
        btnNewFavMake.setVisibility(View.VISIBLE);

        materialProgressBar.setVisibility(View.GONE);

        rvSelectableChips.setVisibility(View.VISIBLE);

        spinner.setEnabled(true);
        spinner.setBackground(ContextCompat.getDrawable(this, R.drawable.spinner_background_with_arrow));

        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        tvToolbarNewFavTitle.setText(R.string.new_fav);

        etSearchWords.setEnabled(true);
        etSearchWords.setInputType(InputType.TYPE_CLASS_TEXT);

        ivBackSpace.setImageResource(R.drawable.ic_backspace_enabled_24dp);
        ivBackSpace.setClickable(true);
    }

    private void setupInput() {
        String text = sharedPreferences.getString(Constants.PREFS_PREVIOUS_INPUT_TEXT, "");
        etSearchWords.setText(text);
        etSearchWords.setSelection(etSearchWords.getText().length());

        etSearchWords.addTextChangedListener(this);

        etSearchWords.requestFocus();
    }

    private void setupMakeBtn() {
        if (etSearchWords.getText().length() > 0) {
            btnNewFavMake.setImageResource(R.drawable.make);
        } else btnNewFavMake.setImageResource(R.drawable.make_disabled);
    }

    @Override
    public void onBackPressed() {
        sharedPreferences.edit().putString(Constants.PREFS_PREVIOUS_INPUT_TEXT,
                etSearchWords.getText().toString()).apply();
        finish();
    }

    @SuppressWarnings("ConstantConditions")
    private void initViews() {
        setSupportActionBar(toolbarNewFav);
        setupSpinner();
        setupInput();
        setupMakeBtn();
        setupSelectableRV();
    }

    private void setupSpinner() {
        SpinnerCharacterAdapter adapter =
                new SpinnerCharacterAdapter(this, R.layout.spinner_item,
                        getIntent().getParcelableArrayListExtra(INTENT_KEY_LIST_CHARACTERS));
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tag = ((Character) adapterView.getSelectedItem()).getTagId();

                knucklesSelected(tag == KNUCKLES_TAG_ID);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner.setSelection(getIntent().getIntExtra(INTENT_KEY_SELECTED_POSITION, -1));
    }

    private void knucklesSelected(boolean isKnucklesSelected) {
        if (isKnucklesSelected) {
            etSearchWords.setFocusable(false);
            etSearchWords.setEnabled(false);
            etSearchWords.setText("");
            rvSelectableChips.requestFocus();
            KeyboardUtil.hideKeyboard(NewFavActivity.this);
        } else {
            etSearchWords.setFocusable(true);
            etSearchWords.setEnabled(true);
            KeyboardUtil.showKeyboard(NewFavActivity.this);
        }
    }

    private void setupSelectableRV() {
        FlexboxLayoutManager selectableFlexboxLayoutManager = new FlexboxLayoutManager(this);
        selectableFlexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        selectableFlexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);
        selectableFlexboxLayoutManager.setAlignItems(AlignItems.STRETCH);

        selectableChipsRVAdapter = new SelectableChipsRVAdapter(this, (word, position) -> {
            int lastSpace = etSearchWords.getText().toString().lastIndexOf(" ");
            String newInput;
            if (lastSpace != -1) {
                newInput = etSearchWords.getText().toString().substring(0, lastSpace) + " " + word + " ";
            } else newInput = word + " ";

            etSearchWords.setText(newInput);
            etSearchWords.setSelection(etSearchWords.getText().length());
        });

        rvSelectableChips.setLayoutManager(selectableFlexboxLayoutManager);
        rvSelectableChips.setAdapter(selectableChipsRVAdapter);
    }

    @SuppressWarnings("ConstantConditions")
    @OnClick({R.id.ivNewFavMake, R.id.tvCancel, R.id.ivBackSpace})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivNewFavMake:
                generateNewFav();
                break;
            case R.id.tvCancel:
                sharedPreferences.edit().putString(Constants.PREFS_PREVIOUS_INPUT_TEXT,
                        etSearchWords.getText().toString()).apply();
                finish();
                break;
            case R.id.ivBackSpace:
                removeLastWord();
                break;
        }
    }

    private void removeLastWord() {
        String input = etSearchWords.getText().toString().trim();
        int indexSpace = input.lastIndexOf(" ");
        if (indexSpace != -1) {
            input = input.substring(0, indexSpace) + " ";
            etSearchWords.setText(input);
            etSearchWords.setSelection(etSearchWords.getText().length());
        } else
            etSearchWords.setText("");
    }

    @SuppressWarnings("ConstantConditions")
    private void generateNewFav() {
        if (etSearchWords.getText().length() == 0)
            return;

        updateUI();

        if (FileUtil.isExternalStorageAvailable()) {
            newFavViewModel.generateFavAndSaveToDB(etSearchWords.getText().toString(), tag)
                    .observe(this, aLong -> ShareActivity.start(
                            this, aLong, etSearchWords.getText().toString()));

            if (!BuildConfig.DEBUG) {
                analyticsHelper.sendCreateFavAnalytics(tag);
            }
        } else {
            Toast.makeText(this, "External storage unavailable!", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateUI() {
        KeyboardUtil.hideKeyboard(this);

        materialProgressBar.setVisibility(View.VISIBLE);

        tvCancel.setVisibility(View.GONE);
        btnNewFavMake.setVisibility(View.GONE);

        rvSelectableChips.setVisibility(View.GONE);

        spinner.setEnabled(false);
        spinner.setBackground(null);

        etSearchWords.setEnabled(false);
        etSearchWords.setInputType(InputType.TYPE_NULL);

        ivBackSpace.setImageResource(R.drawable.ic_backspace_disabled_24dp);
        ivBackSpace.setClickable(false);

        createTimer();
    }

    private void createTimer() {
        countDownTimer = new CountDownTimer(21000, 1000) {

            @Override
            public void onTick(long l) {
                String remainingTime = String.valueOf(l / 1000);
                tvToolbarNewFavTitle.setText(remainingTime);
            }

            @Override
            public void onFinish() {
                tvToolbarNewFavTitle.setText(getString(R.string.faving_in_progress));
            }
        };

        countDownTimer.cancel();
        countDownTimer.start();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length() > 0)
            ivBackSpace.setVisibility(View.VISIBLE);
        else
            ivBackSpace.setVisibility(View.GONE);

        setupMakeBtn();

        try {
            String query = s.toString().substring(s.toString().lastIndexOf(" ") + 1);
            newFavViewModel.searchWords(query, tag);
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
}
