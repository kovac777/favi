package com.favi.favi.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.favi.favi.BuildConfig;
import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.FeedItemClickListener;
import com.favi.favi.adapter.recyclerview.FeedRVAdapter;
import com.favi.favi.adapter.recyclerview.Pagination;
import com.favi.favi.adapter.recyclerview.decorator.FeedDecorator;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.exoplayer.FavPlayer;
import com.favi.favi.glide.GlideApp;
import com.favi.favi.helper.AnalyticsHelper;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.FeedViewModel;
import com.favi.favi.viewmodel.ProfileViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import de.hdodenhof.circleimageview.CircleImageView;

public class OtherUserProfileActivity extends DialogActivity {

    public static final String INTENT_KEY_OTHER_USER_ID = "intentKeyOtherUserId";
    public static final String INTENT_KEY_OTHER_USER_NAME = "intentKeyOtherUserName";
    public static final String INTENT_KEY_OTHER_USER_AVATAR = "intentKeyOtherUserAvatar";

    @BindView(R.id.ivAvatar)
    CircleImageView ivAvatar;
    @BindView(R.id.tvProfileName)
    TextView tvProfileName;
    @BindView(R.id.srl)
    SwipeRefreshLayout srl;
    @BindView(R.id.rvFeed)
    RecyclerView rvFeed;

    private FeedViewModel feedViewModel;
    private ProfileViewModel profileViewModel;

    private FeedRVAdapter feedRVAdapter;
    private boolean isLoading;
    private boolean isLastPage;

    private FavPlayer favPlayer;

    public static void start(Context context, int otherUserId, String userName, String userAvatar) {
        Intent starter = new Intent(context, OtherUserProfileActivity.class);
        starter.putExtra(INTENT_KEY_OTHER_USER_ID, otherUserId);
        starter.putExtra(INTENT_KEY_OTHER_USER_NAME, userName);
        starter.putExtra(INTENT_KEY_OTHER_USER_AVATAR, userAvatar);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_user_profile);
        ButterKnife.bind(this);

        favPlayer = new FavPlayer(this);
        initViews();
        favPlayer.setPlayerListener(feedRVAdapter);
        setupObservables();
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        feedViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(FeedViewModel.class);
        profileViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ProfileViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(feedViewModel);
        viewModels.add(profileViewModel);
        return viewModels;
    }

    private void setupObservables() {
        feedViewModel.getFeed(FeedRVAdapter.FeedType.OTHER_USER_FAVS, false,
                getIntent().getIntExtra(INTENT_KEY_OTHER_USER_ID, 0))
                .observe(this, this::updateFeed);

        feedViewModel.shareIntent().observe(this, intent ->
                startActivity(Intent.createChooser(intent, getString(R.string.share_fav_to))));

        feedViewModel.isLoading().observe(this, this::setVisibilityFooterLoading);

        feedViewModel.isLastPage().observe(this, isLastPage -> this.isLastPage = isLastPage);
    }

    private void initViews() {
        tvProfileName.setText(getIntent().getStringExtra(INTENT_KEY_OTHER_USER_NAME));

        GlideApp.with(this)
                .load(getIntent().getStringExtra(INTENT_KEY_OTHER_USER_AVATAR))
                .into(ivAvatar);

        initRV();
    }

    private void setVisibilityFooterLoading(Boolean aBoolean) {
        isLoading = aBoolean;

        if (isLoading)
            feedRVAdapter.addFooterLoading();
        else
            feedRVAdapter.removeFooterLoading();
    }

    private void updateFeed(List<Feed> feeds) {
        feedRVAdapter.setFeedType(FeedRVAdapter.FeedType.OTHER_USER_FAVS);

        if (srl.isRefreshing()) srl.setRefreshing(false);

        if (feedViewModel.isFeedRefreshed()) {
            feedRVAdapter.clear();

            List<FeedRVAdapter.ItemViewType> list = new ArrayList<>();
            list.addAll(feeds);
            feedRVAdapter.addAll(list);
            return;
        }

        List<FeedRVAdapter.ItemViewType> list = new ArrayList<>();
        list.addAll(feeds);
        feedRVAdapter.addAll(list);
    }

    private void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);

        feedRVAdapter = new FeedRVAdapter(this, favPlayer, new FeedItemClickListener() {
            @Override
            public void onLikeDislikeClicked(int videoId, int position) {
                feedViewModel.postLike(videoId);
            }

            @SuppressWarnings("ConstantConditions")
            @Override
            public void onShareClicked(Feed feed) {
                if (!BuildConfig.DEBUG) {
                    analyticsHelper.sendShareAnalytics(AnalyticsHelper.OTHER_USERS_FAVS_SCREEN,
                            AnalyticsHelper.SHARE_WITH_NATIVE, feed.getTagId());
                }

                showCachingVideoDialog(feed);
            }

            @Override
            public void onReportClicked(int vidId) {
                showReportDialog(vidId);
            }

            @Override
            public void onCommentsClicked(int vidId, int comments, int position) {
                CommentsActivity.start(OtherUserProfileActivity.this, vidId, comments, position);
            }

            @SuppressWarnings("ConstantConditions")
            @Override
            public void onProfileClicked(int userId, String userName, String userAvatar) {
                OtherUserProfileActivity.start(OtherUserProfileActivity.this, userId, userName, userAvatar);
            }
        });

        Pagination pagination = new Pagination(layoutManager, feedRVAdapter, favPlayer) {
            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public void loadMore() {
                feedViewModel.getFeed(FeedRVAdapter.FeedType.OTHER_USER_FAVS, false,
                        getIntent().getIntExtra(INTENT_KEY_OTHER_USER_ID, 0));
            }
        };

        rvFeed.setLayoutManager(layoutManager);
        rvFeed.addOnScrollListener(pagination);
        rvFeed.addItemDecoration(new FeedDecorator());
        rvFeed.setAdapter(feedRVAdapter);

        srl.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent));
        srl.setOnRefreshListener(() -> feedViewModel.getFeed(FeedRVAdapter.FeedType.OTHER_USER_FAVS, true,
                getIntent().getIntExtra(INTENT_KEY_OTHER_USER_ID, 0)));
    }

    @OnClick({R.id.ivBack})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    protected void onResume() {
        super.onResume();
        getCacheDir().delete();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        favPlayer.release();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CommentsActivity.RC_RESULT:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getBundleExtra(CommentsActivity.INTENT_KEY_RESULT);
                    feedRVAdapter.updateCommentsCount(
                            bundle.getInt(CommentsActivity.INTENT_KEY_VIDEO_POSITION),
                            bundle.getInt(CommentsActivity.INTENT_KEY_COMMENTS_COUNT));
                }
                break;
        }
    }
}
