package com.favi.favi.ui.activity;

import com.favi.favi.viewmodel.BaseViewModel;

/**
 * Created by User on 30.01.2018.
 * My SKYPE: maxvinnik
 */

public interface HandleError {

    void httpException(BaseViewModel viewModel);

    void ioException(BaseViewModel viewModel);

    void socketTimeoutException(BaseViewModel viewModel);

    void unknownException(BaseViewModel viewModel);

}
