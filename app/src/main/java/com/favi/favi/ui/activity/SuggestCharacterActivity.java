package com.favi.favi.ui.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;

import com.favi.favi.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SuggestCharacterActivity extends AppCompatActivity {

    public static final String SUGGEST_CHARACTER_FORM_URL = "https://docs.google.com/forms/d/e/" +
            "1FAIpQLSdsDHN0W6UZ_uO9PixEDSaHTJI4oMFB21bwkdLLI3je6Ya_Mg/viewform?usp=sf_link";

    @BindView(R.id.webView)
    WebView webView;

    public static void start(Context context) {
        Intent starter = new Intent(context, SuggestCharacterActivity.class);
        context.startActivity(starter);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_character);

        ButterKnife.bind(this);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(SUGGEST_CHARACTER_FORM_URL);
    }

    @OnClick(R.id.ivClose)
    public void onClick(View view) {
        finish();
    }
}
