package com.favi.favi.ui.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.favi.favi.R;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.ProfileViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dagger.android.support.AndroidSupportInjection;

/**
 * Created by User on 23.01.2018.
 * My SKYPE: maxvinnik
 */

@SuppressWarnings("ConstantConditions")
public class SignInDialog extends BaseDialog implements View.OnClickListener {

    public static final String TAG = SignInDialog.class.getSimpleName();

    private ProfileViewModel profileViewModel;

    private CallbackManager callbackManager;

    public static SignInDialog newInstance() {
        Bundle args = new Bundle();
        SignInDialog fragment = new SignInDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        profileViewModel = ViewModelProviders.of(getActivity(), viewModelFactory)
                .get(ProfileViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(profileViewModel);
        return viewModels;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
    }

    @SuppressLint("InflateParams")
    @SuppressWarnings("ConstantConditions")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View root = getActivity().getLayoutInflater().inflate(R.layout.dialog_sign_in, null);

        ImageView ivClose = root.findViewById(R.id.ivClose);
        ImageView ivSignInFacebook = root.findViewById(R.id.ivSignInFacebook);

        ivClose.setOnClickListener(this);
        ivSignInFacebook.setOnClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(root)
                .setCancelable(false);

        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_background);

        return dialog;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                dismiss();
                break;
            case R.id.ivSignInFacebook:
                loginFB();
                break;
        }
    }

    private void loginFB() {
        profileViewModel.loginFB(callbackManager);
        LoginManager.getInstance()
                .logInWithReadPermissions(this,
                        Collections.singletonList("public_profile"));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        dismiss();
    }
}
