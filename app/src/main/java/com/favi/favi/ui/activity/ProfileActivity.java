package com.favi.favi.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.favi.favi.BuildConfig;
import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.FeedRVAdapter;
import com.favi.favi.adapter.viewpager.ProfileViewPagerAdapter;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.db.entity.LocalProfile;
import com.favi.favi.exoplayer.FavPlayer;
import com.favi.favi.glide.GlideApp;
import com.favi.favi.helper.AnalyticsHelper;
import com.favi.favi.ui.fragment.LikedFavsFragment;
import com.favi.favi.ui.fragment.MyFavsFragment;
import com.favi.favi.ui.fragment.ProfileFeedFragment;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.FeedViewModel;
import com.favi.favi.viewmodel.ProfileViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends DialogActivity implements FeedRVAdapter.ItemClickListener {

    @BindView(R.id.rootView)
    CoordinatorLayout rootView;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvToolbarTitle)
    TextView tvToolbarTitle;
    @BindView(R.id.ivAvatar)
    CircleImageView ivAvatar;
    @BindView(R.id.tvProfileName)
    TextView tvProfileName;
    @BindView(R.id.ivEditProfile)
    ImageView ivEditProfile;
    @BindView(R.id.groupLogin)
    Group groupLogin;
    @BindView(R.id.groupProfile)
    Group groupProfile;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private ProfileViewModel profileViewModel;
    private FeedViewModel feedViewModel;

    private CallbackManager callbackManager;

    private ProfileViewPagerAdapter pagerAdapter;

    private LocalProfile localProfile;

    private FavPlayer favPlayer;

    public static void start(Context context) {
        Intent starter = new Intent(context, ProfileActivity.class);
        starter.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        favPlayer = new FavPlayer(this);
        initViews();
        setupObservables();
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        profileViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ProfileViewModel.class);
        feedViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(FeedViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(profileViewModel);
        viewModels.add(feedViewModel);
        return viewModels;
    }

    private void setupObservables() {
        profileViewModel.getProfile().observe(this, this::processProfile);

        feedViewModel.shareIntent().observe(this, intent ->
                startActivity(Intent.createChooser(intent, getString(R.string.share_fav_to))));
    }

    private void processProfile(@Nullable LocalProfile localProfile) {
        if (localProfile != null) {
            this.localProfile = localProfile;
            updateUI(true);
        } else {
            updateUI(false);
        }
    }

    private void initViews() {
        initFacebook();

        setupTabs();
        fab.setOnClickListener(view -> SelectCharacterActivity.start(this));
    }

    private void initFacebook() {
        callbackManager = CallbackManager.Factory.create();
    }

    private void updateUI(boolean isLogged) {
        if (isLogged) {
            GlideApp.with(this)
                    .load(localProfile.getImage())
                    .into(ivAvatar);
            tvProfileName.setText(localProfile.getUserName());

            TransitionManager.beginDelayedTransition(rootView);
            groupLogin.setVisibility(View.GONE);
            groupProfile.setVisibility(View.VISIBLE);
            ivEditProfile.setVisibility(View.VISIBLE);
        } else {
            TransitionManager.beginDelayedTransition(rootView);
            ivEditProfile.setVisibility(View.GONE);
            groupProfile.setVisibility(View.GONE);
            groupLogin.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressWarnings("ConstantConditions")
    private void setupTabs() {
        pagerAdapter = new ProfileViewPagerAdapter(getSupportFragmentManager(), getPagerFragments());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager, true);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setCustomView(R.layout.profile_tab);
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                updateTabUI(tab, true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                updateTabUI(tab, false);
            }
        });

        updateTabUI(tabLayout.getTabAt(0), true);
        updateTabUI(tabLayout.getTabAt(1), false);

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                ((ProfileFeedFragment) pagerAdapter.getItem(position)).stopPlayer();
            }
        });
    }

    @SuppressWarnings("ConstantConditions")
    private void updateTabUI(TabLayout.Tab tab, boolean isSelected) {
        ImageView icon = tab.getCustomView().findViewById(R.id.imageView);
        TextView title = tab.getCustomView().findViewById(R.id.textView);

        if (isSelected) {
            switch (tab.getPosition()) {
                case 0:
                    icon.setImageResource(R.drawable.myfavs_on);
                    if (Build.VERSION.SDK_INT < 23) {
                        title.setTextAppearance(this, R.style.TextSelectedTab);
                    } else {
                        title.setTextAppearance(R.style.TextSelectedTab);
                    }
                    title.setText(R.string.my_favs);
                    break;
                case 1:
                    icon.setImageResource(R.drawable.likedfavs_on);
                    if (Build.VERSION.SDK_INT < 23) {
                        title.setTextAppearance(this, R.style.TextSelectedTab);
                    } else {
                        title.setTextAppearance(R.style.TextSelectedTab);
                    }
                    title.setText(R.string.liked_favs);
                    break;
            }
        } else {
            switch (tab.getPosition()) {
                case 0:
                    icon.setImageResource(R.drawable.myfavs_off);
                    if (Build.VERSION.SDK_INT < 23) {
                        title.setTextAppearance(this, R.style.TextUnselectedTab);
                    } else {
                        title.setTextAppearance(R.style.TextUnselectedTab);
                    }
                    title.setText(R.string.my_favs);
                    break;
                case 1:
                    icon.setImageResource(R.drawable.likedfavs_off);
                    if (Build.VERSION.SDK_INT < 23) {
                        title.setTextAppearance(this, R.style.TextUnselectedTab);
                    } else {
                        title.setTextAppearance(R.style.TextUnselectedTab);
                    }
                    title.setText(R.string.liked_favs);
                    break;
            }
        }
    }

    private List<Fragment> getPagerFragments() {
        MyFavsFragment myFavsFragment = MyFavsFragment.newInstance();
        myFavsFragment.setFavPlayer(favPlayer);
        LikedFavsFragment likedFavsFragment = LikedFavsFragment.newInstance();
        likedFavsFragment.setFavPlayer(favPlayer);

        List<Fragment> fragments = new ArrayList<>();
        fragments.add(myFavsFragment);
        fragments.add(likedFavsFragment);

        return fragments;
    }

    @OnClick({R.id.ivBack, R.id.ivSignInFB, R.id.ivEditProfile})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivSignInFB:
                profileViewModel.loginFB(callbackManager);
                LoginManager.getInstance()
                        .logInWithReadPermissions(this,
                                Collections.singletonList("public_profile"));
                break;
            case R.id.ivEditProfile:
                SettingsActivity.start(this, localProfile);
                break;
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    protected void onResume() {
        super.onResume();
        getCacheDir().delete();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        favPlayer.release();
    }

    @Override
    public void onLikeDislikeClicked(int videoId, int position) {
        feedViewModel.postLike(videoId);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onShareClicked(Feed feed) {
        if (!BuildConfig.DEBUG) {
            analyticsHelper.sendShareAnalytics(getScreenForAnalytics(),
                    AnalyticsHelper.SHARE_WITH_NATIVE, feed.getTagId());
        }

        showCachingVideoDialog(feed);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onPostToLabClicked(Feed feed) {
        if (!isLoggedIn()) {
            return;
        }

        feedViewModel.postFavInLab(feed).observe(this, updatedFeed ->
                ((ProfileFeedFragment) pagerAdapter.getItem(viewPager.getCurrentItem()))
                        .getRvAdapter().updateItem(updatedFeed, updatedFeed.getAdapterPosition()));
    }

    @Override
    public void onReportClicked(int vidId) {
        showReportDialog(vidId);
    }

    @Override
    public void onCommentsClicked(int vidId, int comments, int position) {
        CommentsActivity.start(this, vidId, comments, position);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onProfileClicked(int userId, String userName, String userAvatar) {
        if (!isLoggedIn())
            return;

        feedViewModel.startOtherProfile(userId).observe(this, isStart -> {
            if (isStart)
                OtherUserProfileActivity.start(this, userId, userName, userAvatar);
        });
    }

    private String getScreenForAnalytics() {
        switch (viewPager.getCurrentItem()) {
            case 0:
                return AnalyticsHelper.MY_FAVS_SCREEN;
            case 1:
                return AnalyticsHelper.MY_FAVORITES_FAVS_SCREEN;
            default:
                throw new RuntimeException("Can`t define screen!");
        }
    }
}
