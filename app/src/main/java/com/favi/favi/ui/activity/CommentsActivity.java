package com.favi.favi.ui.activity;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.CommentsRVAdapter;
import com.favi.favi.adapter.recyclerview.Pagination;
import com.favi.favi.adapter.recyclerview.decorator.FeedDecorator;
import com.favi.favi.pojo.response.Comments;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.CommentsViewModel;
import com.favi.favi.viewmodel.FeedViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommentsActivity extends DialogActivity implements CommentsRVAdapter.ItemClickListener {

    public static final int RC_RESULT = 1001;
    public static final String INTENT_KEY_RESULT = "intentKeyResult";
    public static final String INTENT_KEY_VIDEO_ID = "intentKeyVideoId";
    public static final String INTENT_KEY_COMMENTS_COUNT = "intentKeyCommentsCount";
    public static final String INTENT_KEY_VIDEO_POSITION = "intentKeyVideoPosition";

    @BindView(R.id.rootView)
    ConstraintLayout rootView;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.srl)
    SwipeRefreshLayout srl;
    @BindView(R.id.rvComments)
    RecyclerView rvComments;
    @BindView(R.id.etComment)
    EditText etComment;

    private CommentsViewModel commentsViewModel;
    private FeedViewModel feedViewModel;

    private int videoId;
    private int commentsCount;
    private int videoPosition;

    private CommentsRVAdapter rvAdapter;
    private boolean isLoading;
    private boolean isLastPage;

    public static void start(Activity activity, int videoId, int commentsCount, int videoPosition) {
        Intent starter = new Intent(activity, CommentsActivity.class);
        starter.putExtra(INTENT_KEY_VIDEO_ID, videoId);
        starter.putExtra(INTENT_KEY_COMMENTS_COUNT, commentsCount);
        starter.putExtra(INTENT_KEY_VIDEO_POSITION, videoPosition);
        activity.startActivityForResult(starter, RC_RESULT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        ButterKnife.bind(this);

        initViews();
        setupObservables();
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        commentsViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(CommentsViewModel.class);
        feedViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(FeedViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(commentsViewModel);
        viewModels.add(feedViewModel);
        return viewModels;
    }

    @SuppressWarnings("ConstantConditions")
    private void setupObservables() {
        commentsViewModel.getComments(videoId, false).observe(this, this::updateComments);
        commentsViewModel.postComment().observe(this, this::addComment);

        commentsViewModel.editComment().observe(this, comment -> {

        });

        commentsViewModel.deleteComment().observe(this, comment -> {

        });

        commentsViewModel.isLoading().observe(this, this::setVisibilityFooterLoading);
        commentsViewModel.isLastPage().observe(this, isLastPage -> this.isLastPage = isLastPage);
    }

    private void addComment(Comments.CommentsBean comment) {
        commentsCount = commentsCount + 1;
        rvAdapter.addComment(comment);
        etComment.setText("");
        initTitle(commentsCount);

        Bundle bundle = new Bundle();
        bundle.putInt(INTENT_KEY_VIDEO_POSITION, videoPosition);
        bundle.putInt(INTENT_KEY_COMMENTS_COUNT, commentsCount);
        setResult(Activity.RESULT_OK, new Intent().putExtra(INTENT_KEY_RESULT, bundle));
    }

    private void updateComments(Comments comments) {
        if (srl.isRefreshing()) srl.setRefreshing(false);

        if (commentsViewModel.isFeedRefreshed()) {
            rvAdapter.clear();

            List<CommentsRVAdapter.ItemViewType> list = new ArrayList<>();
            list.addAll(comments.getComments());
            rvAdapter.addAll(list);
            return;
        }

        List<CommentsRVAdapter.ItemViewType> list = new ArrayList<>();
        list.addAll(comments.getComments());
        rvAdapter.addAll(list);
    }

    private void setVisibilityFooterLoading(Boolean aBoolean) {
        isLoading = aBoolean;

        if (isLoading)
            rvAdapter.addFooterLoading();
        else
            rvAdapter.removeFooterLoading();
    }

    private void initViews() {
        videoId = getIntent().getIntExtra(INTENT_KEY_VIDEO_ID, -1);
        commentsCount = getIntent().getIntExtra(INTENT_KEY_COMMENTS_COUNT, -1);
        videoPosition = getIntent().getIntExtra(INTENT_KEY_VIDEO_POSITION, -1);

        initTitle(commentsCount);
        initRV();
        initET();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initTitle(int count) {
        String title;

        if (count == -1) {
            title = getString(R.string.comments);
        } else if (count == 1) {
            title = count + " " + getString(R.string.comment);
        } else
            title = count + " " + getString(R.string.comments);

        tvTitle.setText(title);
    }

    private void initET() {
        etComment.setOnFocusChangeListener((view, isFocused) -> updatePostButtonUI(isFocused));
    }

    private void updatePostButtonUI(boolean isFocused) {
        ConstraintSet set = new ConstraintSet();
        set.clone(rootView);

        TransitionManager.beginDelayedTransition(rootView);
        if (isFocused) {
            set.setVisibility(R.id.ivPost, ConstraintSet.VISIBLE);
            set.applyTo(rootView);
        } else {
            set.setVisibility(R.id.ivPost, ConstraintSet.GONE);
            set.applyTo(rootView);
        }
    }

    private void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);

        rvAdapter = new CommentsRVAdapter(this, this);

        Pagination pagination = new Pagination(layoutManager, rvAdapter, null) {
            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public void loadMore() {
                commentsViewModel.getComments(videoId, false);
            }
        };

        rvComments.setLayoutManager(layoutManager);
        rvComments.addOnScrollListener(pagination);
        rvComments.addItemDecoration(new FeedDecorator());
        rvComments.setAdapter(rvAdapter);

        srl.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent));
        srl.setOnRefreshListener(() -> commentsViewModel.getComments(videoId, true));
    }

    @OnClick({R.id.ivBack, R.id.ivPost})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivPost:
                if (!isLoggedIn()) {
                    return;
                }

                if (!TextUtils.isEmpty(etComment.getText().toString())) {
                    commentsViewModel.post(videoId, etComment.getText().toString());
                }
                break;
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onProfileClicked(int userId, String userName, String userAvatar) {
        if (!isLoggedIn())
            return;
        feedViewModel.startOtherProfile(userId).observe(this, isStart -> {
            if (isStart)
                OtherUserProfileActivity.start(this, userId, userName, userAvatar);
        });
    }

    @Override
    public void onEditClicked(int commentId) {

    }

    @Override
    public void onDeleteClicked(int commentId) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
