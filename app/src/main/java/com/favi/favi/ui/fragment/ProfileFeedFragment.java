package com.favi.favi.ui.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.FeedRVAdapter;
import com.favi.favi.adapter.recyclerview.Pagination;
import com.favi.favi.adapter.recyclerview.decorator.FeedDecorator;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.di.ViewModelFactory;
import com.favi.favi.exoplayer.FavPlayer;
import com.favi.favi.ui.activity.CommentsActivity;
import com.favi.favi.viewmodel.FeedViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

import static android.app.Activity.RESULT_OK;

/**
 * Created by maxvinnik on 17.01.2018.
 */

public abstract class ProfileFeedFragment extends Fragment {

    @Inject
    ViewModelFactory viewModelFactory;
    @BindView(R.id.srl)
    SwipeRefreshLayout srl;
    @BindView(R.id.rvFavs)
    RecyclerView rvFavs;
    private FeedViewModel feedViewModel;
    private boolean isLoading;
    private boolean isLastPage;
    private FavPlayer favPlayer;
    private FeedRVAdapter rvAdapter;

    public abstract FeedRVAdapter.FeedType getFeedType();

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initViews();
        favPlayer.setPlayerListener(rvAdapter);
        setupObservables();
    }

    public void setFavPlayer(FavPlayer favPlayer) {
        this.favPlayer = favPlayer;
    }

    @SuppressWarnings("ConstantConditions")
    private void setupObservables() {
        feedViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(FeedViewModel.class);

        feedViewModel.getFeed(getFeedType(), false, null)
                .observe(this, this::updateFeed);

        feedViewModel.isLoading().observe(this, this::setVisibilityFooterLoading);

        feedViewModel.isLastPage().observe(this, aBoolean -> isLastPage = aBoolean);

        //Handle exceptions
        feedViewModel.httpException().observe(this, throwable -> {
        });
        feedViewModel.ioException().observe(this, throwable -> {
        });
        feedViewModel.socketTimeOutException().observe(this, throwable -> {
            srl.setRefreshing(false);
            Toast.makeText(getActivity(), "Server is not responding!!!", Toast.LENGTH_SHORT).show();
        });
        feedViewModel.unknownException().observe(this, throwable -> {
            Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    private void setVisibilityFooterLoading(Boolean aBoolean) {
        isLoading = aBoolean;

        if (isLoading)
            rvAdapter.addFooterLoading();
        else
            rvAdapter.removeFooterLoading();
    }

    private void updateFeed(List<Feed> feeds) {
        rvAdapter.setFeedType(getFeedType());

        if (srl.isRefreshing()) srl.setRefreshing(false);

        if (feedViewModel.isFeedRefreshed()) {
            rvAdapter.clear();

            List<FeedRVAdapter.ItemViewType> list = new ArrayList<>();
            list.addAll(feeds);
            rvAdapter.addAll(list);
            return;
        }

        List<FeedRVAdapter.ItemViewType> list = new ArrayList<>();
        list.addAll(feeds);
        rvAdapter.addAll(list);
    }

    private void initViews() {
        initRV();
    }

    @SuppressWarnings("ConstantConditions")
    private void initRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);

        rvAdapter = new FeedRVAdapter(getActivity(), favPlayer,
                ((FeedRVAdapter.ItemClickListener) getActivity()));

        Pagination pagination = new Pagination(layoutManager, rvAdapter, favPlayer) {
            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public void loadMore() {
                feedViewModel.getFeed(getFeedType(), false, null);
            }
        };

        rvFavs.setLayoutManager(layoutManager);
        rvFavs.addOnScrollListener(pagination);
        rvFavs.addItemDecoration(new FeedDecorator());
        rvFavs.setAdapter(rvAdapter);

        srl.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        srl.setOnRefreshListener(() ->
                feedViewModel.getFeed(getFeedType(), true, null));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CommentsActivity.RC_RESULT:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getBundleExtra(CommentsActivity.INTENT_KEY_RESULT);
                    rvAdapter.updateCommentsCount(
                            bundle.getInt(CommentsActivity.INTENT_KEY_VIDEO_POSITION),
                            bundle.getInt(CommentsActivity.INTENT_KEY_COMMENTS_COUNT));
                }
                break;
        }
    }

    public void stopPlayer() {
        if (favPlayer.isPlaying()) {
            rvAdapter.videoFinished();
        }
    }

    public FeedRVAdapter getRvAdapter() {
        return rvAdapter;
    }
}
