package com.favi.favi.ui.activity;

import com.favi.favi.db.entity.Feed;
import com.favi.favi.ui.dialog.CachingVideoDialog;
import com.favi.favi.ui.dialog.RateAppDialog;
import com.favi.favi.ui.dialog.ReportDialog;
import com.favi.favi.ui.dialog.SignInDialog;
import com.favi.favi.util.Constants;
import com.favi.favi.util.TimeUtil;

/**
 * Created by User on 31.01.2018.
 * My SKYPE: maxvinnik
 */

public abstract class DialogActivity extends BaseActivity {

    private SignInDialog signInDialog;
    private CachingVideoDialog cachingVideoDialog;
    private ReportDialog reportDialog;
    private RateAppDialog rateAppDialog;

    protected boolean isLoggedIn() {
        if (!sharedPreferences.getBoolean(Constants.PREFS_IS_LOGGED_IN, false)) {
            if (signInDialog == null)
                signInDialog = SignInDialog.newInstance();
            signInDialog.show(getSupportFragmentManager(), SignInDialog.TAG);
            return false;
        } else
            return true;
    }

    protected void showCachingVideoDialog(Feed feed) {
        cachingVideoDialog = CachingVideoDialog.newInstance(feed);
    }

    protected void showReportDialog(int vidId) {
        if (reportDialog == null) {
            reportDialog = ReportDialog.newInstance(vidId);
        }
        reportDialog.show(getSupportFragmentManager(), ReportDialog.TAG);
    }

    protected void isNeedRateDialog() {
        if (TimeUtil.isRateAppNeed(sharedPreferences.getString(Constants.PREFS_LAST_RATE_MILLIS, null))) {
            if (rateAppDialog == null) {
                rateAppDialog = RateAppDialog.newInstance();
            }
            rateAppDialog.show(getSupportFragmentManager(), RateAppDialog.TAG);
        } else {
            ProfileActivity.start(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (signInDialog != null && signInDialog.isVisible())
            signInDialog.dismiss();
        if (cachingVideoDialog != null && cachingVideoDialog.isVisible())
            cachingVideoDialog.dismiss();
        if (reportDialog != null && reportDialog.isVisible())
            reportDialog.dismiss();
        if (rateAppDialog != null && rateAppDialog.isVisible()) {
            rateAppDialog.dismiss();
        }
    }
}
