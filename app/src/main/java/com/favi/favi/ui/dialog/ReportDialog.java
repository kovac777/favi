package com.favi.favi.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.View;

import com.favi.favi.R;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.FeedViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kovac on 12.12.2017.
 * My SKYPE: maxvinnik
 */

public class ReportDialog extends BaseDialog {

    public static final String TAG = ReportDialog.class.getSimpleName();

    public static final String BUNDLE_VIDID_KEY = "BundleVidIdKey";

    private FeedViewModel feedViewModel;

    public static ReportDialog newInstance(int vidId) {
        Bundle args = new Bundle();
        args.putInt(BUNDLE_VIDID_KEY, vidId);
        ReportDialog fragment = new ReportDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("ConstantConditions")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View root = getActivity().getLayoutInflater().inflate(R.layout.dialog_report, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(root)
                .setCancelable(false)
                .setNegativeButton(R.string.no, (dialogInterface, i) -> dismiss())
                .setPositiveButton(R.string.yes, (dialogInterface, i) -> {
                    Snackbar.make(root, getString(R.string.report_successful), Snackbar.LENGTH_SHORT).show();

//                    feedViewModel.postReport(getArguments().getInt(BUNDLE_VIDID_KEY))
//                            .observe(this, isReportPosted -> {
//                        if (isReportPosted) {
//
//                        }
//                    });

//                    dismiss();
                });


        return builder.create();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected List<BaseViewModel> initViewModels() {
        feedViewModel = ViewModelProviders.of(getActivity(), viewModelFactory)
                .get(FeedViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(feedViewModel);
        return viewModels;
    }
}
