package com.favi.favi.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.favi.favi.R;
import com.favi.favi.util.Constants;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        ButterKnife.bind(this);
    }

    @OnClick({R.id.ivClose, R.id.ivFacebook, R.id.ivInstagram, R.id.ivTwitter, R.id.ivYoutube})
    public void doClick(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.ivFacebook:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.LINK_FACEBOOK)));
                break;
            case R.id.ivInstagram:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.LINK_INSTAGRAM)));
                break;
            case R.id.ivTwitter:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.LINK_TWITTER)));
                break;
            case R.id.ivYoutube:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.LINK_YOUTUBE)));
                break;
        }
    }
}
