package com.favi.favi.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.FeedRVAdapter;
import com.favi.favi.util.toucharea.TouchAreaUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectFeedActivity extends AppCompatActivity {

    public static final String INTENT_KEY_FEED_TYPE = "intentKeyFeedType";

    @BindView(R.id.tvFeatured)
    TextView tvFeatured;
    @BindView(R.id.tvLab)
    TextView tvLab;
    @BindView(R.id.tvMyFavs)
    TextView tvMyFavs;
    @BindView(R.id.tvInfo)
    TextView tvInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_feed);

        ButterKnife.bind(this);

        TouchAreaUtil.extendTouchAreaLeft(48, tvInfo);
        TouchAreaUtil.extendTouchAreaBottom(16, tvInfo);
        TouchAreaUtil.extendTouchAreaTop(16, tvInfo);

        setupSelectedFeed();
    }

    @SuppressWarnings("ConstantConditions")
    private void setupSelectedFeed() {
        FeedRVAdapter.FeedType feedType =
                (FeedRVAdapter.FeedType) getIntent().getExtras().getSerializable(INTENT_KEY_FEED_TYPE);
        switch (feedType) {
            case FEATURED:
                tvFeatured.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
                break;
            case LAB:
                tvLab.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
                break;
        }
    }

    @OnClick({R.id.ivClose, R.id.tvFeatured, R.id.tvLab, R.id.tvMyFavs, R.id.tvInfo})
    public void onClick(View view) {
        Intent intent = new Intent();

        switch (view.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.tvFeatured:
                intent.putExtra(INTENT_KEY_FEED_TYPE, FeedRVAdapter.FeedType.FEATURED);
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.tvLab:
                intent.putExtra(INTENT_KEY_FEED_TYPE, FeedRVAdapter.FeedType.LAB);
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.tvMyFavs:
                ProfileActivity.start(this);
                break;
            case R.id.tvInfo:
                startActivity(new Intent(this, AboutActivity.class));
                break;
        }
    }
}
