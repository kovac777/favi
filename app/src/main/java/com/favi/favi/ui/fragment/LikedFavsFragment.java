package com.favi.favi.ui.fragment;

import android.os.Bundle;

import com.favi.favi.adapter.recyclerview.FeedRVAdapter;

/**
 * Created by maxvinnik on 10.01.2018.
 */

public class LikedFavsFragment extends ProfileFeedFragment {

    public static final String TAG = LikedFavsFragment.class.getSimpleName();

    public static LikedFavsFragment newInstance() {

        Bundle args = new Bundle();

        LikedFavsFragment fragment = new LikedFavsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public FeedRVAdapter.FeedType getFeedType() {
        return FeedRVAdapter.FeedType.LIKED_FAVS;
    }
}
