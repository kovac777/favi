package com.favi.favi.ui.dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.favi.favi.di.ViewModelFactory;
import com.favi.favi.helper.AnalyticsHelper;
import com.favi.favi.ui.activity.HandleError;
import com.favi.favi.viewmodel.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * Created by User on 31.01.2018.
 * My SKYPE: maxvinnik
 */

public abstract class BaseDialog extends DialogFragment implements HandleError {

    @Inject
    AnalyticsHelper analyticsHelper;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    ViewModelFactory viewModelFactory;

    protected abstract List<BaseViewModel> initViewModels();

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        observeOnError(initViewModels());
    }

    private void observeOnError(List<BaseViewModel> baseViewModels) {
        if (baseViewModels == null || baseViewModels.size() == 0) {
            return;
        }

        for (BaseViewModel viewModel :
                baseViewModels) {
            httpException(viewModel);
            ioException(viewModel);
            socketTimeoutException(viewModel);
            unknownException(viewModel);
        }
    }

    @Override
    public void httpException(BaseViewModel viewModel) {
        viewModel.httpException().observe(this, throwable -> {
            Crashlytics.logException(throwable);
            Crashlytics.log("HTTP exception occurred!");
            Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void ioException(BaseViewModel viewModel) {
        viewModel.ioException().observe(this, throwable -> {
            Crashlytics.logException(throwable);
            Crashlytics.log("IO exception occurred!");
            Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void socketTimeoutException(BaseViewModel viewModel) {
        viewModel.socketTimeOutException().observe(this, throwable -> {
            Crashlytics.logException(throwable);
            Crashlytics.log("SocketTimeout exception occurred!");
            Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void unknownException(BaseViewModel viewModel) {
        viewModel.unknownException().observe(this, throwable -> {
            Crashlytics.logException(throwable);
            Crashlytics.log("Unknown exception occurred!");
            Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    @SuppressWarnings("ConstantConditions")
    public void showLoadIndicator() {
//        baseViewModel.showLoadIndicator().observe(this, isShow -> {
//            if (isShow)
//                progressBar.setVisibility(View.VISIBLE);
//            else
//                progressBar.setVisibility(View.GONE);
//        });
    }

}
