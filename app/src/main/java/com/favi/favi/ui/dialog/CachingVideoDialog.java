package com.favi.favi.ui.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.favi.favi.R;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.FeedViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kovac on 09.12.2017.
 * My SKYPE: maxvinnik
 */

public class CachingVideoDialog extends BaseDialog {

    public static final String TAG = CachingVideoDialog.class.getSimpleName();

    public static final String BUNDLE_KEY_FEED = "bundleKeyFeed";

    private FeedViewModel feedViewModel;

    public static CachingVideoDialog newInstance(Feed feed) {
        Bundle args = new Bundle();
        args.putParcelable(BUNDLE_KEY_FEED, feed);
        CachingVideoDialog fragment = new CachingVideoDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        feedViewModel.shareFav(getArguments().getParcelable(BUNDLE_KEY_FEED))
                .observe(getActivity(), isDownloadingVideo -> {
                    if (isDownloadingVideo) {
                        show(getFragmentManager(), TAG);
                    } else {
                        dismiss();
                    }
                });
    }

    @SuppressLint("InflateParams")
    @SuppressWarnings("ConstantConditions")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View root = getActivity().getLayoutInflater().inflate(R.layout.dialog_caching_video, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(root)
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    feedViewModel.cancelDownloadVideo();
                    dismiss();
                });

        return builder.create();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected List<BaseViewModel> initViewModels() {
        feedViewModel = ViewModelProviders.of(getActivity(), viewModelFactory)
                .get(FeedViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(feedViewModel);
        return viewModels;
    }
}
