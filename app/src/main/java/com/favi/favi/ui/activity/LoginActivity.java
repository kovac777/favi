package com.favi.favi.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;

import com.favi.favi.R;
import com.favi.favi.pojo.request.LoginProfile;
import com.favi.favi.util.Constants;
import com.favi.favi.util.TextUtil;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.ProfileViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends DialogActivity {

    public static final String INTENT_KEY_LOGIN_PROFILE = "intentKeyLoginProfile";

    @BindView(R.id.etlUserName)
    TextInputLayout etlUserName;
    @BindView(R.id.etUserName)
    TextInputEditText etUserName;
    @BindView(R.id.etlEmail)
    TextInputLayout etlEmail;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;

    private ProfileViewModel profileViewModel;

    public static void start(Context context, LoginProfile loginProfile) {
        Intent starter = new Intent(context, LoginActivity.class);
        starter.putExtra(INTENT_KEY_LOGIN_PROFILE, loginProfile);
        starter.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        initViews();
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        profileViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ProfileViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(profileViewModel);
        return viewModels;
    }

    private void initViews() {
        etlUserName.setHint(getString(R.string.hint_username));
        etlEmail.setHint(getString(R.string.email));
    }

    @OnClick({R.id.ivBack, R.id.tvDone, R.id.tvTermsOfService})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.tvDone:
                if (!isFieldsValid())
                    return;

                LoginProfile loginProfile = getIntent().getParcelableExtra(INTENT_KEY_LOGIN_PROFILE);
                loginProfile.setUserName(etUserName.getText().toString());
                loginProfile.setEmail(etEmail.getText().toString());
                profileViewModel.loginProfile(loginProfile).observe(this, isLogged -> {
                    //noinspection ConstantConditions
                    if (isLogged) {
                        sharedPreferences.edit().putBoolean(Constants.PREFS_IS_LOGGED_IN, true).apply();
                        finish();
                    }
                });
                break;
            case R.id.tvTermsOfService:

                break;
        }
    }

    private boolean isFieldsValid() {
        if (etUserName.getText().toString().length() < 4) {
            etlUserName.setError(getString(R.string._4_or_more_symbols));
            return false;
        } else if (!TextUtil.validateEmail(etEmail.getText().toString())) {
            etlEmail.setError(getString(R.string.error_email));
            return false;
        } else return true;
    }
}
