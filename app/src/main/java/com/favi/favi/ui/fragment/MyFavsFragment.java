package com.favi.favi.ui.fragment;

import android.os.Bundle;

import com.favi.favi.adapter.recyclerview.FeedRVAdapter;

/**
 * Created by maxvinnik on 09.01.2018.
 */

public class MyFavsFragment extends ProfileFeedFragment {

    public static final String TAG = MyFavsFragment.class.getSimpleName();

    public static MyFavsFragment newInstance() {

        Bundle args = new Bundle();

        MyFavsFragment fragment = new MyFavsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public FeedRVAdapter.FeedType getFeedType() {
        return FeedRVAdapter.FeedType.MY_FAVS;
    }
}
