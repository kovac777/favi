package com.favi.favi.ui.dialog;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.RatingBar;

import com.favi.favi.R;
import com.favi.favi.ui.activity.ProfileActivity;
import com.favi.favi.util.Constants;
import com.favi.favi.viewmodel.BaseViewModel;

import java.util.List;

/**
 * Created by maxvinnik on 03.02.2018.
 */

public class RateAppDialog extends BaseDialog {

    public static final String TAG = RateAppDialog.class.getSimpleName();

    private float rating;

    public static RateAppDialog newInstance() {
        Bundle args = new Bundle();
        RateAppDialog fragment = new RateAppDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        return null;
    }

    @SuppressWarnings("ConstantConditions")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View root = getActivity().getLayoutInflater().inflate(R.layout.dialog_rate_app, null);

        RatingBar ratingBar = root.findViewById(R.id.ratingBar);
        ratingBar.setOnRatingBarChangeListener((ratingBar1, rating, isFromUser) -> this.rating = rating);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(root)
                .setPositiveButton(R.string.rate, (dialogInterface, i) -> {
                    sharedPreferences.edit().putString(Constants.PREFS_LAST_RATE_MILLIS,
                            String.valueOf(System.currentTimeMillis())).apply();

                    ProfileActivity.start(getActivity());

                    if (rating > 2) {
                        Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        // To count with Play market backstack, After pressing back button,
                        // to taken back to our application, we need to add following flags to intent.
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id=" +
                                            getActivity().getPackageName())));
                        }
                    }

                    dismiss();
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    ProfileActivity.start(getActivity());
                    dismiss();
                });

        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_background);

        return dialog;
    }
}
