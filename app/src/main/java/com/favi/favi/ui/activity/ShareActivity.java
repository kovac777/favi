package com.favi.favi.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.favi.favi.BuildConfig;
import com.favi.favi.R;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.exoplayer.FavPlayer;
import com.favi.favi.exoplayer.PlayerListener;
import com.favi.favi.glide.GlideApp;
import com.favi.favi.helper.AnalyticsHelper;
import com.favi.favi.util.Constants;
import com.favi.favi.util.ContentUriProvider;
import com.favi.favi.util.FileUtil;
import com.favi.favi.util.toucharea.TouchAreaUtil;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.ShareViewModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;


public class ShareActivity extends DialogActivity implements PlayerListener {

    public static final String INTENT_FAV_UID_KEY = "intentFavUIDKey";
    public static final String INTENT_TEXT_KEY = "intentTextKey";

    @BindView(R.id.materialProgressBar)
    MaterialProgressBar materialProgressBar;
    @BindView(R.id.materialProgressBarLoadVideo)
    MaterialProgressBar materialProgressBarLoadVideo;

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivRegenerate)
    ImageView ivRegenerate;
    @BindView(R.id.tvRegenerate)
    TextView tvRegenerate;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.textureView)
    TextureView textureView;
    @BindView(R.id.ivPreview)
    ImageView ivPreview;
    @BindView(R.id.ivPlay)
    ImageView ivPlay;
    @BindView(R.id.ivSound)
    ImageView ivSound;

    @BindView(R.id.ivMoreSharedActivity)
    ImageView ivMoreSharedActivity;
    @BindView(R.id.tvDoneSharedActivity)
    TextView tvDoneSharedActivity;

    @BindView(R.id.regenerateOpacity)
    View regenerateOpacity;
    @BindView(R.id.viewOpacity)
    View viewOpacity;

    @BindView(R.id.groupAddInLab)
    Group groupAddInLab;
    @BindView(R.id.groupAddedInLab)
    Group groupAddedInLab;
    @BindView(R.id.groupRegenerate)
    Group groupRegenerate;
    @BindView(R.id.groupPosting)
    Group groupPosting;

    private ShareViewModel shareViewModel;

    private FavPlayer favPlayer;
    private Feed myFav;

    private boolean isInLabAdded;

    public static void start(Context context, long myFavUid, String text) {
        Intent starter = new Intent(context, ShareActivity.class);
        starter.putExtra(INTENT_FAV_UID_KEY, myFavUid);
        starter.putExtra(INTENT_TEXT_KEY, text);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        ButterKnife.bind(this);

        setupPlayer();
        setupObservables();
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        shareViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ShareViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(shareViewModel);
        return viewModels;
    }

    private void initViews() {
        TouchAreaUtil.extendTouchArea(32, ivBack, ivRegenerate, tvRegenerate);

        GlideApp.with(this)
                .load(myFav.getLocalPath())
                .into(ivPreview);
    }

    private void setupPlayer() {
        favPlayer = new FavPlayer(this);
        favPlayer.setPlayerListener(this);
        favPlayer.attachToView(textureView);
    }

    @SuppressWarnings("ConstantConditions")
    @OnClick({R.id.ivBack, R.id.ivRegenerate, R.id.tvRegenerate, R.id.tvCancel,
            R.id.textureView, R.id.ivSound, R.id.ivPlay, R.id.ivPostInLab,
            R.id.ivShareInstagram, R.id.ivShareFacebook, R.id.ivShareTwitter, R.id.ivMoreSharedActivity,
            R.id.tvDoneSharedActivity})
    public void doClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivRegenerate:
            case R.id.tvRegenerate:
                regenerateFav();
                break;
            case R.id.tvCancel:
                shareViewModel.cancelRegenerate();
                updateAfterRegeneratingUI();
                break;
            case R.id.textureView:
            case R.id.ivPlay:
                updatePlayerUI();
                break;
            case R.id.ivSound:
                if (favPlayer.getVolume() == 0)
                    favPlayer.setVolume(1);
                else
                    favPlayer.setVolume(0);
                break;
            case R.id.ivPostInLab:
                postInLab();
                break;
            case R.id.ivShareInstagram:
                shareFav(myFav.getLocalPath(), Constants.PACKAGE_INSTAGRAM);
                break;
            case R.id.ivShareFacebook:
                shareFav(myFav.getLocalPath(), Constants.PACKAGE_FACEBOOK);
                break;
            case R.id.ivShareTwitter:
                shareFav(myFav.getLocalPath(), Constants.PACKAGE_TWITTER);
                break;
            case R.id.ivMoreSharedActivity:
                shareFav(myFav.getLocalPath(), null);
                break;
            case R.id.tvDoneSharedActivity:
                sharedPreferences.edit().putString(Constants.PREFS_PREVIOUS_INPUT_TEXT, "").apply();
                isNeedRateDialog();
                break;
        }
    }

    private void updatePlayerUI() {
        if (favPlayer.isPlaying()) {
            favPlayer.stop();
            ivPreview.setVisibility(View.VISIBLE);
            ivPlay.setVisibility(View.VISIBLE);
            ivSound.setVisibility(View.GONE);
        } else {
            if (myFav.getLocalPath() != null) {
                favPlayer.setDataSource(Uri.parse(myFav.getLocalPath()));
                favPlayer.play();
                ivPreview.setVisibility(View.GONE);
                ivPlay.setVisibility(View.GONE);
                ivSound.setVisibility(View.VISIBLE);
            } else finish();
        }
    }

    @SuppressWarnings("ConstantConditions")
    private void regenerateFav() {
        updateBeforeRegeneratingUI();

        if (FileUtil.isExternalStorageAvailable()) {
            shareViewModel.regenerateFav(
                    getIntent().getStringExtra(INTENT_TEXT_KEY), myFav.getTagId(), myFav)
                    .observe(this, feed -> {
                        this.myFav = feed;

                        GlideApp.with(ivPreview)
                                .load(feed.getPath())
                                .into(ivPreview);

                        updateAfterRegeneratingUI();
                    });
        } else {
            updateAfterRegeneratingUI();
            Toast.makeText(this, "External storage unavailable!", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateAfterRegeneratingUI() {
        materialProgressBar.setVisibility(View.GONE);
        tvCancel.setVisibility(View.GONE);
        tvTitle.setVisibility(View.GONE);
        viewOpacity.setVisibility(View.GONE);
        groupRegenerate.setVisibility(View.VISIBLE);
    }

    private void updateBeforeRegeneratingUI() {
        groupRegenerate.setVisibility(View.GONE);
        materialProgressBar.setVisibility(View.VISIBLE);
        tvCancel.setVisibility(View.VISIBLE);
        tvTitle.setVisibility(View.VISIBLE);
        viewOpacity.setVisibility(View.VISIBLE);
    }

    @SuppressWarnings("ConstantConditions")
    private void postInLab() {
        if (!isLoggedIn())
            return;

        updateBeforePostUI();

        shareViewModel.postFavInLab(myFav)
                .observe(this, feed -> {
                    feed.setUid(myFav.uid);
                    feed.setLocalPath(myFav.getLocalPath());
                    shareViewModel.updateFavInDB(feed);

                    isInLabAdded = true;

                    updateAfterPostUI();
                });
    }

    private void updateBeforePostUI() {
        groupAddInLab.setVisibility(View.GONE);
        groupPosting.setVisibility(View.VISIBLE);
        regenerateOpacity.setVisibility(View.VISIBLE);
    }

    private void updateAfterPostUI() {
        groupPosting.setVisibility(View.GONE);
        groupAddedInLab.setVisibility(View.VISIBLE);
        groupRegenerate.setVisibility(View.GONE);
        regenerateOpacity.setVisibility(View.GONE);
    }

    @SuppressWarnings("ConstantConditions")
    protected void setupObservables() {
        shareViewModel.getMyFavFromDB(getIntent().getLongExtra(INTENT_FAV_UID_KEY, -1))
                .observe(this, feed -> {
                    this.myFav = feed;
                    initViews();
                });
        shareViewModel.resetUI().observe(this, isResetUI -> {
            if (isResetUI) resetUI();
        });
    }

    private void resetUI() {
        if (isInLabAdded) {
            materialProgressBar.setVisibility(View.GONE);
            materialProgressBarLoadVideo.setVisibility(View.GONE);
            regenerateOpacity.setVisibility(View.GONE);
            viewOpacity.setVisibility(View.GONE);
            tvCancel.setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            groupPosting.setVisibility(View.GONE);
            groupRegenerate.setVisibility(View.GONE);
            groupAddInLab.setVisibility(View.GONE);
            groupAddedInLab.setVisibility(View.VISIBLE);
        } else {
            materialProgressBar.setVisibility(View.GONE);
            materialProgressBarLoadVideo.setVisibility(View.GONE);
            regenerateOpacity.setVisibility(View.GONE);
            viewOpacity.setVisibility(View.GONE);
            tvCancel.setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            groupPosting.setVisibility(View.GONE);
            groupAddedInLab.setVisibility(View.GONE);
            groupRegenerate.setVisibility(View.VISIBLE);
            groupAddInLab.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (favPlayer.isPlaying()) {
            textureView.performClick();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        favPlayer.release();
    }

    private boolean isAppInstalled(String uri) {
        try {
            getPackageManager().getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void shareFav(String videoPath, @Nullable String packageTo) {
        if (!BuildConfig.DEBUG) {
            analyticsHelper.sendShareAnalytics(AnalyticsHelper.SHARE_SCREEN,
                    packageTo == null ? AnalyticsHelper.SHARE_WITH_NATIVE : packageTo,
                    myFav.getTagId());
        }

        File video = new File(videoPath);
        Uri uri = ContentUriProvider.getUriForFile(this, Constants.FILE_PROVIDER_AUTHORITES, video);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("video/*");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(Intent.EXTRA_STREAM, uri);

        if (packageTo != null && !isAppInstalled(packageTo)) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageTo)));
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageTo)));
            }
        } else if (packageTo != null) {
            intent.setPackage(packageTo);
            startActivity(intent);
        } else {
            startActivity(Intent.createChooser(intent, getString(R.string.share_fav_to)));
        }
    }

    @Override
    public void videoFinished() {
        ivPlay.performClick();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}