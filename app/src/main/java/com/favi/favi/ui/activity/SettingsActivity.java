package com.favi.favi.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.ImageView;

import com.favi.favi.R;
import com.favi.favi.db.entity.LocalProfile;
import com.favi.favi.glide.GlideApp;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.ProfileViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends BaseActivity {

    public static final String INTENT_KEY_LOCAL_PROFILE = "intentKeyLocalProfile";

    @BindView(R.id.ivAvatar)
    ImageView ivAvatar;
    @BindView(R.id.etlUserName)
    TextInputLayout etlUserName;
    @BindView(R.id.etUserName)
    TextInputEditText etUserName;
    private ProfileViewModel profileViewModel;
    private LocalProfile localProfile;

    public static void start(Context context, LocalProfile localProfile) {
        Intent starter = new Intent(context, SettingsActivity.class);
        starter.putExtra(INTENT_KEY_LOCAL_PROFILE, localProfile);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);

        initViews();
        setupObservables();
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        profileViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ProfileViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(profileViewModel);
        return viewModels;
    }

    @SuppressWarnings("ConstantConditions")
    protected void setupObservables() {
        profileViewModel.isEditProfileSuccessful.observe(this, isSuccessful -> {
            if (isSuccessful) {
                finish();
            }
        });
    }

    private void initViews() {
        localProfile = getIntent().getParcelableExtra(INTENT_KEY_LOCAL_PROFILE);

        GlideApp.with(this)
                .load(localProfile.getImage())
                .into(ivAvatar);

        etUserName.setText(localProfile.getUserName());
    }

    @OnClick({R.id.tvCancel, R.id.ivDone})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvCancel:
                finish();
                break;
            case R.id.ivDone:
                if (etUserName.getText().toString().length() < 4)
                    etlUserName.setError(getString(R.string._4_or_more_symbols));
                else
                    localProfile.setUserName(etUserName.getText().toString());
                profileViewModel.editProfile(localProfile);
                break;
        }
    }
}
