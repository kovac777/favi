package com.favi.favi.ui.view;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.favi.favi.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by kovac on 19.11.2017.
 * My SKYPE: maxvinnik
 */

public class InternetConnectionErrorView extends ConstraintLayout {

    @BindView(R.id.ivErrorInternetConnection)
    ImageView ivErrorInternetConnection;
    @BindView(R.id.tvErrorInternetConnection)
    TextView tvErrorInternetConnection;
    private Unbinder unbinder;

    public InternetConnectionErrorView(Context context) {
        super(context);
        init(context);
    }

    public InternetConnectionErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public InternetConnectionErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setBackgroundColor(ContextCompat.getColor(context, R.color.colorErrorConnectionBackground));

        LayoutInflater layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        assert layoutInflater != null;
        layoutInflater.inflate(R.layout.internet_connection_error, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unbinder.unbind();
    }

    public void shake() {
        YoYo.with(Techniques.Shake).duration(1200).playOn(tvErrorInternetConnection);
        YoYo.with(Techniques.Shake).duration(1200).playOn(ivErrorInternetConnection);
    }
}
