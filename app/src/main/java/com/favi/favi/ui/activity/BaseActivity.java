package com.favi.favi.ui.activity;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.favi.favi.R;
import com.favi.favi.di.ViewModelFactory;
import com.favi.favi.helper.AnalyticsHelper;
import com.favi.favi.livedata.NetworkLiveData;
import com.favi.favi.ui.view.InternetConnectionErrorView;
import com.favi.favi.viewmodel.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import retrofit2.HttpException;

@SuppressLint("Registered")
public abstract class BaseActivity extends AppCompatActivity implements HandleError {

    @Inject
    AnalyticsHelper analyticsHelper;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    NetworkLiveData networkLiveData;
    @Inject
    ViewModelFactory viewModelFactory;

    private View parent;
    private FrameLayout child;

    private InternetConnectionErrorView errorInternetConnection;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("InflateParams")
    @SuppressWarnings("ConstantConditions")
    @Override
    public void setContentView(int layoutResID) {
        LayoutInflater inflater = getLayoutInflater();

        parent = inflater.inflate(R.layout.activity_base, null);
        errorInternetConnection = parent.findViewById(R.id.errorLayout);
        child = parent.findViewById(R.id.child);
        progressBar = parent.findViewById(R.id.progressBar);

        inflater.inflate(layoutResID, child, true);

        super.setContentView(parent);

        networkLiveData.observe(this, isInternetAvailable -> {
            if (isInternetAvailable) {
                errorInternetConnection.setVisibility(View.GONE);
            } else {
                errorInternetConnection.setVisibility(View.VISIBLE);
            }
        });

        observeOnError(initViewModels());
    }

    private void observeOnError(List<BaseViewModel> baseViewModels) {
        if (baseViewModels == null || baseViewModels.size() == 0) {
            return;
        }

        for (BaseViewModel viewModel :
                baseViewModels) {
            httpException(viewModel);
            ioException(viewModel);
            socketTimeoutException(viewModel);
            unknownException(viewModel);
        }
    }

    protected abstract List<BaseViewModel> initViewModels();

    @Override
    public void httpException(BaseViewModel viewModel) {
        viewModel.httpException().observe(this, throwable -> {
            Crashlytics.logException(throwable);
            Crashlytics.log("HTTP exception occurred!");
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void ioException(BaseViewModel viewModel) {
        viewModel.ioException().observe(this, throwable -> {
            Crashlytics.logException(throwable);
            Crashlytics.log("IO exception occurred!");
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void socketTimeoutException(BaseViewModel viewModel) {
        viewModel.socketTimeOutException().observe(this, throwable -> {
            Crashlytics.logException(throwable);
            Crashlytics.log("SocketTimeout exception occurred!");
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void unknownException(BaseViewModel viewModel) {
        viewModel.unknownException().observe(this, throwable -> {
            Crashlytics.logException(throwable);
            Crashlytics.log("Unknown exception occurred!");
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    @SuppressWarnings("ConstantConditions")
    public void showLoadIndicator() {
//        baseViewModel.showLoadIndicator().observe(this, isShow -> {
//            if (isShow)
//                progressBar.setVisibility(View.VISIBLE);
//            else
//                progressBar.setVisibility(View.GONE);
//        });
    }
}
