package com.favi.favi.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.favi.favi.R;
import com.favi.favi.adapter.recyclerview.CharacterRVAdapter;
import com.favi.favi.pojo.response.Character;
import com.favi.favi.viewmodel.BaseViewModel;
import com.favi.favi.viewmodel.SelectCharacterViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectCharacterActivity extends BaseActivity implements
        CharacterRVAdapter.ItemClickListener {

    @BindView(R.id.rvCharacters)
    RecyclerView rvCharacters;

    private SelectCharacterViewModel selectCharacterViewModel;
    private CharacterRVAdapter characterRVAdapter;

    public static void start(Context context) {
        Intent starter = new Intent(context, SelectCharacterActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_character);

        ButterKnife.bind(this);

        initViews();
        setupObservables();
    }

    @Override
    protected List<BaseViewModel> initViewModels() {
        selectCharacterViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SelectCharacterViewModel.class);

        List<BaseViewModel> viewModels = new ArrayList<>();
        viewModels.add(selectCharacterViewModel);
        return viewModels;
    }

    private void initViews() {
        characterRVAdapter = new CharacterRVAdapter(this, this);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (characterRVAdapter.getItemCount() % 2 != 0 &&
                        position == characterRVAdapter.getItemCount() - 1) {
                    return 2;
                } else
                    return 1;
            }
        });

        rvCharacters.setLayoutManager(layoutManager);
        rvCharacters.setAdapter(characterRVAdapter);
    }

    @SuppressWarnings("ConstantConditions")
    protected void setupObservables() {
        selectCharacterViewModel.getCharacters().observe(this, characters -> {
            List<CharacterRVAdapter.ItemViewType> list = new ArrayList<>();
            list.addAll(characters);
            characterRVAdapter.addAll(list);
        });
    }

    @Override
    public void onItemClicked(ArrayList<Character> characters, Character selectedCharacter, int position) {
        NewFavActivity.start(this, characters, selectedCharacter, position);
    }

    @OnClick({R.id.ivClose, R.id.ivSuggest})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.ivSuggest:
                SuggestCharacterActivity.start(this);
                break;
        }
    }
}
