package com.favi.favi.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.favi.favi.db.dao.ExternalAccessTokenDAO;
import com.favi.favi.db.dao.FeedDAO;
import com.favi.favi.db.dao.ProfileDAO;
import com.favi.favi.db.entity.ExternalAccessToken;
import com.favi.favi.db.entity.Feed;
import com.favi.favi.db.entity.LocalProfile;

/**
 * Created by kovac on 20.11.2017.
 * My SKYPE: maxvinnik
 */

@Database(entities = {
        Feed.class,
        LocalProfile.class,
        ExternalAccessToken.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {

    public abstract FeedDAO feedDAO();

    public abstract ProfileDAO profileDAO();

    public abstract ExternalAccessTokenDAO externalAccessTokenDAO();

}
