package com.favi.favi.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.favi.favi.db.entity.LocalProfile;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by maxvinnik on 13.01.2018.
 */

@Dao
public interface ProfileDAO {

    @Query("SELECT * FROM LocalProfile")
    Flowable<List<LocalProfile>> getFlowableProfile();

    @Query("SELECT * FROM LocalProfile")
    Single<List<LocalProfile>> getSingleProfile();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveProfile(LocalProfile localProfile);

    @Delete
    void deleteProfile(LocalProfile localProfile);
}
