package com.favi.favi.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by maxvinnik on 14.01.2018.
 */

@Entity
public class ExternalAccessToken {

    @PrimaryKey
    public long uid;

    private String provider;
    private String token;

    public ExternalAccessToken() {
    }

    @Ignore
    public ExternalAccessToken(String provider, String token) {
        this.provider = provider;
        this.token = token;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
