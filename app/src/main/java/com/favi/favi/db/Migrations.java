package com.favi.favi.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.migration.Migration;
import android.support.annotation.NonNull;

/**
 * Created by maxvinnik on 13.01.2018.
 */

public class Migrations {

    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.beginTransaction();
            try {
                database.execSQL("CREATE TEMPORARY TABLE Feed_backup(" +
                        "uid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                        "id INTEGER NOT NULL, " +
                        "path TEXT, " +
                        "name TEXT, " +
                        "tagId INTEGER NOT NULL, " +
                        "likes INTEGER NOT NULL, " +
                        "date TEXT, " +
                        "inLab INTEGER NOT NULL DEFAULT 0, " +
                        "featured INTEGER NOT NULL DEFAULT 0, " +
                        "liked INTEGER NOT NULL DEFAULT 0, " +
                        "thumbnail TEXT, " +
                        "localPath TEXT, " +
                        "isPosting INTEGER NOT NULL DEFAULT 0, " +
                        "adapterPosition INTEGER NOT NULL);");
                database.execSQL("INSERT INTO Feed_backup SELECT uid,id,path,name,tagId,likes," +
                        "date,inLab,featured,liked,thumbnail,localPath,isPosting,adapterPosition FROM Feed");
                database.execSQL("DROP TABLE Feed;");
                database.execSQL("CREATE TABLE Feed(" +
                        "uid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                        "id INTEGER NOT NULL, " +
                        "path TEXT, " +
                        "name TEXT, " +
                        "tagId INTEGER NOT NULL, " +
                        "likes INTEGER NOT NULL, " +
                        "date TEXT, " +
                        "inLab INTEGER NOT NULL DEFAULT 0, " +
                        "featured INTEGER NOT NULL DEFAULT 0, " +
                        "liked INTEGER NOT NULL DEFAULT 0, " +
                        "thumbnail TEXT, " +
                        "localPath TEXT, " +
                        "isPosting INTEGER NOT NULL DEFAULT 0, " +
                        "adapterPosition INTEGER NOT NULL);");
                database.execSQL("INSERT INTO Feed SELECT uid,id,path,name,tagId,likes,date,inLab," +
                        "featured,liked,thumbnail,localPath,isPosting,adapterPosition FROM Feed_backup;");
                database.execSQL("DROP TABLE Feed_backup;");
                database.execSQL("ALTER TABLE Feed ADD COLUMN reports INTEGER NOT NULL DEFAULT 0;");
                database.execSQL("ALTER TABLE Feed ADD COLUMN userId INTEGER NOT NULL DEFAULT 0;");
                database.execSQL("ALTER TABLE Feed ADD COLUMN userName TEXT;");
                database.execSQL("ALTER TABLE Feed ADD COLUMN image TEXT;");
                database.execSQL("ALTER TABLE Feed ADD COLUMN userDate TEXT;");
                database.execSQL("ALTER TABLE Feed ADD COLUMN comments INTEGER NOT NULL DEFAULT 0;");

                database.setTransactionSuccessful();
            } finally {
                database.endTransaction();
            }

            database.execSQL("CREATE TABLE LocalProfile " +
                    "(uid INTEGER PRIMARY KEY NOT NULL, " +
                    "userId INTEGER NOT NULL, " +
                    "userDate TEXT, " +
                    "userName TEXT, " +
                    "image TEXT, " +
                    "hasLocalAccount INTEGER NOT NULL DEFAULT 0, " +
                    "access_token TEXT, " +
                    "token_type TEXT, " +
                    "expires_in TEXT, " +
                    "issued TEXT, " +
                    "expires TEXT)");

            database.execSQL("CREATE TABLE ExternalAccessToken " +
                    "(uid INTEGER PRIMARY KEY NOT NULL, " +
                    "provider TEXT, " +
                    "token TEXT)");
        }
    };
}
