package com.favi.favi.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maxvinnik on 13.01.2018.
 */

@Entity
public class LocalProfile implements Parcelable {

    public static final Creator<LocalProfile> CREATOR = new Creator<LocalProfile>() {
        @Override
        public LocalProfile createFromParcel(Parcel in) {
            return new LocalProfile(in);
        }

        @Override
        public LocalProfile[] newArray(int size) {
            return new LocalProfile[size];
        }
    };
    /**
     * userId : 33
     * userDate : 2018-01-10T02:47:33.283
     * userName : Robot Favi
     * image : http://favi.web.local/Images/user.png
     * hasLocalAccount : true
     * access_token : cnf...
     * token_type : Bearer
     * expires_in : 86400
     * .issued : 1/12/2018 2:44:01 PM +00:00
     * .expires : 1/13/2018 2:44:01 PM +00:00
     */

    @PrimaryKey
    public long uid = 1;
    private int userId;
    private String userDate;
    private String userName;
    private String image;
    private boolean hasLocalAccount;
    private String access_token;
    private String token_type;
    private String expires_in;
    @SerializedName(".issued")
    private String issued;
    @SerializedName(".expires")
    private String expires;

    public LocalProfile() {
    }

    @Ignore
    protected LocalProfile(Parcel in) {
        uid = in.readLong();
        userId = in.readInt();
        userName = in.readString();
        image = in.readString();
        hasLocalAccount = in.readByte() != 0;
        access_token = in.readString();
        token_type = in.readString();
        expires_in = in.readString();
        issued = in.readString();
        expires = in.readString();
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isHasLocalAccount() {
        return hasLocalAccount;
    }

    public void setHasLocalAccount(boolean hasLocalAccount) {
        this.hasLocalAccount = hasLocalAccount;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getUserDate() {
        return userDate;
    }

    public void setUserDate(String userDate) {
        this.userDate = userDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(uid);
        parcel.writeInt(userId);
        parcel.writeString(userName);
        parcel.writeString(image);
        parcel.writeByte((byte) (hasLocalAccount ? 1 : 0));
        parcel.writeString(access_token);
        parcel.writeString(token_type);
        parcel.writeString(expires_in);
        parcel.writeString(issued);
        parcel.writeString(expires);
    }
}
