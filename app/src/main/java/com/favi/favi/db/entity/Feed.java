package com.favi.favi.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.favi.favi.adapter.recyclerview.BaseRVAdapter;

/**
 * Created by kovac on 03.11.2017.
 * My SKYPE: maxvinnik
 */

@Entity
public class Feed implements BaseRVAdapter.ItemViewType, Parcelable {

    /**
     * id : 7312
     * path : Videos\output\c4\44\c8\08\93\45\17.mp4
     * name : good good
     * tagId : 1
     * likes : 2
     * date : 2018-01-10T21:40:33.41
     * inLab : true
     * featured : true
     * liked : false
     * thumbnail : /Videos/output\c4\44\c8\08\93\45\17.jpg
     * reports : 0
     * userId : 2
     * userName : Robot Favi
     * image : http://x.api.faviapp.com/images/users/robot.jpg
     * userDate : 2018-01-12T10:35:15.3433333
     * comments : 0
     */

    @PrimaryKey(autoGenerate = true)
    public long uid;

    private int id;
    private String path;
    private String name;
    private int tagId;
    private int likes;
    private String date;
    private boolean inLab;
    private boolean featured;
    private boolean liked;
    private String thumbnail;
    private int reports;
    private int userId;
    private String userName;
    private String image;
    private String userDate;
    private int comments;

    private String localPath;
    private boolean isPosting;
    private int adapterPosition;

    public Feed() {

    }

    @Ignore
    protected Feed(Parcel in) {
        uid = in.readLong();
        id = in.readInt();
        path = in.readString();
        name = in.readString();
        tagId = in.readInt();
        likes = in.readInt();
        date = in.readString();
        inLab = in.readByte() != 0;
        featured = in.readByte() != 0;
        liked = in.readByte() != 0;
        thumbnail = in.readString();
        reports = in.readInt();
        userId = in.readInt();
        userName = in.readString();
        image = in.readString();
        userDate = in.readString();
        comments = in.readInt();
        localPath = in.readString();
        isPosting = in.readByte() != 0;
        adapterPosition = in.readInt();
    }

    public static final Creator<Feed> CREATOR = new Creator<Feed>() {
        @Override
        public Feed createFromParcel(Parcel in) {
            return new Feed(in);
        }

        @Override
        public Feed[] newArray(int size) {
            return new Feed[size];
        }
    };

    @Override
    public BaseRVAdapter.ViewType getType() {
        return BaseRVAdapter.ViewType.FEED;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isInLab() {
        return inLab;
    }

    public void setInLab(boolean inLab) {
        this.inLab = inLab;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getReports() {
        return reports;
    }

    public void setReports(int reports) {
        this.reports = reports;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUserDate() {
        return userDate;
    }

    public void setUserDate(String userDate) {
        this.userDate = userDate;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public boolean isPosting() {
        return isPosting;
    }

    public void setPosting(boolean posting) {
        isPosting = posting;
    }

    public int getAdapterPosition() {
        return adapterPosition;
    }

    public void setAdapterPosition(int adapterPosition) {
        this.adapterPosition = adapterPosition;
    }

    public void setLocalProfileData(LocalProfile localProfile) {
        this.userId = localProfile.getUserId();
        this.userName = localProfile.getUserName();
        this.image = localProfile.getImage();
        this.userDate = localProfile.getUserDate();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(uid);
        parcel.writeInt(id);
        parcel.writeString(path);
        parcel.writeString(name);
        parcel.writeInt(tagId);
        parcel.writeInt(likes);
        parcel.writeString(date);
        parcel.writeByte((byte) (inLab ? 1 : 0));
        parcel.writeByte((byte) (featured ? 1 : 0));
        parcel.writeByte((byte) (liked ? 1 : 0));
        parcel.writeString(thumbnail);
        parcel.writeInt(reports);
        parcel.writeInt(userId);
        parcel.writeString(userName);
        parcel.writeString(image);
        parcel.writeString(userDate);
        parcel.writeInt(comments);
        parcel.writeString(localPath);
        parcel.writeByte((byte) (isPosting ? 1 : 0));
        parcel.writeInt(adapterPosition);
    }
}
