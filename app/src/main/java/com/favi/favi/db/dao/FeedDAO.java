package com.favi.favi.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.favi.favi.db.entity.Feed;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by kovac on 20.11.2017.
 * My SKYPE: maxvinnik
 */

@Dao
public interface FeedDAO {

    @Query("SELECT * FROM Feed ORDER BY uid DESC LIMIT 20 OFFSET :offset")
    Single<List<Feed>> getMyFavs(int offset);

    @Query("SELECT * FROM Feed")
    Single<List<Feed>> getAllMyFavs();

    @Query("SELECT * FROM Feed WHERE uid = :uid")
    Single<List<Feed>> getFavByUID(long uid);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addFav(Feed feed);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateFav(Feed feed);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateFavs(List<Feed> feeds);

    @Delete
    int deleteFav(Feed feed);
}
