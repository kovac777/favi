package com.favi.favi.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.favi.favi.db.entity.ExternalAccessToken;

import io.reactivex.Single;

/**
 * Created by maxvinnik on 15.01.2018.
 */

@Dao
public interface ExternalAccessTokenDAO {

    @Query("SELECT * FROM ExternalAccessToken LIMIT 1")
    Single<ExternalAccessToken> getExternalAccessToken();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveToken(ExternalAccessToken externalAccessToken);
}
