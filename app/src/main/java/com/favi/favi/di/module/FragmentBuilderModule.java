package com.favi.favi.di.module;

import com.favi.favi.ui.dialog.CachingVideoDialog;
import com.favi.favi.ui.dialog.RateAppDialog;
import com.favi.favi.ui.dialog.ReportDialog;
import com.favi.favi.ui.dialog.SignInDialog;
import com.favi.favi.ui.fragment.LikedFavsFragment;
import com.favi.favi.ui.fragment.MyFavsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by maxvinnik on 09.01.2018.
 */

@Module
public abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract MyFavsFragment bindMyFavsFragment();

    @ContributesAndroidInjector
    abstract LikedFavsFragment bindLikedFavsFragment();

    @ContributesAndroidInjector
    abstract SignInDialog bindSignInDialog();

    @ContributesAndroidInjector
    abstract ReportDialog bindReportDialog();

    @ContributesAndroidInjector
    abstract CachingVideoDialog bindCachingVideoDialog();

    @ContributesAndroidInjector
    abstract RateAppDialog bindRateAppDialog();
}
