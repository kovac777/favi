package com.favi.favi.di.module;

import com.favi.favi.fcm.MyFirebaseInstanceIDService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by User on 24.01.2018.
 * My SKYPE: maxvinnik
 */

@Module
public abstract class ServiceBuilderModule {

    @ContributesAndroidInjector
    abstract MyFirebaseInstanceIDService bindMyFirebaseInstanceIDService();
}
