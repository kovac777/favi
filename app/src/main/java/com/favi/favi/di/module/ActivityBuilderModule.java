package com.favi.favi.di.module;

import com.favi.favi.ui.activity.CommentsActivity;
import com.favi.favi.ui.activity.FeedActivity;
import com.favi.favi.ui.activity.LoginActivity;
import com.favi.favi.ui.activity.NewFavActivity;
import com.favi.favi.ui.activity.OtherUserProfileActivity;
import com.favi.favi.ui.activity.ProfileActivity;
import com.favi.favi.ui.activity.SelectCharacterActivity;
import com.favi.favi.ui.activity.SettingsActivity;
import com.favi.favi.ui.activity.ShareActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by kovac on 04.11.2017.
 * My SKYPE: maxvinnik
 */

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector
    abstract FeedActivity bindFeedActivity();

    @ContributesAndroidInjector
    abstract NewFavActivity bindNewFavActivity();

    @ContributesAndroidInjector
    abstract ShareActivity bindShareActivity();

    @ContributesAndroidInjector
    abstract SelectCharacterActivity bindSelectCharacterActivity();

    @ContributesAndroidInjector
    abstract ProfileActivity bindProfileActivity();

    @ContributesAndroidInjector
    abstract LoginActivity bindLoginActivity();

    @ContributesAndroidInjector
    abstract SettingsActivity bindSettingsActivity();

    @ContributesAndroidInjector
    abstract CommentsActivity bindCommentsActivity();

    @ContributesAndroidInjector
    abstract OtherUserProfileActivity bindOtherProfileActivity();
}
