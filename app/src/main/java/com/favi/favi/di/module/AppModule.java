package com.favi.favi.di.module;

import android.arch.persistence.room.Room;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.appsflyer.AppsFlyerLib;
import com.favi.favi.api.ServiceGenerator;
import com.favi.favi.app.FaViApp;
import com.favi.favi.db.AppDatabase;
import com.favi.favi.db.Migrations;
import com.favi.favi.helper.AnalyticsHelper;
import com.favi.favi.livedata.NetworkLiveData;
import com.favi.favi.util.ResourceProvider;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.google.firebase.analytics.FirebaseAnalytics;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Max on 26.03.2017.
 * My SKYPE: maxvinnik
 */

@Module(includes = {ViewModelModule.class})
public class AppModule {

    @Singleton
    @Provides
    ServiceGenerator provideServiceGenerator() {
        return new ServiceGenerator();
    }

    @Singleton
    @Provides
    SharedPreferences provideSharedPreferences(FaViApp context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Singleton
    @Provides
    FFmpeg provideFFmpeg(FaViApp context) {
        return FFmpeg.getInstance(context);
    }

    @Singleton
    @Provides
    AppDatabase provideAppDatabase(FaViApp context) {
        return Room.databaseBuilder(context, AppDatabase.class, "favi-db")
                .addMigrations(Migrations.MIGRATION_1_2)
                .build();
    }

    @Singleton
    @Provides
    ResourceProvider provideResourceProvider(FaViApp context) {
        return new ResourceProvider(context);
    }

    @Singleton
    @Provides
    NetworkLiveData provideNetworkLiveData(FaViApp context) {
        return new NetworkLiveData(context);
    }

    @Singleton
    @Provides
    FirebaseAnalytics provideFirebaseAnalytics(FaViApp context) {
        return FirebaseAnalytics.getInstance(context);
    }

    @Singleton
    @Provides
    AppsFlyerLib provideAppsFlyerLib() {
        return AppsFlyerLib.getInstance();
    }

    @Singleton
    @Provides
    AnalyticsHelper provideAnalyticsHelper(FaViApp context, FirebaseAnalytics firebaseAnalytics,
                                           AppsFlyerLib appsFlyerLib) {
        return new AnalyticsHelper(context, firebaseAnalytics, appsFlyerLib);
    }
}
