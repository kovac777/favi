package com.favi.favi.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.favi.favi.di.ViewModelFactory;
import com.favi.favi.di.ViewModelKey;
import com.favi.favi.viewmodel.CommentsViewModel;
import com.favi.favi.viewmodel.FeedViewModel;
import com.favi.favi.viewmodel.LoginViewModel;
import com.favi.favi.viewmodel.NewFavViewModel;
import com.favi.favi.viewmodel.ProfileViewModel;
import com.favi.favi.viewmodel.SelectCharacterViewModel;
import com.favi.favi.viewmodel.ShareViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by kovac on 06.11.2017.
 * My SKYPE: maxvinnik
 */

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FeedViewModel.class)
    abstract ViewModel bindFeedViewModel(FeedViewModel feedViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NewFavViewModel.class)
    abstract ViewModel bindNewFavViewModel(NewFavViewModel newFavViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ShareViewModel.class)
    abstract ViewModel bindShareViewModel(ShareViewModel shareViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SelectCharacterViewModel.class)
    abstract ViewModel bindSelectCharacterViewModel(SelectCharacterViewModel selectCharacterViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel.class)
    abstract ViewModel bindProfileViewModel(ProfileViewModel profileViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindLoginViewModel(LoginViewModel loginViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CommentsViewModel.class)
    abstract ViewModel bindCommentsViewModel(CommentsViewModel commentsViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory viewModelFactory);

}
