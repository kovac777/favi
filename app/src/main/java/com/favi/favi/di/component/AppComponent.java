package com.favi.favi.di.component;

import com.favi.favi.app.FaViApp;
import com.favi.favi.di.module.ActivityBuilderModule;
import com.favi.favi.di.module.AppModule;
import com.favi.favi.di.module.FragmentBuilderModule;
import com.favi.favi.di.module.ServiceBuilderModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Max on 26.03.2017.
 * My SKYPE: maxvinnik
 */

@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilderModule.class,
        FragmentBuilderModule.class,
        ServiceBuilderModule.class})
@Singleton
public interface AppComponent extends AndroidInjector<FaViApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<FaViApp> {

    }

}
