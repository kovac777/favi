package com.favi.favi.glide;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by maxvinnik on 08.01.2018.
 */

@GlideModule
public class GlideAppModule extends AppGlideModule {
}
