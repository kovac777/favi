package com.favi.favi.helper;

import android.os.Bundle;

import com.appsflyer.AppsFlyerLib;
import com.favi.favi.app.FaViApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.yandex.metrica.YandexMetrica;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by kovac on 13.12.2017.
 * My SKYPE: maxvinnik
 */

public class AnalyticsHelper {

    //EVENTS
    public static final String SHARE_EVENT = "share_event";
    public static final String CREATE_FAV_EVENT = "create_fav_event";

    //KEYS
    public static final String FROM_SCREEN_KEY = "from_screen";
    public static final String CHARACTER_TAG_KEY = "character_tag";
    public static final String SHARE_WITH_KEY = "share_with";

    //SCREENS
    public static final String FEED_SCREEN = "feed_screen";
    public static final String SHARE_SCREEN = "share_screen";
    public static final String MY_FAVS_SCREEN = "my_favs_screen";
    public static final String MY_FAVORITES_FAVS_SCREEN = "my_favorites_favs_screen";
    public static final String OTHER_USERS_FAVS_SCREEN = "other_users_favs_screen";

    //DEFAULT VALUES
    public static final String SHARE_WITH_NATIVE = "native";

    private FaViApp context;
    private FirebaseAnalytics firebaseAnalytics;
    private AppsFlyerLib appsFlyerLib;

    @Inject
    public AnalyticsHelper(FaViApp context, FirebaseAnalytics firebaseAnalytics,
                           AppsFlyerLib appsFlyerLib) {
        this.context = context;
        this.firebaseAnalytics = firebaseAnalytics;
        this.appsFlyerLib = appsFlyerLib;
    }

    public void sendShareAnalytics(String shareScreen, String shareTo, int tagId) {
        Bundle bundle = new Bundle();
        bundle.putString(FROM_SCREEN_KEY, shareScreen);
        bundle.putString(SHARE_WITH_KEY, shareTo);
        bundle.putString(CHARACTER_TAG_KEY, String.valueOf(tagId));
        firebaseAnalytics.logEvent(SHARE_EVENT, bundle);

        Map<String, Object> eventValue = new HashMap<>();
        eventValue.put(FROM_SCREEN_KEY, shareScreen);
        eventValue.put(SHARE_WITH_KEY, shareTo);
        eventValue.put(CHARACTER_TAG_KEY, tagId);
        appsFlyerLib.trackEvent(context, SHARE_EVENT, eventValue);

        YandexMetrica.reportEvent(SHARE_EVENT, eventValue);
    }

    public void sendCreateFavAnalytics(int tagId) {
        Bundle bundle = new Bundle();
        bundle.putString(CHARACTER_TAG_KEY, String.valueOf(tagId));
        firebaseAnalytics.logEvent(CREATE_FAV_EVENT, bundle);

        Map<String, Object> eventValue = new HashMap<>();
        eventValue.put(CHARACTER_TAG_KEY, String.valueOf(tagId));
        appsFlyerLib.trackEvent(context, CREATE_FAV_EVENT, eventValue);

        YandexMetrica.reportEvent(CREATE_FAV_EVENT, eventValue);
    }
}
