package com.favi.favi.app;


import android.support.multidex.MultiDex;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.crashlytics.android.Crashlytics;
import com.favi.favi.BuildConfig;
import com.favi.favi.di.component.DaggerAppComponent;
import com.favi.favi.util.Constants;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.squareup.leakcanary.LeakCanary;
import com.tspoon.traceur.Traceur;
import com.tspoon.traceur.TraceurConfig;
import com.yandex.metrica.YandexMetrica;

import net.danlew.android.joda.JodaTimeAndroid;

import java.util.Map;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Max on 26.03.2017.
 * My SKYPE: maxvinnik
 */

public class FaViApp extends DaggerApplication implements AppsFlyerConversionListener {

    @Override
    public void onCreate() {
        super.onCreate();

        //init Multidex
        MultiDex.install(this);

        //init Crashlytics
        Fabric.with(this, new Crashlytics());

        //init Pretty StackTrace
        Traceur.enableLogging(new TraceurConfig(true));

        //init LeakCanary
        if (BuildConfig.DEBUG) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your app in this process.
                return;
            }
            LeakCanary.install(this);
            // Normal app init code...

        }

        //init YandexMetrics
        if (!BuildConfig.DEBUG) {
            YandexMetrica.activate(this, Constants.YANDEX_METRICS_API_KEY);
            YandexMetrica.enableActivityAutoTracking(this);
        }

        //init AppsFlyer
        if (!BuildConfig.DEBUG) {
            AppsFlyerLib.getInstance().init(Constants.APPS_FLYER_KEY, this,
                    getApplicationContext());
            AppsFlyerLib.getInstance().startTracking(this);
        }

        //init FFmpegCommand
        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler());
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }

        //init JodaTime
        JodaTimeAndroid.init(this);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }

    @Override
    public void onInstallConversionDataLoaded(Map<String, String> map) {

    }

    @Override
    public void onInstallConversionFailure(String s) {

    }

    @Override
    public void onAppOpenAttribution(Map<String, String> map) {

    }

    @Override
    public void onAttributionFailure(String s) {

    }
}
