package com.favi.favi.api;

import com.favi.favi.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Max on 26.03.2017.
 * My SKYPE: maxvinnik
 */

public class ServiceGenerator {

    //prod
    public static final String BASE_URL = "http://api.faviapp.com/api/";
    public static final String FEED_URL = "http://api.faviapp.com";

    //test
//    public static final String BASE_URL = "http://x.api.faviapp.com/api/";
//    public static final String FEED_URL = "http://x.api.faviapp.com";

    public static final int TAKE_PAGINATION_COUNT = 20;

    private GsonConverterFactory gsonConverterFactory;
    private RxJava2CallAdapterFactory rxJava2CallAdapterFactory;

    private OkHttpClient okHttpClient;

    public ServiceGenerator() {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting()
                .create();

        gsonConverterFactory = GsonConverterFactory.create(gson);

        rxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(httpLoggingInterceptor);
        }

        ErrorRequestInterceptor errorRequestInterceptor = new ErrorRequestInterceptor();
        builder.addInterceptor(errorRequestInterceptor);

        okHttpClient = builder.build();
    }

    public FeedApi feedApi() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .client(okHttpClient)
                .build().create(FeedApi.class);
    }

    public NewFavApi newFavApi() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .client(okHttpClient)
                .build().create(NewFavApi.class);
    }

    public ProfileApi profileApi() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .client(okHttpClient)
                .build().create(ProfileApi.class);
    }

    public CommentsApi commentsApi() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .client(okHttpClient)
                .build().create(CommentsApi.class);
    }
}
