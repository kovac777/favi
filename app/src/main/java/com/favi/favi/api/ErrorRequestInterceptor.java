package com.favi.favi.api;

import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

/**
 * Created by User on 31.01.2018.
 * My SKYPE: maxvinnik
 */

public class ErrorRequestInterceptor implements Interceptor {

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());

        if (!response.isSuccessful()) {
            Request request = chain.request();

            RequestBody requestBody = request.body();
            boolean hasRequestBody = requestBody != null;

            String method = "Method ->\n. ";
            String header = "";
            String body = "Body ->\n. ";

            if (hasRequestBody) {
                method += request.method();

                StringBuilder headers = new StringBuilder("Headers ->\n. ");
                for (int i = 0; i < request.headers().size(); i++) {
                    headers.append(request.headers().name(i)).append(" : ").append(request.headers().value(i)).append("\n. ");
                }
                header = headers.toString();

                body += requestBodyToString(requestBody);
            }

            Crashlytics.logException(new Throwable(request.method() + " : " + request.url()));
            Crashlytics.log(method + header + body);
        }
        return response;
    }

    private String requestBodyToString(RequestBody requestBody) throws IOException {
        Buffer buffer = new Buffer();
        requestBody.writeTo(buffer);
        return buffer.readUtf8();
    }
}
