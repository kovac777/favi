package com.favi.favi.api;

import com.favi.favi.db.entity.LocalProfile;
import com.favi.favi.pojo.request.FirebaseToken;
import com.favi.favi.pojo.request.LoginProfile;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by maxvinnik on 13.01.2018.
 */

public interface ProfileApi {

    @POST("account/RegisterExternal")
    Single<LocalProfile> login(@Body LoginProfile loginProfile);

    @GET("account/obtainlocalaccesstoken")
    Single<LocalProfile> obtainAccessToken(@Query("provider") String provider,
                                           @Query("externalAccessToken") String externalAccessToken);

    @POST("account/Edit")
    Single<LocalProfile> editProfile(@Header("Authorization") String authorization,
                                     @Body LocalProfile profile);

    @POST("account/pushtoken")
    Completable pushFirebaseToken(@Body FirebaseToken firebaseToken);
}
