package com.favi.favi.api;

import com.favi.favi.pojo.request.GenerateFav;
import com.favi.favi.pojo.response.Character;
import com.favi.favi.pojo.response.FFmpegCommand;
import com.favi.favi.pojo.response.SearchWords;
import com.favi.favi.pojo.response.Video;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by maxvinnik on 09.01.2018.
 */

public interface NewFavApi {

    @FormUrlEncoded
    @POST("wordsapi/searchwords")
    Single<SearchWords> getWords(@Field("text") String text,
                                 @Field("tagId") int tagId,
                                 @Field("count") int count);

    @POST("wordsapi/MakeVideo/")
    Single<Video> generateFav(@Body GenerateFav request);

    @POST("wordsapi/MakeVideoffmpeg")
    Single<FFmpegCommand> getFFmpegCommand(@Body GenerateFav request);

    @GET("tags")
    Single<List<Character>> getCharacters();
}
