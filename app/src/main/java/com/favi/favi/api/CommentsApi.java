package com.favi.favi.api;

import com.favi.favi.pojo.request.AddComment;
import com.favi.favi.pojo.request.EditComment;
import com.favi.favi.pojo.request.RemoveComment;
import com.favi.favi.pojo.response.Comments;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by User on 20.01.2018.
 * My SKYPE: maxvinnik
 */

public interface CommentsApi {

    @GET("comments/videowithcount")
    Single<Comments> getVideoComments(@Query("videoId") int videoId,
                                      @Query("skip") int skip,
                                      @Query("take") int take);

    @Headers("Content-type: application/json")
    @POST("comments/add")
    Single<Comments.CommentsBean> addComment(@Header("Authorization") String authorization,
                                             @Body AddComment addComment);

    @Headers("Content-type: application/json")
    @POST("comments/edit")
    Single<Comments.CommentsBean> editComment(@Header("Authorization") String authorization,
                                              @Body EditComment editComment);

    @Headers("Content-type: application/json")
    @POST("comments/delete")
    Completable removeComment(@Header("Authorization") String authorization,
                              @Body RemoveComment removeComment);

}
