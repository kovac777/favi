package com.favi.favi.api;

import com.favi.favi.db.entity.Feed;
import com.favi.favi.pojo.request.Like;
import com.favi.favi.pojo.request.LikesForVideos;
import com.favi.favi.pojo.response.DeleteVideo;
import com.favi.favi.pojo.response.Likes;
import com.favi.favi.pojo.response.RestoreVideo;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by kovac on 03.11.2017.
 * My SKYPE: maxvinnik
 */

public interface FeedApi {

    @GET("feed/get")
    Single<List<Feed>> getFeed(@Query("key") String key,
                               @Query("feedtype") int feedType,
                               @Query("skip") int skip,
                               @Query("take") int take,
                               @Query("userId") int userId);

    @Multipart
    @POST("video/postfile")
    Single<Feed> postInLab(@Part("key") String key,
                           @Part("tagId") int tag,
                           @Part("name") String name,
                           @Part("userId") int userId,
                           @Part MultipartBody.Part video);

    @POST("feed/like")
    Single<Likes> postLike(@Body Like like);

    @FormUrlEncoded
    @POST("video/reportvideo")
    Completable postReport(@Field("key") String key,
                           @Field("vidid") int vidId);

    @Streaming
    @GET
    Single<Response<ResponseBody>> downloadVideo(@Url String url);

    @POST
    Single<List<Likes>> getLikesForVideos(@Body LikesForVideos request);

    @FormUrlEncoded
    @POST("feed/deletevideo")
    Single<DeleteVideo> deleteVideo(@Field("id") int videoId,
                                    @Field("key") String userId);

    @FormUrlEncoded
    @POST("feed/restorevideo")
    Single<RestoreVideo> restoreVideo(@Field("id") int videoId,
                                      @Field("key") String userId);
}
